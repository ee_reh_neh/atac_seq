#!/usr/bin/env perl
# From https://stackoverflow.com/questions/13732295/extract-all-lines-from-text-file-based-on-a-given-list-of-ids

use strict;
use warnings;

my $file1 = $ARGV[0];
my $file2 = $ARGV[1];

open FILE1, "< $file1" or die "could not open $file1\n";
my $keyRef;
while (<FILE1>) {
   chomp;
   $keyRef->{$_} = 1;
}
close FILE1;

open FILE2, "< $file2" or die "could not open $file2\n";
while (<FILE2>) {
    chomp;
    my ($chr, $start, $end, $testKey) = split("\t", $_);
    if (defined $keyRef->{$testKey}) {
        print STDOUT "$_\n";
    }
}
close FILE2;