
### For basic QC analysis of ATAC-seq data in human and chimpanzee iPSCs, and identification of outliers and problematic individuals.
### IGR, 04.15.15

### Last update: 18.11.01: 
### Removed C40210 to make up for its accidental omission from the atac-seq side of the paper. 

### 1. LOAD PACKAGES, COLLECT SESSION INFO.
### 2. READ IN DATA AND CHOOSE WHAT TO SUBSET!
### 3. SUMMARY ANALYSES OF THE DATA ETC.
### 4. DEFINE THE PLOTTING FUNCTIONS AND GENERATE COVARIATE MATRICES FOR TESTING.
### 5. MAKE LOTS OF PLOTS. ###

### THIS SCRIPT IS BEST RUN WITH >= 25GB OF RAM
### Can be run via Rscript with a single command line argument: the choice of allCounts subsets, which are two arguments: the individual subsets and the X chr:
### Assumes allCounts already exists. 
### Options are:
### For the individual subsets
### 1. all: all inds 
### 2. 200k: all inds with 200k pellets
### 3. drop3: 200k pellets, drop three obvious outliers 
### 4. drop4: 200k pellets, drop four obvious outliers 
### 5. drop5: 200k pellets, drop five obvious outliers 
### 6. drop4.1: 200k pellets, drop four obvious outliers and a human-like chimp 
### 7. over: 200k pellets, no overdigested individuals or FC4
### 8. over5: 200k pellets, no overdigested individuals or five obvious outliers from drop5

### For the X chr:
### 1. x: keep the X
### 2. no.x: remove the X

### For the CPM calculation:
### 1. no: do not recalculate CPM, use genome-wide CPM estimates to calculate things.
### 2. recalc: recalculate CPM using read depth only at the selected top x% windows.

### For the CPM filtering:
### 1. union: union of the top x% in both species.
### 2. inters: intersection of the top x% in both species.

### Usage
### echo 'Rscript ~/repos/atac_seq/atac.seq.basic.qc.R [inds filter] [x chr filter] [cpm filter] [union filter]' | qsub -l h_vmem=25 -j [log] -N [jobname] -cwd -j y -V 

# for inds in over5 over drop4.1 drop5 drop4 drop3 200k all; do
#     for xchr in x no.x; do
#         for cpm in no recalc; do
#             for intersect in union inters; do 
#                 echo "Rscript ~/repos/atac_seq/atac.seq.basic.qc.R $inds $xchr $cpm $intersect" |qsub -l h_vmem=30g -o ~/atac_seq/featurecounts/atac_seq_100/logs/before.merge.${inds}.${xchr}.${cpm}.${intersect}.log -N ${inds}.${xchr}.${cpm}.${intersect} -cwd -j y -V; 
#             done;
#         done;
#     done;
# done           


###############################################
### 1. LOAD PACKAGES, COLLECT SESSION INFO. ###
###############################################

# Parse the allCounts switches:
allCountsSwitch <- commandArgs(trailingOnly = TRUE)
# allCountsSwitch <- c("over5", "x", "recalc", "union") # test case

library(gplots) # 3.0.1
library(edgeR, lib="~/R_libs") #3.6.8
library(RColorBrewer) # 1.1-2
library(gdata) # 2.17.0
library(matrixStats) # 0.50.2
library(lattice) #0.20-33
library(beeswarm) #
sessionInfo()

# R version 3.2.2 (2015-08-14)
# Platform: x86_64-pc-linux-gnu (64-bit)

# locale:
#  [1] LC_CTYPE=en_GB.UTF-8       LC_NUMERIC=C               LC_TIME=en_GB.UTF-8        LC_COLLATE=en_GB.UTF-8     LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_GB.UTF-8    LC_PAPER=en_GB.UTF-8      
#  [8] LC_NAME=C                  LC_ADDRESS=C               LC_TELEPHONE=C             LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       

# attached base packages:
# [1] stats     graphics  grDevices utils     datasets  methods   base     

# other attached packages:
# [1] lattice_0.20-33    data.table_1.9.6   matrixStats_0.50.2 gdata_2.17.0       RColorBrewer_1.1-2 edgeR_3.6.8        limma_3.26.9       gplots_3.0.1      

# loaded via a namespace (and not attached):
# [1] KernSmooth_2.23-15 grid_3.2.2         caTools_1.17.1     chron_2.3-47       bitops_1.0-6       gtools_3.5.0     

options(width=200)

##################################################
### 2. READ IN DATA AND CHOOSE WHAT TO SUBSET! ###
##################################################

print("Reading in the counts and cpm matrices (this can take a while...).")
setwd("~/atac_seq/featurecounts/atac_seq_100/")
allCPM <- readRDS("all_samples_100_all_cpm_before_merge.RDS")
allCounts <- readRDS("all_samples_100_all_counts_before_merge.RDS")

# For the correlation with PC1 stuff later...
    tssCounts <- readRDS(file="../atac_seq_100/all_samples_tss_all_counts_before_merge.RDS")
    bgCounts <- readRDS(file="../atac_seq_100/all_samples_bg_all_counts_before_merge.RDS")
    # Fragments is in a different orientation, so all calls to filter it are on rowname, not colname!
    fragments <- readRDS(file="~/atac_seq/mapped/atac_seq/mapping_qc/filtered/processed_fragment_data.RDS")
    # fragments <- readRDS(file="processed_fragment_data.RDS")

# allCounts can take multiple values, and does so here through the switches provided as an external argument. This is the easiest way of extending the pipeline to multiple subsets.

# Write the filters in a somewhat intelligent way:
    # filter the x:
    # We throw out windows if they fall outside the autosomes in either species. Chimp location is coded in the 'chr' column, whereas human location is coded in the Geneid column in allCounts. We then use that subset to prune allCPM.
    if (allCountsSwitch[2] == "x"){
        print("Retaining X-chromosome data.")
        file.prefix <- "all_samples.before_merge"
    } else if (allCountsSwitch[2] == "no.x"){
        print("Removing X-chromosome data.")
        allCounts <- allCounts[!(grepl("chrX", allCounts$Geneid) | grepl("chrX", allCounts$Chr)),]
        allCPM <- allCPM[which(rownames(allCPM) %in% allCounts$Geneid),]
        tssCounts <- tssCounts[tssCounts$Chr != "chrX",]
        bgCounts <- bgCounts[bgCounts$Chr != "chrX",]
        fragments <- fragments
        file.prefix <- "all_samples.before_merge.nox"
    }

    # Filter individuals:
        # All the possible filters:
    if(allCountsSwitch[1] == "200k"){
        print("Removing individuals with pellets != 200k.") 
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts")
        droppedInds <- "HGl|FCN|_100|_50"
    } else if(allCountsSwitch[1] == "drop3"){
        print("Removing individuals with pellets != 200k and 3 obvious outliers.") 
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop3")
        droppedInds <- "HGl|FCN|_100|_50|H20961FC1|H19114FC4|C3651FC1"
    } else if(allCountsSwitch[1] == "drop4"){
        print("Removing individuals with pellets != 200k and 4 obvious outliers.") 
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop4")
        droppedInds <- "HGl|FCN|_100|_50|H20961FC1|H19114FC4|C3651FC1|H19101FC1"
    } else if(allCountsSwitch[1] == "drop5"){
        print("Removing individuals with pellets != 200k and 5 obvious low-depth outliers.") 
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop5")
        droppedInds <- "HGl|FCN|_100|_50|H20961FC1|H19114FC4|C3651FC1|H19101FC1|C40210FC1"
    } else if(allCountsSwitch[1] == "drop4.1"){
        print("Removing individuals with pellets != 200k, 4 obvious outliers and a weird chimpanzee.") 
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop4+1")
        droppedInds <- "HGl|FCN|_100|_50|H20961FC1|H19114FC4|C3651FC1|H19101FC1|C3649FC2"
    } else if(allCountsSwitch[1] == "over"){
        print("Removing individuals with pellets != 200k and some probably overdigested individuals.") 
        droppedInds <- "HGl|FCN|_100|_50|H18489|H19114FC4|C3649FC2"
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop_overdigested")
    } else if(allCountsSwitch[1] == "over5"){
        print("Removing individuals with pellets != 200k, 5 obvious low depth outliers, and some probably overdigested individuals.") 
        droppedInds <- "HGl|FCN|_100|_50|H18489|H19114FC4|C3649FC2|H20961FC1|C3651FC1|H19101FC1|C40210FC1"
        file.prefix <- paste0(file.prefix, ".no_low_cell_counts.drop_5.drop_overdigested")
    } else if(allCountsSwitch[1] == "final"){
        print("Perfect matching to msCentipede runs, including the accidental ommision of C40210.") 
        droppedInds <- "HGl|FCN|_100|_50|H18489|H19114FC4|C3649FC2|H20961FC1|C3651FC1|H19101FC1|C40210"
        file.prefix <- paste0(file.prefix, ".final_set")
    } else if(allCountsSwitch[1] == "all"){
        print("Removing no individuals.")
        droppedInds <- "HGl|FCN"
    }

    # Actual filtering: 
    allCounts <- allCounts[,!grepl(droppedInds, colnames(allCounts))]
    allCPM <- allCPM[,!grepl(droppedInds, colnames(allCPM))]
    tssCounts <- tssCounts[,!grepl(droppedInds, colnames(tssCounts))]
    bgCounts <- bgCounts[,!grepl(droppedInds, colnames(bgCounts))]
    fragments <- fragments[!grepl(droppedInds, rownames(fragments)),]

############################################
### 3. SUMMARY ANALYSES OF THE DATA ETC. ###
############################################

# Change directory to write the plots to:
dir.create(paste0(getwd(), "/basic_qc_final"))
setwd("basic_qc_final")

# The rest of the code assumes allCPM is an n inds * m windows matrix with the column and row names containing sample ids and window ids respectively, while allCounts is n+6 * m, with sample ids in column names and the first six columns are identical to the featureCounts output. 

print("Calculating CPM distribution by species")
chimp.cpm.median <- rowMedians(allCPM[,grepl("^C", colnames(allCPM))])
chimp.cpm.quant <- quantile(chimp.cpm.median, probs=c(0.95, 0.99, 0.995, 0.999))
print("Chimpanzee quantiles")
print(chimp.cpm.quant)

human.cpm.median <- rowMedians(allCPM[,grepl("^H", colnames(allCPM))])
human.cpm.quant <- quantile(human.cpm.median, probs=c(0.95, 0.99, 0.995, 0.999))
print("Human quantiles")
print(human.cpm.quant)

names(chimp.cpm.median) <- allCounts[,1]
names(human.cpm.median) <- allCounts[,1]

# Pull union or intersection of top x% of windows in either species
print("Retrieving top CPM windows across species")
if (allCountsSwitch[4] == "union"){
    print ("Retrieveing the union of chimp and human top windows")
    to.keep.01 <- union(names(human.cpm.median[human.cpm.median >= human.cpm.quant[4]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[4]]))
    to.keep.05 <- union(names(human.cpm.median[human.cpm.median >= human.cpm.quant[3]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[3]]))
    to.keep.1 <- union(names(human.cpm.median[human.cpm.median >= human.cpm.quant[2]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[2]]))
    to.keep.5 <- union(names(human.cpm.median[human.cpm.median >= human.cpm.quant[1]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[1]]))
        if (allCountsSwitch[3] == "recalc") {
        allCPM.01 <- cpm(allCounts[allCounts$Geneid %in% to.keep.01,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.05 <- cpm(allCounts[allCounts$Geneid %in% to.keep.05,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.1 <- cpm(allCounts[allCounts$Geneid %in% to.keep.1,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.5 <- cpm(allCounts[allCounts$Geneid %in% to.keep.5,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        print("Recalculated CPM on the basis of selected regions.")
        file.suffix <- ".union_recalc"
    } else if (allCountsSwitch[3] == "no") {
        allCPM.01 <- allCPM[to.keep.01,]
        allCPM.05 <- allCPM[to.keep.05,]
        allCPM.1 <- allCPM[to.keep.1,]
        allCPM.5 <- allCPM[to.keep.5,]
        print("Retrieved genome-wide CPM estimates for selected regions.")  
        file.suffix <- ".union_gw"
    }
    print("Retrieving filtered count matrices for PCA covariates")
    allCounts.01 <- allCounts[allCounts$Geneid %in% to.keep.01,]
    allCounts.05 <- allCounts[allCounts$Geneid %in% to.keep.05,]
    allCounts.1 <- allCounts[allCounts$Geneid %in% to.keep.1,]
    allCounts.5 <- allCounts[allCounts$Geneid %in% to.keep.5,]

    print("Plotting median CPM by species")
    pdf(file=paste0(file.prefix, file.suffix, ".median_cpm_human_chimp.pdf"))
    smoothScatter(human.cpm.median[to.keep.01], chimp.cpm.median[to.keep.01], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 0.1%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.01], chimp.cpm.median[to.keep.01], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.05], chimp.cpm.median[to.keep.05], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 0.5%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.05], chimp.cpm.median[to.keep.05], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.1], chimp.cpm.median[to.keep.1], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 1%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.1], chimp.cpm.median[to.keep.1], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.5], chimp.cpm.median[to.keep.5], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 5%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.5], chimp.cpm.median[to.keep.5], method="spearman"), digits=3), bty="n")
    dev.off()
} else if (allCountsSwitch[4] == "inters"){
    print ("Retrieveing the intersection of chimp and human top windows")
    to.keep.01 <- intersect(names(human.cpm.median[human.cpm.median >= human.cpm.quant[4]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[4]]))
    to.keep.05 <- intersect(names(human.cpm.median[human.cpm.median >= human.cpm.quant[3]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[3]]))
    to.keep.1 <- intersect(names(human.cpm.median[human.cpm.median >= human.cpm.quant[2]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[2]]))
    to.keep.5 <- intersect(names(human.cpm.median[human.cpm.median >= human.cpm.quant[1]]), names(chimp.cpm.median[chimp.cpm.median >= chimp.cpm.quant[1]]))
    if (allCountsSwitch[3] == "recalc") {
        allCPM.01 <- cpm(allCounts[allCounts$Geneid %in% to.keep.01,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.05 <- cpm(allCounts[allCounts$Geneid %in% to.keep.05,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.1 <- cpm(allCounts[allCounts$Geneid %in% to.keep.1,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        allCPM.5 <- cpm(allCounts[allCounts$Geneid %in% to.keep.5,7:dim(allCounts)[2]], log=T, prior.count=0.25)
        print("Recalculated CPM on the basis of selected regions.")
        file.suffix <- ".intersect_recalc"
    } else if (allCountsSwitch[3] == "no") {
        allCPM.01 <- allCPM[to.keep.01,]
        allCPM.05 <- allCPM[to.keep.05,]
        allCPM.1 <- allCPM[to.keep.1,]
        allCPM.5 <- allCPM[to.keep.5,]
        print("Retrieved genome-wide CPM estimates for selected regions.")  
        file.suffix <- ".intersect_gw"
    }
    print("Retrieving filtered count matrices for PCA covariates")
    allCounts.01 <- allCounts[allCounts$Geneid %in% to.keep.01,]
    allCounts.05 <- allCounts[allCounts$Geneid %in% to.keep.05,]
    allCounts.1 <- allCounts[allCounts$Geneid %in% to.keep.1,]
    allCounts.5 <- allCounts[allCounts$Geneid %in% to.keep.5,]

    print("Plotting median CPM by species")
    pdf(file=paste0(file.prefix, file.suffix, ".median_cpm_human_chimp.pdf"))
    smoothScatter(human.cpm.median[to.keep.01], chimp.cpm.median[to.keep.01], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 0.1%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.01], chimp.cpm.median[to.keep.01], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.05], chimp.cpm.median[to.keep.05], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 0.5%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.05], chimp.cpm.median[to.keep.05], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.1], chimp.cpm.median[to.keep.1], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 1%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.1], chimp.cpm.median[to.keep.1], method="spearman"), digits=3), bty="n")

    smoothScatter(human.cpm.median[to.keep.5], chimp.cpm.median[to.keep.5], xlab="log2 human median CPM", ylab="log2 chimp median CPM", main="top 5%")
    abline(a=0, b=1, col="red", lty=2)
    legend(x="topright", legend=round(cor(human.cpm.median[to.keep.5], chimp.cpm.median[to.keep.5], method="spearman"), digits=3), bty="n")
    dev.off()
}

print("Dimensions")
print(dim(allCPM.01))
print(dim(allCPM.05))
print(dim(allCPM.1))
print(dim(allCPM.5))

#####################################################################################
### 4. DEFINE THE PLOTTING FUNCTIONS AND GENERATE COVARIATE MATRICES FOR TESTING. ###
#####################################################################################

# Here is the PCA plotting function:
plot.pca <- function(data.to.pca, type, percentage, sum.palette, species.col, names.pch, sample.names){
    # check for invariant rows:
    data.to.pca.clean <- data.to.pca[!apply(data.to.pca, 1, var) == 0,]
    pca <- prcomp(t(data.to.pca.clean), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    plot(pca$x[,1], pca$x[,2], col=species.col, pch=names.pch, cex=2, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=species.col[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)
    plot(pca$x[,2], pca$x[,3], col=species.col, pch=names.pch, cex=2, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=species.col[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)
    plot(pca$x[,3], pca$x[,4], col=species.col, pch=names.pch, cex=2, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=species.col[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)

    plot(pca$x[,1], pca$x[,2], col=sum.palette, pch=names.pch, cex=2, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=sum.palette[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)
    plot(pca$x[,2], pca$x[,3], col=sum.palette, pch=names.pch, cex=2, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=sum.palette[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)
    plot(pca$x[,3], pca$x[,4], col=sum.palette, pch=names.pch, cex=2, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""), main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
    legend(col=sum.palette[!duplicated(sample.names)], legend=unique(sample.names), pch=unique(names.pch), x="topright", cex=0.6)

    all.pcs <- pc.assoc(pca)
    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

# And here is the PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))
    return(all.pcs)
}

# Fraction of reads in x% vectors:
    all.sums <- colSums(allCounts[,7:dim(allCounts)[2]])
    all.sums.01 <- colSums(allCounts.01[,7:dim(allCounts)[2]])
    all.sums.05 <- colSums(allCounts.05[,7:dim(allCounts)[2]])
    all.sums.1 <- colSums(allCounts.1[,7:dim(allCounts)[2]])
    all.sums.5 <- colSums(allCounts.5[,7:dim(allCounts)[2]])

    # Write these out for future access by other scripts that might need them!
    ratio.01 <- all.sums.01/all.sums
    ratio.05 <- all.sums.05/all.sums
    ratio.1 <- all.sums.1/all.sums
    ratio.5 <- all.sums.5/all.sums

# TSS ratios vectors: 
    tss.bg <- colSums(tssCounts[,7:ncol(tssCounts)])/colSums(bgCounts[,7:ncol(bgCounts)])
    tss.all <- colSums(tssCounts[,7:ncol(tssCounts)])/all.sums
    names(tss.bg) <- colnames(tssCounts[,7:ncol(tssCounts)])
    names(tss.all) <- colnames(tssCounts[,7:ncol(tssCounts)])

# Species vector:
    species <- as.factor(ifelse(grepl("^H", colnames(allCPM)), "H", "C"))

# Fragment size distribution
    # There is nothing to prep here, it was all done above. 

# Write everything out:
all.covars.df <- cbind(all.sums, ratio.01, ratio.05, ratio.1, ratio.5, tss.bg, tss.all, species, fragments)
all.sums.df <- cbind(all.sums, all.sums.01, all.sums.05, all.sums.1, all.sums.5)

write.table(all.sums.df, file=paste0(file.prefix, file.suffix, "_all_sums.out"), quote=F, sep="\t")
write.table(all.covars.df, file=paste0(file.prefix, file.suffix, "_all_covars.out"), quote=F, sep="\t")
    
##############################
### 5. MAKE LOTS OF PLOTS. ###
##############################

#Make a read-depth based color scheme for samples, also define plotting symbols. Note that the ones that are needed for the PCA plotting need to be returned to the global environment for scope reasons. 
sum.palette <- ifelse(all.sums >= 20000000, "green3", ifelse(all.sums >= 10000000, "yellow3", ifelse(all.sums >= 5000000, "orange2", "red3")))
sample.names <- colnames(allCounts[,7:dim(allCounts)[2]])
sample.names.slim <- gsub("FC[0-9]|LIB1|LIB2|\\.", "", sample.names)

names.pch <- c(1:length(colnames(allCPM)))
names.pch.slim <- as.numeric(as.factor(sample.names.slim)) 

species.col <- ifelse(grepl("^H", sample.names), "plum4", "yellow3")

#Boxplots of cpm per window:
print("Making boxplots")
pdf(file=paste0(file.prefix, file.suffix, "_cpm_boxplots.pdf"))
par(mai=c(1.5,0.82,0.82,0.42))
par(cex.main=0.8)
    boxplot(allCPM.01, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 0.1% windows") 
    boxplot(allCPM.05, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 0.5% windows") 
    boxplot(allCPM.1, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 1% windows") 
    boxplot(allCPM.5, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 5% windows") 
dev.off()

# PCs 1-3, coloured both by depth and by species. 
print("Calculating and plotting PCA results (this can take a few minutes)")
pdf(file=paste0(file.prefix, file.suffix, "_cpm_pca.pdf"))
    pca.covars.01 <- plot.pca(allCPM.01, "CPM", 0.1, sum.palette, species.col, names.pch.slim, sample.names.slim)
    pca.covars.05 <- plot.pca(allCPM.05, "CPM", 0.5, sum.palette, species.col, names.pch.slim, sample.names.slim)
    pca.covars.1 <- plot.pca(allCPM.1, "CPM", 1, sum.palette, species.col, names.pch.slim, sample.names.slim)
    pca.covars.5 <- plot.pca(allCPM.5, "CPM", 5, sum.palette, species.col, names.pch.slim, sample.names.slim)
dev.off()

write.table(pca.covars.01, file=paste0(file.prefix, file.suffix, "_01_pca_covars.out"), quote=F, row.names=T, col.names=T, sep="\t", eol="\n")
write.table(pca.covars.05, file=paste0(file.prefix, file.suffix, "_05_pca_covars.out"), quote=F, row.names=T, col.names=T, sep="\t", eol="\n")
write.table(pca.covars.1, file=paste0(file.prefix, file.suffix, "_1_pca_covars.out"), quote=F, row.names=T, col.names=T, sep="\t", eol="\n")
write.table(pca.covars.5, file=paste0(file.prefix, file.suffix, "_5_pca_covars.out"), quote=F, row.names=T, col.names=T, sep="\t", eol="\n")

# And what about the correlations between samples and replicates?
print("Calculating and plotting correlation matrices (this can take a few minutes)")
pdf(file=paste0(file.prefix, file.suffix, "_cpm_correlation_heatmaps.pdf"))
par(cex.main=0.8)
    heatmap.2(cor(allCPM.5, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation all top 5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.1, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation all top 1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.05, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation all top 0.5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.01, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation all top 0.1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.5, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation all top 5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.1, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation all top 1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.05, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation all top 0.5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
    heatmap.2(cor(allCPM.01, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation all top 0.1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
dev.off()

######################################################################
### 6. Look at the relationship between replicates in this subset. ###
######################################################################
 
sample.names <- colnames(allCounts[,7:dim(allCounts)[2]])
sample.names.ind <- sub("FC[0-9].LIB[0-9]", "", sample.names, fixed=F)
sample.names.lib <- sub("FC[0-9]", "", sample.names, fixed=F)

sample.palette <- c(brewer.pal(12, "Set3"), "black")
sample.cols <- sample.palette[as.numeric(as.factor(sample.names.ind))]

plot.reproducibility <- function(data.to.test, testtype){
    cor.mat <- cor(data.to.test, method=testtype, use="pairwise.complete.obs")

    library.rep <- vector()
    library.rep.col <- vector()
    ind.rep <- vector()
    ind.rep.col <- vector()
    species.rep <- vector()
    between.species <- vector()

    for (i in 1:ncol(data.to.test)){
        for (j in 1:ncol(data.to.test)){
            if (j > i){
                if (sample.names.lib[i] == sample.names.lib[j])
                    {library.rep <- c(library.rep, cor.mat[i,j])
                     library.rep.col <- c(library.rep.col, sample.cols[i])       
                } else if (sample.names.ind[i] == sample.names.ind[j]) 
                    {ind.rep <- c(ind.rep, cor.mat[i,j])
                     ind.rep.col <- c(ind.rep.col, sample.cols[i])
                } else if (species[i] == species[j]) 
                    {species.rep <- c(species.rep, cor.mat[i,j])
                } else {between.species <- c(between.species, cor.mat[i,j])}
            }
        }
    }

    for.plot <- list(library.rep, ind.rep, species.rep, between.species)
    boxplot(for.plot, ylab=paste0(testtype, " correlation"), names=c("Within\nlibraries", "Within\nindividuals", "Within\nspecies", "Between\nspecies"))
    beeswarm(for.plot[1], vertical = TRUE, method = "swarm", add = TRUE, pch = 20, cex=2, pwcol=library.rep.col)
    beeswarm(for.plot[2], vertical = TRUE, method = "swarm", add = TRUE, pch = 20, cex=2, at=2, pwcol=ind.rep.col)
    stripchart(for.plot[3:4], vertical = TRUE, method = "jitter", add = TRUE, pch = 20, cex=2, at=3:4, col="grey50")
    legend(x="topright", legend=unique(sample.names.ind), col=unique(sample.cols), bty="n", pch=20, cex=0.75)
}

print("Calculating and plotting reproducibility between samples")
pdf(file=paste0(file.prefix, file.suffix, "_cpm_reproducibility.pdf"))
    plot.reproducibility(allCPM.01, "spearman")
    plot.reproducibility(allCPM.05, "spearman")
    plot.reproducibility(allCPM.1, "spearman")
    plot.reproducibility(allCPM.5, "spearman")
    plot.reproducibility(allCPM.01, "pearson")
    plot.reproducibility(allCPM.05, "pearson")
    plot.reproducibility(allCPM.1, "pearson")
    plot.reproducibility(allCPM.5, "pearson")
dev.off()
