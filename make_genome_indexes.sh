#For indexing with Subread 1.4.6 and BWA both. Replaces make_subread_indexes.sh

echo "subread-buildindex -o /data/external_public/reference_genomes/hg19/hg19.subread.autoXM.no_Y_random /data/external_public/reference_genomes/hg19/hg19.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N hg19_subread_index -o ~/logs/hg19_subread_index.out -e ~/logs/hg19_subread_index.err

echo "subread-buildindex -o /data/external_public/reference_genomes/panTro3/panTro3.subread.autoXM.no_Y_random /data/external_public/reference_genomes/panTro3/panTro3.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N panTro3_subread_index -o ~/logs/panTro3_subread_index.out -e ~/logs/panTro3_subread_index.err

echo "subread-buildindex -o /data/external_public/reference_genomes/rheMac3/rheMac3.subread.autoXM.no_Y_random /data/external_public/reference_genomes/rheMac3/rheMac3.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N rheMac3_subread_index -o ~/logs/rheMac3_subread_index.out -e ~/logs/rheMac3_subread_index.err

#echo "subread-buildindex -o /data/external_public/reference_genomes/rheMac2/rheMac2_subread /data/external_public/reference_genomes/rheMac2/rheMac2.fa" | qsub -l h_vmem=10g -v PATH -cwd -N rheMac2_index -o ~/logs/rheMac2_subread_index.out -e ~/logs/rheMac2_subread_index.err

echo "bwa index /data/external_public/reference_genomes/hg19/hg19.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N hg19_bwa_index -o ~/logs/hg19_bwa_index.out -e ~/logs/hg19_bwa_index.err

echo "bwa index /data/external_public/reference_genomes/panTro3/panTro3.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N panTro3_bwa_index -o ~/logs/panTro3_bwa_index.out -e ~/logs/panTro3_bwa_index.err

echo "bwa index /data/external_public/reference_genomes/rheMac3/rheMac3.autoXM.no_Y_random.fa" | qsub -l h_vmem=10g -v PATH -cwd -N rheMac3_bwa_index -o ~/logs/rheMac3_bwa_index.out -e ~/logs/rheMac3_bwa_index.err