#! /bin/bash

## File: mapping.stats.and.featurecounts.sh 
## Author: IGR
## Date: 22.03.15
## This code is used to take a list of qc-ed bam files 
## and generate quality plots and feature counts
## from them. It takes 2 arguments-
## $1: The path to the chosen bam files. *required*
## $2: The path to the featureCounts outdirectory. *required*

picardpath="/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar"
java="/usr/bin/java"
samtools="/usr/local/bin/samtools"
qcdir="/mnt/gluster/data/internal_supp/atac_seq/mapped/atac_seq/mapping_qc/filtered"
bamtools="/mnt/lustre/home/shyamg/bin/bamtools"

if [ $# -ne 1 ]; then
    echo "USAGE: split.bam.fragmentsize.sh InputBamDirectory"
    exit 1
fi

bamdirectory=$1

if [ ! -d $qcdir/logs ]; then
    mkdir -p $qcdir/logs
fi

for tsampName in $bamdirectory/*.merged.nochrM.rmdup.filter.TAG*.bam; do
    sampName=`basename $tsampName .merged.nochrM.rmdup.filter.TAG*.bam`

# First, qsub the two picard jobs that can handle the METRIC_ACCUMULATION_LEVEL argument: 
# CollectInsertSizeMetrics 
    echo -e "#! /bin/bash

    echo 'Calculating fragment size distribution for $sampName.'
    if [ -s $qcdir/$sampName.processed.fragments.out ]; then
        echo 'Fragment size distribution for $sampName has already been calculated.'
    else
        # Launch CollectInsertSizeMetrics
        # Unintuitevely, we do not need to use the READ_GROUP option in METRIC_ACCUMULATION_LEVEL, because picard does not parse reads on the basis of their individual RG tag. 
        # Instead, the RG:ID tag is used only to decipher the rest of the tags in the RG header, and reads are split according to those. 
        # READ_GROUP corresponds to the PU tag in the header, which itself stands for 'platform unit'.
        # See also: http://sourceforge.net/p/samtools/mailman/message/32186480/ 
        # This is a bit silly but in this case can be easily solved by compiling metrics at the library level.
        $java -Xmx3g -jar $picardpath CollectInsertSizeMetrics INPUT=$tsampName OUTPUT=$qcdir/$sampName.processed.fragments.out HISTOGRAM_FILE=$qcdir/$sampName.processed.fragments.pdf METRIC_ACCUMULATION_LEVEL=SAMPLE VALIDATION_STRINGENCY=LENIENT ASSUME_SORTED=TRUE

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed CollectInsertSizeMetrics with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully calculated fragment size distribution for $sampName.
        fi
    fi
    " | qsub -l h_vmem=4g -o $qcdir/logs/$sampName.fragments.processed.out -N insertsize.$sampName -cwd -j y 
      
done