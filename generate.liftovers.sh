#! /bin/bash

## File: generate.liftovers.sh
## Author: Shyam Gopalakrishnan
## Creation date: 15th Mar 2015
## Last edit date: 22nd Mar 2015
## This script runs the lift over from human genome to the
## chimp genome. It uses a whole set of stringencies - (0.7,
## 0.75, 0.8, 0.85, 0.9, 0.95) - to get the orthologous 
## windows in the chimp. Any window that does not have a unique
## map in the chimp is discarded. The chosen windows are then 
## lifted over back to the human genome with the same stringency.
## Again, non-uniquely mapping windows are discarded. 
## The script takes32 arguments
## $1 - human-chimp liftover chain
## $2 - chimp-human liftover chain
## $3 - the original bed file to be lifted over
## NOTE: Although I have said human and chimp in the description, 
## the chains define the two species. It could easily be any 2 species.
##
## Edit on 22nd Mar 2015: 
## The original liftover takes forever, so we are going to split it
## into many smaller processes, one for each window.

if [ $# -ne 4 ]; then
  echo "USAGE: generate.liftovers.sh FirstChain SecondChain BedFile Stringency"
  exit 1
fi 

## Assumptions - the bed file has a .bed extension
## The chain files are named species1Tospecies2.chain.gz
## These assumption help in naming the output files.
firstchain=$1
secondchain=$2
bedfile=$3
stringency=$4

## parameters, window size limit upper and lower, 
## also num windows per process
window_lower=80
window_upper=120
windows_per_bed=200000

basebed=`basename $bedfile .bed`
species1=`basename $firstchain | sed "s/To.\+$//"`
species2=`basename $secondchain | sed "s/To.\+$//"`

#Variable names
liftover='/mnt/lustre/home/shyamg/bin/liftOver'
proj_home='/mnt/gluster/data/internal_supp/atac_seq/'
outdir='/mnt/gluster/data/internal_supp/atac_seq/liftOvers'
logdir='/mnt/gluster/data/internal_supp/atac_seq/liftOvers/logs'
tmpdir='/mnt/gluster/data/internal_supp/atac_seq/picard_temp/lifttemp/'
pywinscript='/mnt/lustre/home/shyamg/projects/atac_seq/getLiftedWindows.py'
pymapscript='/mnt/lustre/home/shyamg/projects/atac_seq/getWindowMappability.py'

# Control variables
mapfun='propUnique'
mapthresh='0.8'

# Create directories
if [ ! -d $outdir ]; then
  mkdir -p $outdir
fi
if [ ! -d $logdir ]; then
  mkdir -p $logdir
fi
if [ ! -d $tmpdir ]; then
  mkdir -p $tmpdir
fi

# Generate the script file to generate the liftovers, fwd and rev
# combine them and filter them on mappability
num_windows=`wc -l $bedfile | cut -f1 -d ' '`
let num_procs=($num_windows+$windows_per_bed-1)/$windows_per_bed

###### DEBUG
#num_procs=2
###### END DEBUG

## Launch the split jobs.
echo "#! /bin/bash

#$ -cwd
#$ -j y
#$ -o $logdir/$species1.$species2.$stringency.\$TASK_ID.log
#$ -N $species1.$species2.$stringency
#$ -t 1-$num_procs
#$ -l h_vmem=2g

inbed=$tmpdir/$basebed.\$JOB_NAME.part\$SGE_TASK_ID.in.bed
tempbed=$tmpdir/$basebed.\$JOB_NAME.part\$SGE_TASK_ID.temp.bed
frontbed=$tmpdir/$basebed.\$JOB_NAME.part\$SGE_TASK_ID.front.bed
revbed=$tmpdir/$basebed.\$JOB_NAME.part\$SGE_TASK_ID.rev.bed
unmapped=$tmpdir/$basebed.\$JOB_NAME.part\$SGE_TASK_ID.unmapped.bed

# Generate the temporary file for this job. 
let headlines=\$SGE_TASK_ID*$windows_per_bed
head -\$headlines $bedfile | tail -n $windows_per_bed > \$inbed

echo parameters $species1 $species2 $stringency part\$SGE_TASK_ID
echo Creating first liftover 
$liftover \$inbed $firstchain \$tempbed \$unmapped

exitstatus=\$?
if [ \$exitstatus -ne 0 ]; then
  echo Failed first liftover of part \$SGE_TASK_ID with exit code \$exitstatus
  exit \$exitstatus
else
  echo Completed first liftover
fi

echo Filtering windows
# Filter windows
awk '{ if (\$3-\$2 <= $window_upper && \$3-\$2 >= $window_lower) { print \$0 } }' < \$tempbed > \$frontbed

exitstatus=\$?
if [ \$exitstatus -ne 0 ]; then
  echo Failed first awk filter of part \$SGE_TASK_ID with exit code \$exitstatus
  exit \$exitstatus
else
  echo Completed first awk filter
fi

echo Creating second liftover 
$liftover \$frontbed $secondchain \$tempbed \$unmapped

exitstatus=\$?
if [ \$exitstatus -ne 0 ]; then
  echo Failed second liftover of part \$SGE_TASK_ID with exit code \$exitstatus
  exit \$exitstatus
else
  echo Completed second liftover
fi

echo Filtering windows
# Filter windows
awk '{ if (\$3-\$2 <= $window_upper && \$3-\$2 >= $window_lower) { print \$0 } }' < \$tempbed > \$revbed

exitstatus=\$?
if [ \$exitstatus -ne 0 ]; then
  echo Failed second awk filter of part \$SGE_TASK_ID with exit code \$exitstatus
  exit \$exitstatus
else
  echo Completed second awk filter
fi

echo Making final files.
# Get window names
python $pywinscript \$inbed \$frontbed \$revbed $tmpdir/$species1.$stringency.part\$SGE_TASK_ID.bed $tmpdir/$species2.$stringency.part\$SGE_TASK_ID.bed 
echo Made final files.
" | qsub 

# Launch collect job based on split.
echo "#! /bin/bash

#$ -cwd
#$ -j y
#$ -o $logdir/$species1.$species2.$stringency.collect.log
#$ -N collect.$species1.$species2.$stringency
#$ -l h_vmem=10g
#$ -hold_jid $species1.$species2.$stringency

echo Collecting all intermediate bed files.
cat \`ls $tmpdir/$species1.$stringency.part*.bed | sort -k3.5,3n -t.\` > $outdir/$basebed.$species1.$stringency.bed
cat \`ls $tmpdir/$species2.$stringency.part*.bed | sort -k3.5,3n -t.\` > $outdir/$basebed.$species2.$stringency.bed
echo Done collecting files.

refsp1='/data/external_public/reference_genomes/$species1/gem/$species1.autoXM.no_Y_random_gem_kmer50.mappability'
refsp2='/data/external_public/reference_genomes/$species2/gem/$species2.autoXM.no_Y_random_gem_kmer50.mappability'
echo Starting mappability filtering
python $pymapscript -b $outdir/$basebed.$species1.$stringency.bed -b $outdir/$basebed.$species2.$stringency.bed -g \$refsp1 -g \$refsp2 -w -d $basebed.mapp$mapthresh -s -f $mapfun -t $mapthresh 
echo Done mappability filtering
" | qsub
