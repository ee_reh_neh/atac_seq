### 
### Script for processing ATAC-seq data genome wide and around 
### transcription start sites. Replaces some of the piece-meal scripts
### that existed before into a single integrated one with functions at
### most steps and the such. 
###

### IGR
### Last update: 17.08.20: Updated with final BG and TSS files. 

##############################################
### 0. LOAD LIBRARIES AND CLEAR WORKSPACE. ###
##############################################

rm(list=ls())
library(gplots) # 3.0.1
library(limma) # 3.26.9
library(edgeR, lib="~/R_libs") #3.6.8
library(RColorBrewer) # 1.1-2
library(gdata) # 2.17.0
library(matrixStats) # 0.50.2
library(data.table) # 1.9.6
# library(ggplot2)
# library(reshape2)

sessionInfo()

options(width=200)

#################################################
### 1. DEFINE AND RUN THE PRE-MERGE FUNCTION. ### 
#################################################

qcBySpecies <- function(indir, file_in, file_out, plotMe){
    # Streamline reading in
    setwd(indir)
    speciesCounts <- fread(paste0(file_in), sep="\t")

    print(paste0("Read in ", dim(speciesCounts)[1], " rows from ", file_in))

    # Clean out the differentiation samples
    speciesCounts <- speciesCounts[, !grepl("day", colnames(speciesCounts)), with=FALSE]

    # Some basic QC: 
    # How many rows are invariant and contain 0 reads across all samples? 
    speciesTrim <- subset(speciesCounts, rowSums(speciesCounts[,7:dim(speciesCounts)[2], with=F]) > 0,)

    print(paste0(dim(speciesCounts)[1] - dim(speciesTrim)[1], " windows contained 0 reads across all samples."))
    print(paste0(round(dim(speciesTrim)[1]/dim(speciesCounts)[1], digits=3)*100, "% of windows contain at least 1 read somewhere."))

    # Then, compute CPM and RPKM 
    print("Calculating CPM for basic descriptive plots")
    speciesCpm <- cpm(speciesCounts[,7:dim(speciesCounts)[2], with=F], log=T, prior.count=0.25)
    # speciesRpkm <- rpkm(speciesCounts[,7:dim(speciesCounts)[2], with=F], log=T, prior.count=0.25, gene.length=speciesCounts$Length)

    rownames(speciesCpm) <- speciesCounts$Geneid
    speciesCpm.median <- rowMedians(speciesCpm)
    speciesCpm.quant <- quantile(speciesCpm.median, probs=c(0.95, 0.99, 0.995, 0.999))
    print("Unfiltered log2 CPM quantiles:")
    print(speciesCpm.quant)
    
    # rownames(speciesRpkm) <- speciesCounts$Geneid
    # speciesRpkm.median <- rowMedians(speciesRpkm)
    # speciesRpkm.quant <- quantile(speciesRpkm.median, probs=c(0.95, 0.99, 0.995, 0.999))
    # print("Unfiltered log2 RPKM quantiles:")
    # print(speciesRpkm.quant)

    names(speciesCpm.median) <- speciesCounts[,Geneid]
    # names(speciesRpkm.median) <- speciesCounts[,Geneid]

    speciesCpm.01 <- speciesCpm[speciesCounts$Geneid %in% names(speciesCpm.median[speciesCpm.median >= speciesCpm.quant[4]]),]
    speciesCpm.05 <- speciesCpm[speciesCounts$Geneid %in% names(speciesCpm.median[speciesCpm.median >= speciesCpm.quant[3]]),]
    speciesCpm.1 <- speciesCpm[speciesCounts$Geneid %in% names(speciesCpm.median[speciesCpm.median >= speciesCpm.quant[2]]),]
    speciesCpm.5 <- speciesCpm[speciesCounts$Geneid %in% names(speciesCpm.median[speciesCpm.median >= speciesCpm.quant[1]]),]

    # speciesRpkm.01 <- speciesRpkm[speciesCounts$Geneid %in% names(speciesRpkm.median[speciesRpkm.median >= speciesRpkm.quant[4]]),]
    # speciesRpkm.05 <- speciesRpkm[speciesCounts$Geneid %in% names(speciesRpkm.median[speciesRpkm.median >= speciesRpkm.quant[3]]),]
    # speciesRpkm.1 <- speciesRpkm[speciesCounts$Geneid %in% names(speciesRpkm.median[speciesRpkm.median >= speciesRpkm.quant[2]]),]
    # speciesRpkm.5 <- speciesRpkm[speciesCounts$Geneid %in% names(speciesRpkm.median[speciesRpkm.median >= speciesRpkm.quant[1]]),]

    # Plotting takes forever, so we put it behind a flag:
    if(plotMe == TRUE){

        #Make a quick, read-depth based color scheme for samples, also define plotting symbols
        speciesSums <- colSums(speciesCounts[,7:dim(speciesCounts)[2], with=F])
        sum.palette <- ifelse(speciesSums >= 20000000, "green3", ifelse(speciesSums >= 10000000, "yellow3", ifelse(speciesSums >= 5000000, "orange2", "red3")))
        sample.names <- colnames(speciesCounts[,7:dim(speciesCounts)[2], with=F])
        names.pch <- seq(1, length(sample.names), 1)

        print("Plotting CPM and RPKM densities")
        pdf(file=paste0("plots/", file_out, "_cpm_rpkm_densities.pdf"))
            plotDensities(speciesCpm)
            # plotDensities(speciesRpkm)
        dev.off()

        print("Drawing CPM and RPKM boxplots at different quantiles.")
        pdf(file=paste0("plots/", file_out, "_rpkm_cpm_boxplots_before_merge.pdf"))
            par(mai=c(1.5,0.82,0.82,0.42))
            par(cex.main=0.8)
            boxplot(speciesCpm.01, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 0.1% windows") 
            boxplot(speciesCpm.05, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 0.5% windows") 
            boxplot(speciesCpm.1, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 1% windows") 
            boxplot(speciesCpm.5, ylab="log2 CPM", names=sample.names, las=2, col=sum.palette, main="CPM top 5% windows") 
            # boxplot(speciesRpkm.01, ylab="log2 RPKM", names=sample.names, las=2, col=sum.palette, main="RPKM top 0.1% windows") 
            # boxplot(speciesRpkm.05, ylab="log2 RPKM", names=sample.names, las=2, col=sum.palette, main="RPKM top 0.5% windows") 
            # boxplot(speciesRpkm.1, ylab="log2 RPKM", names=sample.names, las=2, col=sum.palette, main="RPKM top 1% windows") 
            # boxplot(speciesRpkm.5, ylab="log2 RPKM", names=sample.names, las=2, col=sum.palette, main="RPKM top 5% windows") 
        dev.off()

        # And what about the correlations between samples and replicates?
        print("Calculating and drawing correlation matrices of outlier quantiles.")
        pdf(file=paste0("plots/", file_out, "_sample_correlations_before_merge.pdf"))
            par(cex.main=0.8)
            heatmap.2(cor(speciesCpm.5, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.1, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.05, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 0.5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.01, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 0.1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.5, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.1, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.05, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 0.5% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            heatmap.2(cor(speciesCpm.01, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 0.1% from CPM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.5, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 5% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.1, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 1% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.05, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 0.5% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.01, method="pearson", use="pairwise.complete.obs"), trace="none", main="Pearson correlation species top 0.1% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.5, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 5% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.1, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 1% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.05, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 0.5% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
            # heatmap.2(cor(speciesRpkm.01, method="spearman", use="pairwise.complete.obs"), trace="none", main="Spearman correlation species top 0.1% from RPKM", margins=c(8,8), srtCol=45, srtRow=45)
        dev.off()

        plot.pca <- function(data.to.pca, type, percentage){
            # check for invariant rows:
            data.to.pca.clean <- data.to.pca[!apply(data.to.pca, 1, var) == 0,]
            pca <- prcomp(t(data.to.pca.clean), scale=T, center=T)
            plot(pca$x[,1], pca$x[,2], col=sum.palette, pch=names.pch, cex=2, ylab="PC2", xlab="PC1", main=paste("PCA,", type, "top", percentage, "% windows", sep=" "))
            legend(col=sum.palette, legend=sample.names, pch=names.pch, x="topleft")
        }

        pdf(file=paste0("plots/", file_out, "_pcas_before_merge.pdf"))
            print("Calculating and plotting PCA plots of outlier quantiles.")
            plot.pca(speciesCpm.01, "CPM", 0.1)
            plot.pca(speciesCpm.05, "CPM", 0.5)
            plot.pca(speciesCpm.1, "CPM", 1)
            plot.pca(speciesCpm.5, "CPM", 5)
            # plot.pca(speciesRpkm.01, "RPKM", 0.1)
            # plot.pca(speciesRpkm.05, "RPKM", 0.5)
            # plot.pca(speciesRpkm.1, "RPKM", 1)
            # plot.pca(speciesRpkm.5, "RPKM", 5)
        dev.off()

    } else { 
        print("No plots generated. If you want descriptive plots, set plotMe to TRUE")
    }

    return(speciesCounts)
}

# chimpCounts <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_200/", "chimp_counts_matrix_hg19_200_windows_clean.out", "chimp", FALSE)
# humanCounts <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_200/", "human_counts_matrix_hg19_200_windows_clean.out", "human", FALSE)

chimp100 <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100/", "chimp_counts_matrix_hg19_windows_clean.out", "chimp", FALSE)
human100 <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100/", "human_counts_matrix_hg19_windows_clean.out", "human", FALSE)

# chimp100old <- qcBySpecies("~/atac_seq/featurecounts/atac_seq", "chimp_counts_matrix_hg19_windows_clean.out", "chimp100", FALSE)
# human100old <- qcBySpecies("~/atac_seq/featurecounts/atac_seq", "human_counts_matrix_hg19_windows_clean.out", "human100", FALSE)

# New files: Ensembl 89, hg19 and panTro3, bg regions matched for density (mostly)
chimpTSS <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100", "chimp_counts_matrix_ortho_TSS_clean.out", "chimp_TSS", TRUE)
chimpBG <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100", "chimp_counts_matrix_ortho_bg_clean.out", "chimp_bg", TRUE)

humanTSS <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100", "human_counts_matrix_ortho_TSS_clean.out", "human_TSS", TRUE)
humanBG <- qcBySpecies("~/atac_seq/featurecounts/atac_seq_100", "human_counts_matrix_ortho_bg_clean.out", "human_bg", TRUE)


###################################################################
### 2. MERGE THE TWO SPECIES, WRITE OUTFILES FOR OTHER SCRIPTS. ###
###################################################################

### Note that lines 213 and 218 introduce filtering criteria for column names - edit as needed.

mergeSpecies <- function(species1, species2, dir_out, file_out, trimChoice){

    # Get ready:
    setwd(dir_out)

    allCounts <- merge(species1, species2[,c(1,7:dim(species2)[2]), with=F], by.x="Geneid", by.y="Geneid", all=T)
    # rm(species2, species1)

    # Drop rows with 0 reads.
    if (trimChoice == TRUE) {
        allTrim <- subset(allCounts, rowSums(allCounts[,7:dim(species1)[2], with=F]) > 0 | rowSums(allCounts[,(dim(species1)[2]+1):dim(allCounts)[2], with=F]) > 0,)
        print(paste0(round(dim(allTrim)[1]/dim(allCounts)[1], digits=3)*100, "% of windows contain at least one read in at least one species. Discarding completely empty windows."))
    } else {
        allTrim <- allCounts 
        print(paste0(round(dim(subset(allCounts, rowSums(allCounts[,7:dim(species1)[2], with=F]) > 0 | rowSums(allCounts[,(dim(species1)[2]+1):dim(allCounts)[2], with=F]) > 0,))[1]/dim(allCounts)[1], digits=3)*100, "% of windows contain at least one read in at least one species. Because you set trimChoice to False, empty windows are being kept."))
    }

    allCpm <- cpm(allTrim[,7:dim(allTrim)[2], with=F], log=T, prior.count=0.25)
    rownames(allCpm) <- allTrim$Geneid
    pdf(file=paste0("plots/", file_out, "_before_merge_cpm_distributions.pdf"))
        plotDensities(allCpm)
    dev.off()

    # Writing out counts and CPM matrices
    print(paste0("Writing out counts and CPM matrices to ", dir_out, file_out, "_[etc]"))

    saveRDS(allCpm, file=paste0(file_out, "_all_cpm_before_merge.RDS"))
    saveRDS(allTrim, file=paste0(file_out, "_all_counts_before_merge.RDS"))

    # Note that these quantiles can include some lines we would not want to keep - the Greenleaf stuff and day 20 experiments. OTOH the Greenleaf libraries are useful as an internal control - it is amazing how much more reproducible they are than our ATAC-seq stuff, suggesting the protocol is terribly noisy in my hands. Right now the CPM is is calculated relative to the relevant subset, but all libraries are kept at the end.
    print("Calculating CPM distribution by species")
    species1CpmMedian <- rowMedians(allCpm[,(grepl("^Hutt|^H[0-9]", colnames(allCpm)) & !(grepl("_day20", colnames(allCpm))))]) 
    species1CpmQuant <- quantile(species1CpmMedian, probs=c(0.95, 0.99, 0.995, 0.999))
    print("Trimmed quantiles for first species:")
    print(species1CpmQuant)

    species2CpmMedian <- rowMedians(allCpm[,(grepl("^C", colnames(allCpm)) & !(grepl("_day20", colnames(allCpm))))])
    species2CpmQuant <- quantile(species2CpmMedian, probs=c(0.95, 0.99, 0.995, 0.999))
    print("Trimmed quantiles for second species:")
    print(species2CpmQuant)

    names(species1CpmMedian) <- allTrim[,Geneid]
    names(species2CpmMedian) <- allTrim[,Geneid]

    # Pull union of top x% of windows in either species
    print("Retrieving and plotting top CPM winwdows across species")
    allCpm.01 <- allCpm[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[4]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[4]]))),]
    allCpm.05 <- allCpm[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[3]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[3]]))),]
    allCpm.1 <- allCpm[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[2]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[2]]))),]
    allCpm.5 <- allCpm[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[1]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[1]]))),]

    ### Look at the median CPM distribution for all the windows in the top quantiles - again without Greenleaf etc:
    pdf(file=paste0("plots/", file_out, "_before_merge_median_cpm.pdf"))
        smoothScatter(species1CpmMedian[rownames(allCpm.01)], species2CpmMedian[rownames(allCpm.01)], xlab="log2 species1 median CPM", ylab="log2 species2 median CPM", main="top 0.1%")
        abline(a=0, b=1, col="red", lty=2)
        legend(x="topright", legend=round(cor(species1CpmMedian[rownames(allCpm.01)], species2CpmMedian[rownames(allCpm.01)], method="spearman"), digits=4), bty="n")

        smoothScatter(species1CpmMedian[rownames(allCpm.05)], species2CpmMedian[rownames(allCpm.05)], xlab="log2 species1 median CPM", ylab="log2 species2 median CPM", main="top 0.5%")
        abline(a=0, b=1, col="red", lty=2)
        legend(x="topright", legend=round(cor(species1CpmMedian[rownames(allCpm.05)], species2CpmMedian[rownames(allCpm.05)], method="spearman"), digits=4), bty="n")

        smoothScatter(species1CpmMedian[rownames(allCpm.1)], species2CpmMedian[rownames(allCpm.1)], xlab="log2 species1 median CPM", ylab="log2 species2 median CPM", main="top 1%")
        abline(a=0, b=1, col="red", lty=2)
        legend(x="topright", legend=round(cor(species1CpmMedian[rownames(allCpm.1)], species2CpmMedian[rownames(allCpm.1)], method="spearman"), digits=4), bty="n")

        smoothScatter(species1CpmMedian[rownames(allCpm.5)], species2CpmMedian[rownames(allCpm.5)], xlab="log2 species1 median CPM", ylab="log2 species2 median CPM", main="top 5%")
        abline(a=0, b=1, col="red", lty=2)
        legend(x="topright", legend=round(cor(species1CpmMedian[rownames(allCpm.5)], species2CpmMedian[rownames(allCpm.5)], method="spearman"), digits=4), bty="n")
    dev.off()

    ### Now doing the same with counts, to see how many of them are in the top x% windows as a control of sorts. 
    print("Retrieving and plotting top counts winwdows across species")
    allCounts.01 <- allCounts[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[4]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[4]]))),]
    allCounts.05 <- allCounts[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[3]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[3]]))),]
    allCounts.1 <- allCounts[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[2]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[2]]))),]
    allCounts.5 <- allCounts[allTrim$Geneid %in% unique(c(names(species1CpmMedian[species1CpmMedian >= species1CpmQuant[1]]), names(species2CpmMedian[species2CpmMedian >= species2CpmQuant[1]]))),]

    #Some plots before merging:
    allSums <- colSums(allCounts[,7:dim(allCounts)[2], with=F])
    allSums.01 <- colSums(allCounts.01[,7:dim(allCounts)[2], with=F])
    allSums.05 <- colSums(allCounts.05[,7:dim(allCounts)[2], with=F])
    allSums.1 <- colSums(allCounts.1[,7:dim(allCounts)[2], with=F])
    allSums.5 <- colSums(allCounts.5[,7:dim(allCounts)[2], with=F])

    pdf(file=paste0("plots/", file_out, "_before_merge_counts_depth_boxplots.pdf"))
        boxplot(list(allSums/1000000, allSums.5/1000000, allSums.1/1000000, allSums.05/1000000, allSums.01/1000000), ylab="Millions of reads after QC", names=c("All reads", "Top 5% windows", "Top 1% windows", "Top 0.5% windows,", "Top 0.1% windows")) 
        boxplot(list(allSums.5/allSums, allSums.1/allSums, allSums.05/allSums, allSums.01/allSums), ylab="Fraction of reads after QC", names=c("Top 5% windows", "Top 1% windows", "Top 0.5% windows,", "Top 0.1% windows")) 
    dev.off()

}

mergeSpecies(chimp100, human100, "~/atac_seq/featurecounts/atac_seq_100/", "all_samples_100", TRUE) # Chimps are species 1 so that we can filter out X chromosome in both species using the Geneid (human info) and the Chr (chimp info) columns

# Clean up to handle the memory issues...
rm(chimp100, human100)

mergeSpecies(chimpTSS, humanTSS, "~/atac_seq/featurecounts/atac_seq_100/", "all_samples_tss", FALSE) # Chimps are species 1 so that we can filter out X chromosome in both species using the Geneid (human info) and the Chr (chimp info) columns

mergeSpecies(chimpBG, humanBG, "~/atac_seq/featurecounts/atac_seq_100/", "all_samples_bg", FALSE) # Chimps are species 1 so that we can filter out X chromosome in both species using the Geneid (human info) and the Chr (chimp info) columns


