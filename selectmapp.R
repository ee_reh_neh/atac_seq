# Modified from Xiang's selectmapp.R to filter only to human and chimp rather than three-way species orthology. 
# IGR 06.19.2015
# Fixed bug in TF file call 27.11.15

args<-commandArgs(TRUE)

TF=args[1]
filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".H.filt.bed", sep="")
H_bed=read.table(filename)
filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".C.filt.bed", sep="")
C_bed=read.table(filename)

filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".H.mapp.txt", sep="")
H_mapp=read.table(filename)
filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".C.mapp.txt", sep="")
C_mapp=read.table(filename)

s=(H_mapp$V1>0.8)&(C_mapp$V1>0.8)

filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".H.final.bed", sep="")
write.table(H_bed[s,], filename, col.names=F, row.names=F, quote=F, sep="\t")

filename=paste("/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif/", TF, ".C.final.bed", sep="")
write.table(C_bed[s,], filename, col.names=F, row.names=F, quote=F, sep="\t")

