#! /bin/bash

## File: mapping.stats.and.featurecounts.sh 
## Author: IGR
## Date: 22.03.15
## This code is used to take a list of qc-ed bam files 
## and generate quality plots and feature counts
## from them. It takes 2 arguments-
## $1: The path to the chosen bam files. *required*
## $2: The path to the featureCounts outdirectory. *required*

picardpath="/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar"
java="/usr/bin/java"
samtools="/usr/local/bin/samtools"
qcdir="/mnt/gluster/data/internal_supp/atac_seq/mapped/atac_seq/mapping_qc_200"
bamtools="/mnt/lustre/home/shyamg/bin/bamtools"
featurecounts="/mnt/gluster/home/ireneg/bin/subread-1.4.6-p1-Linux-x86_64/bin/featureCounts"

chimpindex="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3/panTro3.autoXM.no_Y_random.fa"
humanindex="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19/hg19.autoXM.no_Y_random.fa"

# chimpwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win100.panTro3.0.8.mapp.0.8.newstart.saf"
# humanwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win100.hg19.0.8.mapp.0.8.newstart.saf"
# indextype="hg19_windows"

# chimpwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win200.panTro3.0.8.mapp.0.8.newstart.saf"
# humanwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win200.hg19.0.8.mapp.0.8.newstart.saf"
# indextype="hg19_200_windows"

# chimpwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win100.panTro3.0.8.mapp.0.8.newstart.saf"
# humanwindows="/mnt/gluster/data/internal_supp/atac_seq/liftOvers/hg19.autoX.win100.hg19.0.8.mapp.0.8.newstart.saf"
# indextype="panTro3_windows"

chimpwindows="/mnt/gluster/home/ireneg/atac_seq/liftOvers/orthoTSS/panTro3_ensembl89_filteredTSS_2kb.saf"
humanwindows="/mnt/gluster/home/ireneg/atac_seq/liftOvers/orthoTSS/hg19_ensembl89_filteredTSS_2kb.saf"
indextype="ortho_TSS"

# chimpwindows="/mnt/gluster/home/ireneg/atac_seq/liftOvers/ortho_bg_new/panTro3_ensembl89_filteredBG_2kb.saf"
# humanwindows="/mnt/gluster/home/ireneg/atac_seq/liftOvers/ortho_bg_new/hg19_ensembl89_filteredBG_2kb.saf"
# indextype="ortho_bg"


if [ $# -ne 2 ]; then
    echo "USAGE: mapping.stats.and.featurecounts.sh InputBamDirectory OutputFeatureCountsDirectory"
    exit 1
fi

bamdirectory=$1
fcdirectory=$2

if [ ! -d $qcdir/logs ]; then
    mkdir -p $qcdir/logs
elif [ ! -d $fcdirectory/logs ]; then
    mkdir -p $fcdirectory/logs
fi

for tsampName in $bamdirectory/*.merged.nochrM.rmdup.filter.bam; do
    sampName=`basename $tsampName .merged.nochrM.rmdup.filter.bam`

# First, qsub the two picard jobs that can handle the METRIC_ACCUMULATION_LEVEL argument: 
# CollectInsertSizeMetrics 
    echo -e "#! /bin/bash

    echo 'Calculating fragment size distribution for $sampName.'
    if [ -s $qcdir/$sampName.processed.fragments.out ]; then
        echo 'Fragment size distribution for $sampName has already been calculated.'
    else
        # Launch CollectInsertSizeMetrics
        # Unintuitevely, we do not need to use the READ_GROUP option in METRIC_ACCUMULATION_LEVEL, because picard does not parse reads on the basis of their individual RG tag. 
        # Instead, the RG:ID tag is used only to decipher the rest of the tags in the RG header, and reads are split according to those. 
        # READ_GROUP corresponds to the PU tag in the header, which itself stands for 'platform unit'.
        # See also: http://sourceforge.net/p/samtools/mailman/message/32186480/ 
        # This is a bit silly but in this case can be easily solved by compiling metrics at the library level.
        $java -Xmx3g -jar $picardpath CollectInsertSizeMetrics INPUT=$tsampName OUTPUT=$qcdir/$sampName.processed.fragments.out HISTOGRAM_FILE=$qcdir/$sampName.processed.fragments.pdf METRIC_ACCUMULATION_LEVEL=ALL_READS METRIC_ACCUMULATION_LEVEL=SAMPLE METRIC_ACCUMULATION_LEVEL=LIBRARY VALIDATION_STRINGENCY=LENIENT ASSUME_SORTED=TRUE

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed CollectInsertSizeMetrics with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully calculated fragment size distribution for $sampName.
        fi
    fi
    " | qsub -l h_vmem=4g -o $qcdir/logs/$sampName.fragments.processed.out -N insertsize.$sampName -cwd -j y 

# CollectAlignmentSummaryMetrics
    echo -e "#! /bin/bash

    echo 'Calculating alignment summary metrics for $sampName.'
    if [ -s $qcdir/$sampName.processed.alignment.out ]; then
        echo 'Alignment summary metrics for $sampName have already been calculated.'
    else
        # Launch CollectAlignmentSummaryMetrics
        $java -Xmx3g -jar $picardpath CollectAlignmentSummaryMetrics INPUT=$tsampName OUTPUT=$qcdir/$sampName.processed.alignment.out  METRIC_ACCUMULATION_LEVEL=ALL_READS METRIC_ACCUMULATION_LEVEL=SAMPLE METRIC_ACCUMULATION_LEVEL=LIBRARY VALIDATION_STRINGENCY=LENIENT ASSUME_SORTED=TRUE

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed CollectAlignmentSummaryMetrics with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully calculated alignment summary metrics for $sampName.
        fi
    fi
    " | qsub -l h_vmem=4g -o $qcdir/logs/$sampName.alignment.processed.out -N alignment.$sampName -cwd -j y

# Split files by read group using bamtools. 
# While these jobs are qsubbed, the variable waitlist is populated with the names of all the jobs, so that featureCounts down below can wait on the successful completion of all of them. 
    echo -e "#! /bin/bash

    if ls $bamdirectory/$sampName.merged.nochrM.rmdup.filter.TAG*; then
        echo 'Sample $sampName has already been split.'
    else
        echo 'Splitting $sampName by read group.'
        $bamtools split -tag RG -in $tsampName

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to split bam file with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully split $sampName.
        fi
    fi
    " | qsub -l h_vmem=8g -o $qcdir/logs/$sampName.split.processed.out -N split.$sampName -hold_jid insertsize.$sampName,alignment.$sampName -cwd -j y

# The splitting command creates 1-2 files per original file with suffixes that cannot be systematically predicted (given the multiple library and flow cell combinations)
# Therefore, need to look at list of files and push each TAG_RG file per sample 
# Because all split files write to the same log file they need to be run sequentially. The job also cannot begin until $sampName.split above is done. 
# The easiest solution, therefore, is to wrap everything into a single script and qsub the fetching of names and serial running CollectGcBiasMetrics as needed.

    # Populate the array with names of split files for each sample and then launch the qsub jobs. 
    tsplitNames=($bamdirectory/$sampName.merged.nochrM.rmdup.filter.TAG*)

    # Because the log files get written serially, it is easier (for me!) to run this loop on a counter, rather than on the element name. 
    for ((i=0; i < ${#tsplitNames[@]}; i++)); do

        tsplitFile=${tsplitNames[$i]}
        splitName=`basename $tsplitFile`

        if echo $splitName | grep -q "^C"; then
            speciesindex=$chimpindex
        elif echo $splitName | grep -q "^H"; then
            speciesindex=$humanindex    
        fi    

        if [ -s $qcdir/$splitName.gcbias.out ]; then
            echo 'Sample $splitName has already analysed for GC content bias.'
        else
            # Dynamically generate the job ID name to pass to -hold_jid and -N below so the jobs can be queued successfully.
            # Also begin to populate featurecountshold, which will be used below.
            if [ $i -gt 0 ]; then
                j=$(($i - 1))
                tPrevious=${tsplitNames[$j]}
                previousName=`basename $tPrevious`
                holdjob=gcbias.$previousName.$j
            else 
                holdjob=split.$sampName
            fi

            echo -e "#! /bin/bash

            if [ $i -gt 0 ]; then
                echo 'Calculating GC bias for \$splitName.'
                echo 'This is not the first split file, will wait to run until the previous GC file is done.'
            else 
                echo 'Calculating GC bias for \$splitName.'
                echo 'This is the first split file, will wait to run until bamtools split command finishes.'
            fi

            # Launch GC content calculation
            $java -Xmx6g -jar $picardpath CollectGcBiasMetrics INPUT=$tsplitFile REFERENCE_SEQUENCE=$speciesindex OUTPUT=$qcdir/$splitName.gcbias.out CHART_OUTPUT=$qcdir/$splitName.gcbias.chart.pdf SUMMARY_OUTPUT=$qcdir/$splitName.gcbias.summary.out WINDOW_SIZE=100 ASSUME_SORTED=TRUE VALIDATION_STRINGENCY=LENIENT 

            echo 'The qsub command is qsub -l h_vmem=8g -o $qcdir/logs/$sampName.split.processed.out -N gcbias.$splitName.$i -hold_jid $holdjob -cwd -j y'

            exitstatus=( \$? )
            if [ \${exitstatus}  != 0 ]; then
                echo ERROR: Failed to calculate GC content bias metrics for $splitName with exit code \${exitstatus}
                echo Running on $HOSTNAME
                exit \${exitstatus}
            else 
                echo Successfully estimated GC content bias for $splitName.
            fi
            " | qsub -l h_vmem=8g -o $qcdir/logs/$sampName.split.processed.out -N gcbias.$splitName.$i -hold_jid $holdjob -cwd -j y

        fi 
    
    done
        
done

# Finally, launch featureCounts for each species. 
# Because featureCounts handles multiple files at once it is outside the loop and relies on waitlist to exit its initial hold status, and on the loop above to get the list of needed files. 

# First generate the list of the split file names for each species: split.[basic sample id] 
for longWaitlist in $bamdirectory/*.merged.nochrM.rmdup.filter.bam; do
    sampName=`basename $longWaitlist .merged.nochrM.rmdup.filter.bam`
    if echo $sampName | grep -q "^C"; then
        chimpWaitlist=split.$sampName,$chimpWaitlist
    elif echo $sampName | grep -q "^H"; then
        humanWaitlist=split.$sampName,$humanWaitlist
    fi    
done


# Then get the list of split files, again by species:
for longfcNames in $bamdirectory/*.merged.nochrM.rmdup.filter.TAG*; do
    sampfcName=`basename $longfcNames .merged.nochrM.rmdup.filter.TAG*`
    if echo $sampfcName | grep -q "^C"; then
        cfcNames="$longfcNames $cfcNames"
    elif echo $sampfcName | grep -q "^H"; then
        hfcNames="$longfcNames $hfcNames"
    fi    
done

#After that, the commands are very straightforward:
    # Chimpanzees:
    echo -e "#! /bin/bash

    echo 'Launching featureCounts for all chimpanzee samples.'
    if [ -s $fcdirectory/chimpanzee_counts_matrix_$indextype.out ]; then
        echo 'featureCounts has already been run for this species.'
    else
        # Launch featureCounts
        $featurecounts --read2pos 5 -F SAF -o $fcdirectory/chimp_counts_matrix_$indextype.out -a $chimpwindows $cfcNames 
        sed 's:\/::g' $fcdirectory/chimp_counts_matrix_$indextype.out | sed 's/mntglusterhomeirenegatac_seqmappedatac_seqfiltered//g' | sed 's/.merged.nochrM.rmdup.filter.TAG_RG_//g' | sed 's/\.bam//g' > $fcdirectory/chimp_counts_matrix_${indextype}_clean.out

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed featureCounts with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran featureCounts for all chimpanzee samples.
        fi
    fi
    " | qsub -l h_vmem=10g -o $fcdirectory/logs/chimp_featureCounts_$indextype.out -N fc.chimp.$indextype -hold_jid $chimpWaitlist -cwd -j y -V 

    # Human
    echo -e "#! /bin/bash

    echo 'Launching featureCounts for all human samples.'
    if [ -s $fcdirectory/human_counts_matrix_$indextype.out ]; then
        echo 'featureCounts has already been run for this species.'
    else
        # Launch featureCounts
        $featurecounts --read2pos 5 -F SAF -o $fcdirectory/human_counts_matrix_$indextype.out -a $humanwindows $hfcNames 
        sed 's:\/::g' $fcdirectory/human_counts_matrix_$indextype.out | sed 's/mntglusterhomeirenegatac_seqmappedatac_seqfiltered//g' | sed 's/.merged.nochrM.rmdup.filter.TAG_RG_//g' | sed 's/\.bam//g' > $fcdirectory/human_counts_matrix_${indextype}_clean.out


        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed featureCounts with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran featureCounts for all human samples.
        fi
    fi
    " | qsub -l h_vmem=10g -o $fcdirectory/logs/human_featureCounts_$indextype.out -N fc.human.$indextype -hold_jid $humanWaitlist -cwd -j y -V 
