#!/bin/sh
#$ -t 1-422
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/mergeMotif.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/mergeMotif.error
LIFTOVER=/mnt/lustre/data/share/HCR_Chipseq/bin/liftOver/liftOver
CHAINHC=/data/share/tridnase/LiftOverChain/hg19ToPanTro3.filtered.chain.gz
CHAINCR=/data/share/tridnase/LiftOverChain/panTro3ToRheMac2.filtered.chain.gz
CHAINHR=/data/share/tridnase/LiftOverChain/hg19ToRheMac2.filtered.chain.gz  
CHAINRH=/data/share/tridnase/LiftOverChain/rheMac2ToHg19.filtered.chain.gz
CHAINCH=/data/share/tridnase/LiftOverChain/panTro3ToHg19.filtered.chain.gz  
CHAINRC=/data/share/tridnase/LiftOverChain/rheMac2ToPanTro3.filtered.chain.gz
BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin
INPATHH=/data/share/tridnase/human/motifSitesReScan
INPATHC=/data/share/tridnase/chimp/motifSitesReScan
INPATHR=/data/share/tridnase/rhesus/motifSitesReScan
OUTPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse/Motif

i=0
for FILE in `ls ${INPATHH}/*.bed.gz`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/.bed.gz/\n/'))

let i=$i+1
if ((i==${SGE_TASK_ID})); then
cd ${OUTPATH}
zcat ${INPATHH}/${TF}.bed.gz | awk -v id=${TF} 'BEGIN{OFS="\t"}{print $1, $2, $3, id"-"NR, $5, $6}' >  ${OUTPATH}/${TF}_H.map.bed
${LIFTOVER} ${OUTPATH}/${TF}_H.map.bed ${CHAINHC} ${OUTPATH}/${TF}_HC.map.bed ${OUTPATH}/${TF}_HC.unmap.bed
${LIFTOVER} ${OUTPATH}/${TF}_H.map.bed ${CHAINHR} ${OUTPATH}/${TF}_HR.map.bed ${OUTPATH}/${TF}_HR.unmap.bed
${BEDTOOLS}/intersectBed -a ${OUTPATH}/${TF}_HC.map.bed -b ${INPATHC}/${TF}.bed.gz -wa -wb -f 1 -r | awk 'BEGIN{OFS="\t"}{print $7, $8, $9, $4, $11, $12}' > ${OUTPATH}/${TF}_C.map.bed
${BEDTOOLS}/intersectBed -a ${OUTPATH}/${TF}_HR.map.bed -b ${INPATHR}/${TF}.bed.gz -wa -wb -f 1 -r | awk 'BEGIN{OFS="\t"}{print $7, $8, $9, $4, $11, $12}' > ${OUTPATH}/${TF}_R.map.bed

t=0
while read ITEM; do
ARR=($(echo ${ITEM}))
ID=${ARR[3]}
line1=`cat ${OUTPATH}/${TF}_C.map.bed | grep -w ${ID} | wc -l ${INFILE} | cut -f1 -d' '`
line2=`cat ${OUTPATH}/${TF}_R.map.bed | grep -w ${ID} | wc -l ${INFILE} | cut -f1 -d' '`
if [ ${line1} -eq 1 ] && [ ${line2} -eq 1 ]; then
let t=$t+1
cat ${OUTPATH}/${TF}_H.map.bed | grep -w ${ID} | awk -v id=${TF} -v s=${t} 'BEGIN{OFS="\t"}{print $1, $2, $3, id"-"s, $5, $6}' >> ${OUTPATH}/${TF}_H.bed
cat ${OUTPATH}/${TF}_C.map.bed | grep -w ${ID} | awk -v id=${TF} -v s=${t} 'BEGIN{OFS="\t"}{print $1, $2, $3, id"-"s, $5, $6}' >> ${OUTPATH}/${TF}_C.bed
cat ${OUTPATH}/${TF}_R.map.bed | grep -w ${ID} | awk -v id=${TF} -v s=${t} 'BEGIN{OFS="\t"}{print $1, $2, $3, id"-"s, $5, $6}' >> ${OUTPATH}/${TF}_R.bed
fi
done < ${OUTPATH}/${TF}_H.map.bed

rm ${OUTPATH}/${TF}_*map.bed

fi
done
