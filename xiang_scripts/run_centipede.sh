#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/run_centipede.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/run_centipede.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse
CPATH=/mnt/lustre/home/xz7/git/centipede/

i=0
for FILE in `ls ${INPATH}/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then
source ~/.bashrc
cd ${CPATH}/src

SPECIES=H
if [ ! -s ${CPATH}/cache/${TF}_${SPECIES}_candidate_sites.txt.gz ]; then
cat ${INPATH}/Motif/${TF}_${SPECIES}_final.bed | awk 'BEGIN{OFS="\t"}{if ($1!="chrY") print $1, $2, $3, $6, $5}' > ${CPATH}/cache/${TF}_${SPECIES}_candidate_sites.txt
gzip -f ${CPATH}/cache/${TF}_${SPECIES}_candidate_sites.txt
fi
for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
if [ ! -s ${CPATH}/cache/${TF}_${idv}_binding_sites.bed.gz ]; then
python ./call_binding.py --infer --track="hcr_dnase/${idv}" --sites="${TF}_${SPECIES}" --output="${TF}_${idv}"
python ./call_binding.py --decode --track="hcr_dnase/${idv}" --sites="${TF}_${SPECIES}" --model="${TF}_${idv}" --output="${TF}_${idv}"
fi
done

fi
done