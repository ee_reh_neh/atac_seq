#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscoverage.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscoverage.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
WINDOW_C=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Chimp_Window_2kb.bed
WINDOW_H=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Human_Window_2kb.bed
WINDOW_R=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Rhesus_Window_2kb.bed
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse

i=0
for FILE in `ls ${INPATH}/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then

for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
if [ ! -s ${INPATH}/Coverage/${TF}_TSS_H_${idv}.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/Coverage/${TF}_H_${idv}.bed -a ${WINDOW_H} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_H_${idv}.txt
fi
done

for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
if [ ! -s ${INPATH}/Coverage/${TF}_TSS_C_${idv}.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/Coverage/${TF}_C_${idv}.bed -a ${WINDOW_C} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_C_${idv}.txt
fi
done

for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
if [ ! -s ${INPATH}/Coverage/${TF}_TSS_R_${idv}.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/Coverage/${TF}_R_${idv}.bed -a ${WINDOW_R} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_R_${idv}.txt
fi
done

fi
done
