#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/calcmapp.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/calcmapp.error
XB=/data/tools/ucsctools/extractBedFromXbFile
MAPFILEH=/data/share/tridnase/human/hg19index/crossMapp.x8b
MAPFILEC=/data/share/tridnase/chimp/panTro3index/crossMapp.x8b
MAPFILER=/data/share/tridnase/rhesus/rheMac2index/crossMapp.x8b
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse/Motif

i=0
for FILE in `ls ${INPATH}/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then
cd ${INPATH}
${XB} ${MAPFILEH} ${INPATH}/${TF}_H.bed stdout -window=100 -readsize=22 | awk '{s=0; for (i=1; i<=NF; i++) {if ($i==1) {s++}}; print s/NF }' > ${INPATH}/${TF}_H.mapp.txt
${XB} ${MAPFILEC} ${INPATH}/${TF}_C.bed stdout -window=100 -readsize=22 | awk '{s=0; for (i=1; i<=NF; i++) {if ($i==1) {s++}}; print s/NF }' > ${INPATH}/${TF}_C.mapp.txt
${XB} ${MAPFILER} ${INPATH}/${TF}_R.bed stdout -window=100 -readsize=22 | awk '{s=0; for (i=1; i<=NF; i++) {if ($i==1) {s++}}; print s/NF }' > ${INPATH}/${TF}_R.mapp.txt
fi
done
