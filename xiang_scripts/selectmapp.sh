#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/selectmapp.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/selectmapp.error
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse/Motif
RCODE=/data/share/HCR_Chipseq/Mapped/DNAse/scripts/selectmapp.R

i=0
for FILE in `ls ${INPATH}/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then
Rscript --verbose ${RCODE} ${TF}
fi
done
