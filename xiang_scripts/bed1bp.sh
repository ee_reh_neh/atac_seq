#!/bin/sh                                                                                                                                                                                
#$ -t 1-23                                                                                                                                                                             
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bed1bp.out                                                                                                          
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bed1bp.error                                                                                                        

INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse
i=0
for idv in H_NA19239 H_NA19141 H_NA18505 H_NA19238 H_NA19193 H_NA18508 H_NA18507 H_NA18522 C_Pt30 C_Pt91 C_3659 C_18358 C_18359 C_3610 C_4973 R_181-96 R_249-97 R_290-96 R_153-99 R_150-99 R_265-95 R_303-97 R_256-95
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
if [ ! -s ${INPATH}/Mapped/${idv}_1bp.sort.bed.gz ]; then
zcat ${INPATH}/Mapped/${idv}.sort.bed.gz | awk 'BEGIN{OFS="\t"}{if ($6=="+") {print $1, $2, $2+1, $4, $5, $6} else {print $1, $3-1, $3, $4, $5, $6} }' \
> ${INPATH}/Mapped/${idv}_1bp.sort.bed
gzip -f ${INPATH}/Mapped/${idv}_1bp.sort.bed
fi
fi
done
