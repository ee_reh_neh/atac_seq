#!/bin/sh
#$ -t 1-23
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam_sortindex.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam_sortindex.error

HUMANPATH=/data/share/tridnase/Mapping/HumanLanes/BAM_indiv_nb
CHIMPPATH=/data/share/tridnase/Mapping/ChimpLanes/BAM_indiv_nb
RHESUSPATH=/data/share/tridnase/Mapping/RhesusLanes/BAM_indiv_nb
OUTPATH=/data/share/HCR_Chipseq/Mapped/DNAse/Mapped
SAMTOOLS=/data/share/HCR_Chipseq/bin/samtools-0.1.18/samtools

let i=0
INPATH=${HUMANPATH}
for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${SAMTOOLS} sort ${INPATH}/${idv}.bam ${OUTPATH}/H_${idv}.sort
${SAMTOOLS} index ${OUTPATH}/H_${idv}.sort.bam
${SAMTOOLS} view -b -o ${OUTPATH}/H_${idv}.sort.noY.bam ${OUTPATH}/H_${idv}.sort.bam chr{1..22} chrX
fi
done

INPATH=${CHIMPPATH}
for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${SAMTOOLS} sort ${INPATH}/${idv}.bam ${OUTPATH}/C_${idv}.sort
${SAMTOOLS} index ${OUTPATH}/C_${idv}.sort.bam
${SAMTOOLS} view -b -o ${OUTPATH}/C_${idv}.sort.noY.bam ${OUTPATH}/C_${idv}.sort.bam chr1 chr2A chr2B chr{3..22} chrX
fi
done


INPATH=${RHESUSPATH}
for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${SAMTOOLS} sort ${INPATH}/${idv}.bam ${OUTPATH}/R_${idv}.sort
${SAMTOOLS} index ${OUTPATH}/R_${idv}.sort.bam
${SAMTOOLS} view -b -o ${OUTPATH}/R_${idv}.sort.noY.bam ${OUTPATH}/R_${idv}.sort.bam chr{1..20} chrX
fi
done
