#!/bin/sh
#$ -t 1-162
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscoverage_subset.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscoverage_subset.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
WINDOW_C=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Chimp_Window_10kb.bed
WINDOW_H=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Human_Window_10kb.bed
WINDOW_R=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Rhesus_Window_10kb.bed
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse

i=0
for FILE in `ls ${INPATH}/DarrenSelect/*_H_select.bed.gz`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H_select.bed.gz/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then

for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
${BEDTOOLS}/intersectBed -a ${INPATH}/Coverage/${TF}_H_${idv}.bed -b ${INPATH}/DarrenSelect/${TF}_H_select.bed.gz -u > ${INPATH}/Coverage/${TF}_H_${idv}_subset.bed
cat ${INPATH}/Coverage/${TF}_H_${idv}_subset.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/intersectBed -b stdin -a ${WINDOW_H} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_H_${idv}_subset.txt
done

for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
${BEDTOOLS}/intersectBed -a ${INPATH}/Coverage/${TF}_C_${idv}.bed -b ${INPATH}/DarrenSelect/${TF}_C_select.bed.gz -u > ${INPATH}/Coverage/${TF}_C_${idv}_subset.bed
cat ${INPATH}/Coverage/${TF}_C_${idv}_subset.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/intersectBed -b stdin -a ${WINDOW_C} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_C_${idv}_subset.txt
done

for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
${BEDTOOLS}/intersectBed -a ${INPATH}/Coverage/${TF}_R_${idv}.bed -b ${INPATH}/DarrenSelect/${TF}_R_select.bed.gz -u > ${INPATH}/Coverage/${TF}_R_${idv}_subset.bed
cat ${INPATH}/Coverage/${TF}_R_${idv}_subset.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/intersectBed -b stdin -a ${WINDOW_R} -wa -wb | awk 'BEGIN{OFS="\t"}{print $4, $11}' > ${INPATH}/Coverage/${TF}_TSS_R_${idv}_subset.txt
done

fi
done