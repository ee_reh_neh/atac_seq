#!/bin/sh
#$ -t 1-23
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam2h5.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam2h5.error

BAM2H5=/mnt/lustre/home/xz7/git/genome/python/script/db/load_bam_5prime_ends.py
INPATH=/data/share/HCR_Chipseq/Mapped/DNAse/Mapped

let i=0
for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
source .bashrc
python ${BAM2H5} --assembly=hg19 hcr_dnase/${idv}_fwd hcr_dnase/${idv}_rev ${INPATH}/H_${idv}.sort.bam
fi
done

for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
source .bashrc
python ${BAM2H5} --assembly=panTro3 hcr_dnase/${idv}_fwd hcr_dnase/${idv}_rev ${INPATH}/C_${idv}.sort.bam
fi
done

for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
source .bashrc
python ${BAM2H5} --assembly=rheMac2 hcr_dnase/${idv}_fwd hcr_dnase/${idv}_rev ${INPATH}/R_${idv}.sort.bam
fi
done
