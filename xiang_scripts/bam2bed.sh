#!/bin/sh
#$ -t 1-23
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam2bed.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/bam2bed.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
INPATH=/data/share/HCR_Chipseq/Mapped/DNAse/Mapped

let i=0
for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${BEDTOOLS}/bamToBed -i ${INPATH}/H_${idv}.sort.bam > ${INPATH}/H_${idv}.sort.bed
gzip ${INPATH}/H_${idv}.sort.bed
fi
done

for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${BEDTOOLS}/bamToBed -i ${INPATH}/C_${idv}.sort.bam > ${INPATH}/C_${idv}.sort.bed
gzip ${INPATH}/C_${idv}.sort.bed
fi
done

for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
${BEDTOOLS}/bamToBed -i ${INPATH}/R_${idv}.sort.bam > ${INPATH}/R_${idv}.sort.bed
gzip ${INPATH}/R_${idv}.sort.bed
fi
done
