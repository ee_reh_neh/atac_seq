#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsspwm.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsspwm.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
WINDOW_C=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Chimp_Window_2kb.bed
WINDOW_H=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Human_Window_2kb.bed
WINDOW_R=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Rhesus_Window_2kb.bed
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse/Motif

i=0
for FILE in `ls /mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then

if [ ! -s ${INPATH}/${TF}_TSS_H_final.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/${TF}_H_final.bed -a ${WINDOW_H} -wa -wb | awk 'BEGIN{OFS="\t"}{if ($6==$12) {print $4, $11, 1} else {print $4, $11, -1} }' > ${INPATH}/${TF}_TSS_H_final.txt
fi

if [ ! -s ${INPATH}/${TF}_TSS_C_final.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/${TF}_C_final.bed -a ${WINDOW_C} -wa -wb | awk 'BEGIN{OFS="\t"}{if ($6==$12) {print $4, $11, 1} else {print $4, $11, -1} }' > ${INPATH}/${TF}_TSS_C_final.txt
fi

if [ ! -s ${INPATH}/${TF}_TSS_R_final.txt ]; then
${BEDTOOLS}/intersectBed -b ${INPATH}/${TF}_R_final.bed -a ${WINDOW_R} -wa -wb | awk 'BEGIN{OFS="\t"}{if ($6==$12) {print $4, $11, 1} else {print $4, $11, -1} }' > ${INPATH}/${TF}_TSS_R_final.txt
fi

fi
done
