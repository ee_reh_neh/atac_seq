#!/bin/sh
#$ -t 1-23
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscount.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/tsscount.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
WINDOW_C=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Chimp_Window_2kb.bed
WINDOW_H=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Human_Window_2kb.bed
WINDOW_R=/data/share/HCR_OrthoExonData/OrthoTSSWindow/Rhesus_Window_2kb.bed
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse

for idv in H_NA19239 H_NA19141 H_NA18505 H_NA19238 H_NA19193 H_NA18508 H_NA18507 H_NA18522 
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
if [ ! -s ${INPATH}/Count/TSS_${idv}.txt ]; then
${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/${idv}.sort.bed.gz -b ${WINDOW_H} | awk 'BEGIN{OFS="\t"}{print $4, $7}' > ${INPATH}/Count/TSS_${idv}.txt
fi
fi
done

for idv in C_Pt30 C_Pt91 C_3659 C_18358 C_18359 C_3610 C_4973 
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
if [ ! -s ${INPATH}/Count/TSS_${idv}.txt ]; then
${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/${idv}.sort.bed.gz -b ${WINDOW_C} | awk 'BEGIN{OFS="\t"}{print $4, $7}' > ${INPATH}/Count/TSS_${idv}.txt
fi
fi
done

for idv in R_181-96 R_249-97 R_290-96 R_153-99 R_150-99 R_265-95 R_303-97 R_256-95
do
let i=$i+1
if ((i==${SGE_TASK_ID})); then
if [ ! -s ${INPATH}/Count/TSS_${idv}.txt ]; then
${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/${idv}.sort.bed.gz -b ${WINDOW_R} | awk 'BEGIN{OFS="\t"}{print $4, $7}' > ${INPATH}/Count/TSS_${idv}.txt
fi
fi
done
