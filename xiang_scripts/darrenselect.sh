#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/darrenselect.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/darrenselect.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse
LISTSELECT=/data/share/HCR_Chipseq/Mapped/DNAse/DarrenSelect/hg19_10kbresultsplus_sorted.bed

i=0
for FILE in `ls ${INPATH}/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then

${BEDTOOLS}/intersectBed -a ${LISTSELECT} -b ${INPATH}/Motif/${TF}_H_final.bed -wa -wb -f 1 -r | awk 'BEGIN{OFS="\t"}{print $6, $7, $8, $9, $10, $11}' > ${INPATH}/DarrenSelect/${TF}_H_select.bed
if [ ! -s ${INPATH}/DarrenSelect/${TF}_H_select.bed ]; then
rm ${INPATH}/DarrenSelect/${TF}_H_select.bed
else
gzip -f ${INPATH}/DarrenSelect/${TF}_H_select.bed
fi

fi
done
