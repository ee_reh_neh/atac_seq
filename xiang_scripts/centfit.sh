#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/centfit.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/centfit.error
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse
RCODE=/data/share/HCR_Chipseq/Mapped/DNAse/scripts/centfit.R

i=0
for FILE in `ls ${INPATH}/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then
idvtotal1=$(find ${INPATH}/Centipede/ -maxdepth 1 -name "${TF}_*.txt.gz" | wc -l)
idvtotal2=$(find ${INPATH}/Centipede/ -maxdepth 1 -name "${TF}_*.bed.gz" | wc -l)
if [ ${idvtotal1} -eq 23 ] && [ ${idvtotal2} -lt 23 ]; then
Rscript --verbose ${RCODE} ${TF}
gzip ${INPATH}/Centipede/${TF}_*_centfit.bed
fi
fi
done