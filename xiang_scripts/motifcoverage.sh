#!/bin/sh
#$ -t 1-421
#$ -o /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/motifcoverage.out
#$ -e /data/share/HCR_Chipseq/Mapped/DNAse/output_sum/motifcoverage.error

BEDTOOLS=/mnt/lustre/data/share/HCR_Chipseq/bin/BEDTools-Version-2.14.3/bin/
INPATH=/mnt/lustre/data/share/HCR_Chipseq/Mapped/DNAse

i=0
for FILE in `ls ${INPATH}/Motif/*_H.bed`
do
FILE=`basename ${FILE}`
TF=($(echo ${FILE} | sed 's/_H.bed/\n/'))
let i=$i+1
if ((i==${SGE_TASK_ID})); then

for idv in NA19239 NA19141 NA18505 NA19238 NA19193 NA18508 NA18507 NA18522
do
if [ ! -s ${INPATH}/Coverage/${TF}_H_${idv}.bed ]; then
cat ${INPATH}/Motif/${TF}_H_final.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/H_${idv}.sort.bed.gz -b stdin | awk 'BEGIN{OFS="\t"}{print $1, $2, $3, $4, $7, $6}' > ${INPATH}/Coverage/${TF}_H_${idv}.bed
fi
done

for idv in Pt30 Pt91 3659 18358 18359 3610 4973
do
if [ ! -s ${INPATH}/Coverage/${TF}_C_${idv}.bed ]; then
cat ${INPATH}/Motif/${TF}_C_final.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/C_${idv}.sort.bed.gz -b stdin | awk 'BEGIN{OFS="\t"}{print $1, $2, $3, $4, $7, $6}' > ${INPATH}/Coverage/${TF}_C_${idv}.bed
fi
done

for idv in 181-96 249-97 290-96 153-99 150-99 265-95 303-97 256-95
do
if [ ! -s ${INPATH}/Coverage/${TF}_R_${idv}.bed ]; then
cat ${INPATH}/Motif/${TF}_R_final.bed | awk 'BEGIN{OFS="\t"}{print $1, $2-100, $3+100, $4, $5, $6}' | ${BEDTOOLS}/coverageBed -a ${INPATH}/Mapped/R_${idv}.sort.bed.gz -b stdin | awk 'BEGIN{OFS="\t"}{print $1, $2, $3, $4, $7, $6}' > ${INPATH}/Coverage/${TF}_R_${idv}.bed
fi
done

fi
done
