#!/usr/bin/env python

# File: getLiftedWindows.py
# Author: Shyam Gopalakrishnan
# Date: 24th March 2015
# This script is a replacement script for the
# grep process that was used to get final windows
# from each of the split processes. It makes the 
# window for the two species based on windows that
# got lifted to and fro from species1 to species2
# and back.
# Args:
# sys.argv[1]: initial bed file
# sys.argv[2]: firstLiftover bedfile
# sys.argv[3]: secondLiftover bedfile
# sys.argv[4]: outfile for species 1 name
# sys.argv[5]: outfile for species 2 name

# Edit date: 30th March 2015
# Author: Shyam Gopalakrishnan
# Added removal of _random chrs from final file
# and sorted the based on chromosome.
# Numerically for humans,
# Lexicographically for chimps.

import sys
import os

if (len(sys.argv) != 6):
    print 'Usage:', os.path.basename(sys.argv[0]), ' initBedFile firstLiftBed secondLiftBed species1OutBed species2OutBed'
    sys.exit(1)

## Species names are obtained from the output file names
species1 = os.path.basename(sys.argv[4]).split('.')[0]
species2 = os.path.basename(sys.argv[5]).split('.')[0]

if species1 == 'hg19':
    sp1_chrs = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX']
elif species1 == 'panTro3':
    sp1_chrs = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chr2A', 'chr2B', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']
elif species1 == 'rheMac3':
    sp1_chrs = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr2', 'chr20', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']
else:
    print 'Species 1 unknown:', species1

if species2 == 'hg19':
    sp2_chrs = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX']
elif species2 == 'panTro3':
    sp2_chrs = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chr2A', 'chr2B', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']
elif species2 == 'rheMac3':
    sp2_chrs = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr2', 'chr20', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']
else:
    print 'Species 2 unknown:', species2

# Main processing 
successWindows = []
secondLiftover = open(sys.argv[3])
# Read list of window names that got lifted to and fro successfully
for line in secondLiftover:
    if line.strip().split()[0] in sp1_chrs: # Only read the windows that are on a chr in the species 1 accepted chrs.
        successWindows.append(line.strip().split()[3])
secondLiftover.close()
print "Read successful windows:", len(successWindows), "from", sys.argv[3]

# Open first liftover file to make output file for species 2
# if line contains successful window, keep it else discard
cnt = 0
firstLiftover = open(sys.argv[2])
outlist = []
outspecies2 = open(sys.argv[5], "w")
for line in firstLiftover:
    toks = line.strip().split()
    if toks[3] in successWindows:
        if toks[0] in sp2_chrs: # only write if it is on a accepted chr of species 2
            outspecies2.write("\t".join(toks)+"\n")
        else: # if not on accepted chr of species2, remove from successful windows so that the species 1 bed file does not have this :)
            successWindows.remove(toks[3])
    cnt += 1
    if cnt%50000==0:
        print cnt, "first done."
firstLiftover.close()
outspecies2.close()
print "Wrote species2 successful windows."

# Open init bed file to make output file for species 1
# if line contains successful window, keep it else discard
cnt = 0
initbedfile = open(sys.argv[1])
outspecies1 = open(sys.argv[4], "w")
for line in initbedfile:
    toks = line.strip().split()
    if toks[3] in successWindows:
        outspecies1.write("\t".join(toks)+"\n")
    cnt += 1
    if cnt%50000==0:
        print cnt, "second done."
initbedfile.close()
outspecies1.close()
print "Wrote species1 successful windows."
