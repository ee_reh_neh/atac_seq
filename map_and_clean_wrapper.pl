#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

# IGR, 03.08.15
# For submission of mapping jobs with BWA and some downstream processing with picard tools.
# 1. Check through flowcell directories before mapping to make sure there's only one file per lane per ind per direction
# 2. Submit map_and_clean.pl for each lane/ind combination. 
#
# Assumes data are nested one directory deep given the reference path
# 1. C47ECACXX (run in the FGF core)
# 2. C47FKACXX (run by Claudia)
# 3. FC3: C5DGKACXX (Single individual, H1849)
# 4. FC4: (Single individual (H19114) at three cell depths, original flow cell ID unclear)
# 5. FC5: C6CD7ACXX (rerun of 8 individuals, bad lane 5)


# ONLY WORKS WITH NEW CASAVA

scalar @ARGV == 3 or die<<USAGE;
Usage:
perl -w map_and_clean_wrapper.pl [Path to flowcell] [Path to data outdir] [Desired flow cell ID]
USAGE

# Path to scripts repo - HARDCODED: 
my $scriptspath = "/mnt/gluster/home/ireneg/repos/atac_seq";

# Merge multiple R1 and R2 within a lane, generate 8 fastq.gz per individual:
my $flowcell = $ARGV[0];
my $outdir = $ARGV[1];
my $newfcid = $ARGV[2];

print "Concatenating files by individual by lane...\n";

# Read in directories and file names:
sub list
{
  my ($dir) = @_;
  return unless -d $dir;

  my @files;
  if (opendir my $dh, $dir)
  {
    # Capture entries first, so we don't descend with an
    # open dir handle.
    my @list;
    my $file;
    while ($file = readdir $dh)
    {
      push @list, $file;
    }
    closedir $dh;

    for $file (@list)
    {
      # Unix file system considerations.
      next if $file eq '.' || $file eq '..' || $file eq 'SampleSheet.csv';

      # Swap these two lines to follow symbolic links into
      # directories.  Handles circular links by entering an
      # infinite loop.
      push @files, "$dir/$file"        if -f "$dir/$file";
      push @files, concat ("$dir/$file") if -d "$dir/$file";
    }
  }

  return @files;
}

sub concat
{
  my ($dir) = @_;
  return unless -d $dir;

  my @files;
  if (opendir my $dh, $dir)
  {
    # Capture entries first, so we don't descend with an
    # open dir handle.
    my @list;
    my $file;
    while ($file = readdir $dh)
    {
      next if $file eq '.' || $file eq '..' || $file eq 'SampleSheet.csv';
      push @list, $file;
    }
    closedir $dh;

    # Prepare to map each lane - extract info and deal with the possibility of multiple files for each lane/ind combo
    # Test if file exists
    for my $lanecounter (1..8){
      
      my @R1reads; 
      my @R2reads;

      my $laneid = "L00" . $lanecounter;

      for my $fastq (@list){
       	if ($fastq =~ "R1" && $fastq =~ $laneid){
        	push @R1reads, $fastq;
        }
        elsif ($fastq =~ "R2" && $fastq =~ $laneid){
        	push @R2reads, $fastq;
        }
      }
      
      # Get sample name and check for "day20" (from FC1) or 100 and 50, from FC4:
      my @indname = split "_", $R1reads[0];
      my $indnamelong;
      if($indname[1] eq "day20"){
        $indnamelong = $indname[0] . "_day20";
      }
      elsif($indname[1] eq "100"){
        $indnamelong = $indname[0] . "_100";
      }
      elsif($indname[1] eq "50"){
        $indnamelong = $indname[0] . "_50";
      }
      else{$indnamelong = $indname[0];}

      # Some samples have multiple files per lane. Because this is unpredictable, check the length of R1reads
      # If > 1, do a system call to cat them and move to cat_$newfcid 
      # If = 1, simply make a symlink within cat_$newfcid

      my $catpath = $flowcell;
      my @splitfc = split(/\//, $catpath);
      my $fcid = pop(@splitfc);

      $catpath =~ s/$fcid/cat_$newfcid/;

      unless (-d $catpath){
        system("mkdir -p $catpath");
      }

      if(scalar @R1reads > 1){
        # Check if R1 file exists (potential problem: not checking R2):
        unless(-f "$catpath/$indnamelong.$newfcid.$laneid.R1.fastq.gz"){
          #Add the path back so that cat doesn't falter and make sure they are sorted in the same order:
          @R1reads = map {$_ =~ s/^/$flowcell\/Sample_$indnamelong\//; $_} @R1reads; 
          @R2reads = map {$_ =~ s/^/$flowcell\/Sample_$indnamelong\//; $_} @R2reads; 

          @R1reads = sort @R1reads;
          @R2reads = sort @R2reads;

          local $" = ' ';
          system("cat @R1reads > $catpath/$indnamelong.$newfcid.$laneid.R1.fastq.gz");   
          system("cat @R2reads > $catpath/$indnamelong.$newfcid.$laneid.R2.fastq.gz");   
          print("I concatenated files\n");
        }

      }
      else{
        unless(-l "$catpath/$indnamelong.$newfcid.$laneid.R1.fastq.gz"){
          system("ln -s $flowcell/Sample_$indnamelong/@R1reads $catpath/$indnamelong.$newfcid.$laneid.R1.fastq.gz");
          system("ln -s $flowcell/Sample_$indnamelong/@R2reads $catpath/$indnamelong.$newfcid.$laneid.R2.fastq.gz");
          print("I made new symlinks\n");
        }
      }

      # Pass information to the actual mapping script (species determination is done by mapping script):
      # ARGV[0]: Sample name ($indnamelong)
      # ARGV[1]: Path to fastq ($catpath)
      # ARGV[2]: outdir ($outdir)
      # ARGV[3]: Desired flowcell ID ($newfcid)  
      # ARGV[4]: lane ID ($laneid)

     	system("echo 'perl -w $scriptspath/map_and_clean.pl $indnamelong $catpath $outdir $newfcid $laneid' | qsub -l h_vmem=6g -N map_${indnamelong}_${newfcid}_${laneid} -e /mnt/gluster/data/internal_supp/atac_seq/logs/map.$indnamelong.$newfcid.$laneid.err -o /mnt/gluster/data/internal_supp/atac_seq/logs/map.$indnamelong.$newfcid.$laneid.out -V");
        
    }
  }
}

list ($flowcell);
exit 0;
