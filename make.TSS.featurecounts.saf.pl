#!/usr/bin/perl

# IGR, 4.22.15
# Read in bed file generated with liftover and adds +1 to the left side of the interval, since featureCounts expect coordinates to be 1-based. Also rearranges columns in the right order for a SAF.

scalar @ARGV == 2 or die<<USAGE;
Usage:
perl -w make.TSS.featurecounts.saf.pl [BED file] [OUTFILE]
USAGE

my $bedfile = $ARGV[0];
my $outfile = $ARGV[1];

open(IN, "< $bedfile") or die ("Cannot open info file $bedfile\n");
open(OUT, "> $outfile") or die ("Can't open $outfile : $!\n");

while (my $bed= <IN>) {
#	next if ($bed ne "^chr");
	chomp;
	my @bedline = split ("\t", $bed);
	my $newstart = $bedline[1] + 1;
	print OUT "$bedline[3]\t$bedline[0]\t$newstart\t$bedline[2]\t$bedline[5]";
}
 
close IN;
close OUT;