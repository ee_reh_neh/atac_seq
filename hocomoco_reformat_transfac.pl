#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

# IGR, 2016.11.07
# Parses transfac-style output from HOCOMOCO and converts it to a PWM format that scanMotif can accept.

scalar @ARGV == 2 or die<<USAGE;
Usage:
perl -w hocomoco_reformat_transfac.pl [file to split] [outdir]
USAGE

my $hocomoco = $ARGV[0];
my $outdir = $ARGV[1];

open(IN, "< $hocomoco") or die ("Cannot open HOCOMOCO file $hocomoco\n");
open(OUT2, ">$outdir/hocomoco_pwms.txt");

while (my $pwm= <IN>) {
    chomp $pwm;
    next if ($pwm =~ "^XX");
    my @pwmline = split ("\t", $pwm);
    if ($pwmline[0] =~ "^AC") {
        my $pwmname = $pwmline[1];
        open(OUT, "> $outdir/${pwmname}_reformat.pfm");   
        print OUT ("##@pwmline\n");
        print OUT2 ("$pwmname\t");
    }
    elsif ($pwmline[0] =~ "^ID") {
        print OUT ("##@pwmline\n");
        print OUT2 ("$pwmline[1]\n")
    }
    elsif ($pwmline[0] =~ "^NA") {
        print OUT ("##@pwmline\n");
    }
    elsif ($pwmline[0] =~ "^BF") {
        print OUT ("##@pwmline\n");
    }
    elsif ($pwmline[0] =~ "^P0") {
        print OUT "$pwmline[1]\t$pwmline[2]\t$pwmline[3]\t$pwmline[4]\n";
    }
    elsif ($pwmline[0] =~ "^[0-9]") {
        print OUT "$pwmline[1]\t$pwmline[2]\t$pwmline[3]\t$pwmline[4]\n";
    }
    elsif ($pwmline[0] eq "//") {
        close OUT;
    }
}
 
close IN;
close OUT2;