#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

# IGR, 03.03.15
# Assumes data are nested one directory deep given the reference path
# Launch fastQC for the two ATAC-seq runs 
# 1. C47ECACXX (run in the FGF core)
# 2. C47FKACXX (run by Claudia)

scalar @ARGV >= 1 or die<<USAGE;
Usage:
perl -w launch_fastqc [Path to flowcell] [Path to fastqc outdir]
USAGE

# Merge multiple R1 and R2 within a lane, generate 8 fastq.gz per individual:
my $flowcell = $ARGV[0];
my $outdir = $ARGV[1];

# Subroutine from http://www.perlmonks.org/?node_id=489552
# Read in directories and file names:


sub list
{
  my ($dir) = @_;
  return unless -d $dir;

  my @files;
  if (opendir my $dh, $dir)
  {
    # Capture entries first, so we don't descend with an
    # open dir handle.
    my @list;
    my $file;
    while ($file = readdir $dh)
    {
      push @list, $file;
    }
    closedir $dh;

    for $file (@list)
    {
      # Unix file system considerations.
      next if $file eq '.' || $file eq '..' || $file eq '0' || $file eq 'SampleSheet.csv';

      # Swap these two lines to follow symbolic links into
      # directories.  Handles circular links by entering an
      # infinite loop.
      push @files, "$dir/$file"        if -f "$dir/$file";
      push @files, list("$dir/$file")   if -d "$dir/$file";
     
    }
  #print Dumper @files;  
    
   my $testcounter = 0;
   foreach (@files){
      	my @fullpath = split /\//, $_;
      	next if $fullpath[-1] eq '.' || $fullpath[-1] eq '..' || $fullpath[-1] eq 'SampleSheet.csv';
  
    	# Check the outdir
   	my $fastqcinddir = $outdir ."/". $fullpath[-2];
   	# This can be commented out after the first run, to avoid lots of checking of the file system.... spudhead is hateful enough as is
    	 unless (-d $fastqcinddir){
    	   system("mkdir -p $fastqcinddir");
    	 }
    	system("echo \'fastqc $_ -o ${fastqcinddir}\' | qsub -l h_vmem=6g -N fastqc_$fullpath[-1] -e ~/logs/fastqc_$fullpath[-1].err -o ~/logs/fastqc_$fullpath[-1].out -v PATH");
    	$testcounter++; 
  	}
  }
  #return @files;
  return;
}

list ($flowcell);
exit 0;
