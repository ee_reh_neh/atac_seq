#!/usr/bin/perl
use strict;
use warnings;

## Julien Roux
## May 23, 2013
## use Bedtools to extract coverage of samples at coordinates of orthologous exons
## Then calculate RPKMs

## IGR 06.26.13
## Modified paths from julien's original, now call are less relative. 

my $mappedpath = "/mnt/gluster/home/ireneg/ipsc_paper/mapped_data/";
my $rpkmpath = '/mnt/gluster/home/ireneg/atac_seq/rpkm';
my $orthopath = '/mnt/gluster/home/ireneg/reference/hg19v89_panTro3_ortho';

my $sample_sheet = $ARGV[0];
my @humans;
my @chimps;

open(IN, '<', "$sample_sheet") or die ("Cannot open $sample_sheet file\n");
while (defined (my $sample = <IN>)) {
  chomp $sample;
  next if $sample eq '';
  next if $sample =~ /^#/;
#  print "Treating sample $sample\n";

  # sort samples by species and prepare the command line
  if ($sample =~ m/^H/){ ## human
 	my $fullsample = $mappedpath . $sample . "/accepted_hits.bam";
	push(@humans, $fullsample);
  }
  elsif ($sample =~ m/^C/){ ## chimp
 	my $fullsample = $mappedpath . $sample . "/accepted_hits.bam";
    push(@chimps, $fullsample);
  }

}
close IN;

#launch the jobs
#print "@chimps\n@humans\n";

my $humancommand = "echo \"featureCounts @humans -a $orthopath/hg19_ensembl89_orthoexon_0.97_ortho_FALSE_pc.txt -o $rpkmpath/ipsc_human_ensembl89_0.97_ortho_FALSE.out -F SAF\"| qsub -l h_vmem=8g -N fc_human -v PATH\n";
my $chimpcommand = "echo \"featureCounts @chimps -a $orthopath/panTro3_ensembl89_orthoexon_0.97_ortho_FALSE_pc.txt -o $rpkmpath/ipsc_chimp_ensembl89_0.97_ortho_FALSE.out -F SAF\" | qsub -l h_vmem=8g -N fc_chimp -v PATH\n";

system($humancommand)==0
    or warn "Failed to submit the job to the cluster\n";
system($chimpcommand)==0
    or warn "Failed to submit the job to the cluster\n";

my $humanclean = "echo \"sed 's:\/::g' $rpkmpath/ipsc_human_ensembl89_0.97_ortho_FALSE.out | sed 's/mntglusterhomeirenegipsc_papermapped_data//g' | sed 's/accepted_hits//g' | sed 's/\.bam//g'  | cut -f 1,6-21 > $rpkmpath/ipsc_human_ensembl89_0.97_ortho_FALSE_clean.out\"| qsub -l h_vmem=2g -N clean_human -v PATH -hold-jid fc_human\n";
my $chimpclean = "echo \"sed 's:\/::g' $rpkmpath/ipsc_human_ensembl89_0.97_ortho_FALSE.out | sed 's/mntglusterhomeirenegipsc_papermapped_data//g' | sed 's/accepted_hits//g' | sed 's/\.bam//g' | cut -f 1,6-21 > $rpkmpath/ipsc_human_ensembl89_0.97_ortho_FALSE_clean.out\"| qsub -l h_vmem=2g -N clean_human -v PATH -hold-jid fc_chimp\n";
exit;