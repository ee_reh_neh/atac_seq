# Pipeline for processing  atac-seq data #

In the first part of this README, we detail the steps that were taken to pre-process the atac-seq 
raw data to obtain summary measures (read counts in windows) for the human-chimp comparative chromatin 
accessibility study. In the second part, we describe the actual analysis performed using the summary 
data to contrast the differences between the species.

# Table of Contents #
* Study design
    + Sample information
    + Library design and preparation
    + Sequencing
* Mapping
    + Adding read group information
    + Merging reads across samples
* Quality control
    + Fragment size distribution
* Identifying windows for comparative accessibility
    + Windows from liftover chains 
    + Liftovers from hg19 -> panTro3 -> hg19
    + Mappability filtering
        - Gem mappability generation
        - Filtering windows using mappability
* Summary measures
    + Feature counts in windows
* Quality control pt 2
* Other analyses and tools
    + mtDNA assemply
    + Making bedGraphs
* msCentipede analysis
    + scanMotif pipeline
        - Current problems
* Tools used


## Study design ##
This study consists of seven human and chimpanzee iPSCs cell lines.
### Sample information ###

### Library design and preparation ###
All samples were prepared from 200,000 iPSCs following the ATAC-seq protocol given in [Buenrostro et al, 2013](http://www.nature.com/nmeth/journal/v10/n12/full/nmeth.2688.html).

### Sequencing ###
Samples were sequenced on five different flow cells at five different times. More detailed info is available in the sample sheet detailed below and in these spreadsheets:

* [Sample sequencing and read depth sheet](https://docs.google.com/spreadsheets/d/15kgvrdDhfJ3-hm68_SplgftsoSm9ACTHjIChQ4nAc9k/edit?usp=sharing)
* [Sample info sheet](https://docs.google.com/spreadsheets/d/1A3xlJZt-gZBJo6dFeFOAFKZ9y7R6L0uXBd6LfQBnTKA/edit?usp=sharing) 

Unless otherwise noted, libraries were made by Amy Mitrano. Flowcell 1 was sequenced in the Functional Genomics Facility HiSeq2500. All others were sequenced in the Gilad lab HiSeq2500. Reads were returned to us demultiplexed but not merged across lanes, and with a bizarre maximum file size limit, meaning a single lane splits multiple files. This is fixed by `concatenate_files.pl`.

## Mapping ##
Mapping is done with BWA 0.7.9a-r786. It requires two scripts and an additional sample information file. The information file should be built as follows, but the top line should not be included in the final file and is shown here only for reference. Note that some of the RG tags are being used in unconventional ways, and that many of these ids will be replaced downstream with a more streamlined version. 

Flow cell (RG:ID) | Sample name (RG:SM) | Center (RG:CN, Gilad or FGF core) | Library (RG:LB, library identifier) | Run type (RG:PL, full or rapid) 
------|-------|-------|-------|------
FC1 | C3647 | FGF | LIB1 | FULL
FC1 | C3649 | FGF | LIB1 | FULL
FC1 | C3651 | FGF | LIB1 | FULL

Each sample in each flow cell should be documented here. Flowcell IDs should match the ones used in `map_and_clean_wrapper.pl`. The path to the info sheet should be specified in `map_and_clean.pl`.

The two scripts are
~~~
map_and_clean_wrapper.pl
map_and_clean.pl
~~~

Only `map_and_clean_wrapper.pl` needs to be run by the user:

~~~
scalar @ARGV >= 1 or die<<USAGE;
Usage:
perl -w map_and_clean_wrapper [Path to flowcell] [Path to data outdir] [Desired flow cell ID]
USAGE
~~~

`[path to flowcell]` is simply the path to a symlink (or to the actual data) after demultiplexing with CASAVA (v >= 1.9). The script assumes all directories within that folder begin with `Sample_` and that the first letter of the sample name is either H (human sample) or C (chimpanzee sample). It expects data to be paired-end. 

`[Data outdir]` is the final output directory for the mapped data. 

`[Desired flow cell id]` should match the sample info sheet. 

The script will concatenate files from the same lane into a single sorted gzip file, and will create symbolic links to all other files in a directory named `cat_[new flow cell ID]`, at the same depth as the original flow cell data. The location of `map_and_clean.pl` is hardcoded in the script (can be changed).

`map_and_clean.pl` itself launches three BWA jobs:

Dealing with individual reads:
~~~
-n 2: allow 2 mismatches in read:

bwa aln -n 2 [index] [R1 reads] > outfile1.sai
bwa aln -n 2 [index] [R2 reads] > outfile2.sai
~~~

Mapping paired end:
~~~
-a 5000: maximum fragment size
-n 3: Maximum number of alignments to output in the XA tag for reads paired properly. If a read has more than INT hits, the XA tag will not be written.
-N 10: Maximum number of alignments to output in the XA tag for disconcordant read pairs (excluding singletons). If a read has more than INT hits, the XA tag will not be written.
-r '@RG\tID:$newfcid\tSM:$laneid' :  Specify the read group in a format like ‘@RG\tID:foo\tSM:bar’. [null]

bwa sampe -a 5000 -n 3 -N 10 -r [new read group info] [index] [outfile1.sai] [outfile2.sai] [raw reads 1] [raw reads 2] > outfile.sam
~~~

The genome to map to is decided on the basis of the first letter of the sample name, as above; the -r option is filled in with information from the sample info sheet detailed above. Paths to indexes are hardcoded in the script. Final output is a sorted sam file with its corresponding index. Sam and sai intermediates are deleted. 

### Adding read group information ###

This used to be a separate step, implemented in `reassign.read.groups.sh`, but as of April 10th has been incorporated into `map_and_clean.pl`. Aligned bam files now contain the following types of info:

* RG:ID = unique library-flowcell id, in the form of `[flow cell].[library]` 
* RG:LB = library ID
* RG:PL = type of run: rapid or full 
* RG:PU = lane
* RG:SM = sample name 
* RG:CN = sequencing center: Gilad lab or FGF core 

### Merging reads across samples ###

Samples are merged by library at this point, because the same libraries are often sequenced across multiple flow cells and lanes, and treating them separately would decrease our ability to spot and remove things like PCR duplicates. They are split apart again for QC in R (see below)

## Quality control ##

To ensure high quality data, we remove a bunch of reads with `qc_and_filter.sh` and split files again by library and flow cell. We use picard to remove the following:

* reads mapped to mtDNA
* PCR duplicate reads
* non-uniquely mapping reads
* low mapping quality reads (mapq < 30)
* reads outside our favourite high confidence orthology regions (see below)

More details are in the script.

### Fragment size distribution:###

Given the biases in fragments, `metrics_by_tag.sh` will look through every split .bam file and calculate the fragment size distribution for each sample, which will be used downstream for more QC. The output is not 100% predictable, but can be processed using the following snippet to yield a file ready for `fragment.size.qc.metric.testing.sh`:

    # Fragment size distribution matrix
    # Generated like this:
    # NB: sed '1,10d' is the number of lines needed to cut to ensure the top of the histogram file produced by picard doesn't include dross and throw the alignment off.
    cd ~/atac_seq/mapped/atac_seq/mapping_qc/filtered
      for onion in *fragments.out; do
        sed '1,10d' $onion | paste fragment_sizes_for_testing <(grep -E $'^insert_size|^9\t|^5[0-9]\t|^10[0-9]\t|^15[0-9]\t|^20[0-9]\t' -) > test_file_alt ;
        mv test_file_alt fragment_sizes_for_testing
      done
    ls *out | sed 's/.merged.nochrM.rmdup.filter.TAG_RG_//g' | sed 's/.bam.processed.fragments.out//g' > file_names_for_fragment_size


## Identifying windows for comparative accessibility ##
All of this was done by Shyam G, who has not yet written anything about it, so I'm just annotating the scripts used for every chunk. At some point this will hopefully become far more detailed.

### Liftovers from hg19 -> panTro3 -> hg19 ###

Requires three scripts in order:

* `liftChainToBed.sh` Takes a single liftover chain and turns it into a bedfile of 100bp (or other) windows spanning all the liftable regions. 
* `generate.liftovers.sh` Submits liftover jobs from 1st to 2nd species and back to 1st species for each of the 100bp windows created above.
* `getLiftedWindows.py` Processes the output from above into a single file of liftable, 100bp windows. Currently the stringency is set to 80, which is probably too permissive. 

### Mappability filtering ###

Requires two scripts, but maybe has a wrapper? Needs documenting.

* `gem.mappability.sh` Runs GEM genome-wide on whatever genome one feels like. 
* `gen.gem.index.py` Index genomes for processing with the GEM mapper to compute mappability.
* `getWindowMappability.py` 


## Summary measures ##
### Feature counts in windows ###
featureCounts (version v1.4.6-p1) requires a SAF file of windows. Working with the filtered window data, the first step is to convert the bed into SAF:

~~~
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' hg19.autoX.win100.hg19.0.8.mapp0.8.bed > hg19.autoX.win100.hg19.0.8.mapp.0.8.newstart.saf
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' hg19.autoX.win100.panTro3.0.8.mapp0.8.bed > hg19.autoX.win100.panTro3.0.8.mapp.0.8.newstart.saf
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' panTro3.autoX.win100.hg19.0.8.mapp0.8.bed > panTro3.autoX.win100.hg19.0.8.mapp.0.8.newstart.saf
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' panTro3.autoX.win100.panTro3.0.8.mapp0.8.bed > panTro3.autoX.win100.panTro3.0.8.mapp.0.8.newstart.saf
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' hg19.autoX.win200.hg19.0.8.mapp0.8.bed > hg19.autoX.win200.hg19.0.8.mapp.0.8.newstart.saf
awk '{print $4 "\t" $1 "\t" $2+1 "\t" $3 "\t" "+"}' hg19.autoX.win200.panTro3.0.8.mapp0.8.bed > hg19.autoX.win200.panTro3.0.8.mapp.0.8.newstart.saf
~~~

featureCounts expects both start and end coordinates to be 1-based, rather than 0-based as in the original bed, so we add 1 to the start column. To reflect this, I append `.newstart` to the file name.

Summary counts themselves are produced at the end of `mapping.stats.and.featurecounts.sh`. For more detail on QC at the flow cell and individual level, see the R sections below. This script needs to be run twice - it should technically be split into two scripts... 

Also note that `mapping.stats.and.featurecounts.sh` can take a bunch of different saf files. Right now we're using .saf files for the following sets of regions:

* high quality orthologous 100bp windows across the entire genome
* high quality orthologous 200bp windows across the entire genome
* high quality 1-to-1 orthologous TSS across the entire genome (~26k; oTSS)
* background regions across the entire genome (~26k, defined as 50kb upstream of the oTSS - **in the future it'd be nice to check these to see how badly unrepresentative they are... but then again the same could be said of the TSSs. Can of worms. Worth opening?**)

## Quality control pt 2##

Further QC of the data, including libraries, assessments of reproducibility etc etc, happens at a few different levels. We are particularly interested in the fragment pattern biases (of course), and in looking at reproducibility between libraries, tech replicates and species, and things like that. There's a set of scripts that handle most of this, and they need to be run in a particular order for things to work. 

It can be divided in two steps:

1. Wrangling files by species, with very basic descriptive stats.
    1. `atac.seq.basic.processing.R`: This will take the 100 or 200 bp window output from featureCounts, take out the day20 samples and return a .RDS objects of CPM and reads across both species. It's made up out of two modular functions. See the script for more details on running it, should be executed within R.
    2. `fragment.size.qc.metric.testing.R`: This will take the fragment count matrix described in the relevant section above and process it 

2. Actual QC scripts. Note that these scripts expect .RDS objects from the genome wide windows as well as the oTSS and the background data:
    3. `atac.seq.basic.qc.R`: This scripts examines reproducibility between replicates etc, and in general looks at data before summing individual replicates.
    4. `atac.seq.basic.qc.summed.R`: This script sums replicates and then does basic within and between species QC. It is set up to test different combinations of bad quality replicates, flow cells and samples hopefully identified in the previous step. 

At the end of `atac.seq.basic.qc.summed.R` you can choose your favourite subset of samples (we got with a super restrictive one), throw it into limma and analyse the data that way with `atac.seq.basic.limma.ortho.TSS.R` 


Other scripts:

* `atac.seq.basic.qc.merge.species.R` reads in both counts matrices and merges them between species. It also makes plots of reproducibility within all replicates of a sample (hardcoded)
* `atac.seq.basic.qc.extra.inds.R` Very basic analysis of our data compared to LCL data from the Greenleaf lab and our own cardiac data.

Additionally, there is a first script for performing basic limma analyses on data derived from orthologous TSS (as defined by Xiang - note that these are 3-way orthoTSS, which is ok insofar as the gene expression data we work with is also 3-way ortho): `atac.seq.basic.analysis.orthoTSS.R`.


## Other analyses ##
### mtDNA assembly ###

The sizable number of mitochondrial reads generated by ATAC-seq can be used to reconstruct the mtDNA sequence of all samples. The original idea was to simply assign the chimps to different geographic lineages, since we have no information on subspecies, but it is also useful to confirm whether there are any sample labelling errors between flowcells, as replicates from the same individual should have the same mtDNA genome. 

The reference genomes used are:

* Human: revised Cambridge reference sequence, [Genbank:NC_012920](http://www.ncbi.nlm.nih.gov/nuccore/NC_012920)
* Chimpanzee: Pan troglodytes reference chrM, [Genbank:NC_001643](http://www.ncbi.nlm.nih.gov/nuccore/5835121) 

The script is `mtdna.processing.sh`. It makes uses of [MIRA 4](http://sourceforge.net/projects/mira-assembler/files/MIRA/stable/) and MITObim 1.8 ([paper](http://nar.oxfordjournals.org/content/41/13/e129) [pipeline](https://github.com/chrishah/MITObim)). Paths to reference fasta files, picard, MIRA and a couple of other options need to be declared in the script, but the usage is very straightforward:

`bash mtdna.processing.sh InputBamDirectory OutputDirectory`

Finally, the first line of the assembled fasta file is not informative and should be replaced with the file name. This is easiest to do with the following snippet:

~~~
for i in *; do
perl -i -pe 's/.*/>$ARGV/ if $.==1' $i
done
~~~

### Making bedGraphs ###

`featurecounts.to.bedgraph.sh` creates bedGraphs for each individual .bam file in the input directory of choice, s well, as, through an Rscript call to `make.cumulative.bedgraph.R`, a bedGraph file summing reads across all samples from a single species. The script currently summarises data in 10,000 and 50,000 windows, but that can all be easily changed by modifying the paths etc. 

These can then be viewed in the UCSC genome browser, and are very useful for looking at outlier windows etc. 














## msCentipede analyses ##
### scanMotif pipeline ###

`scan.motif.sh` serves to scan the genome for PWM matches for any given PWM (as of 10/15, these are TRANSFAC 2015.2 PWMs) and then filter them to those that meet certain orthology and score thresholds. It is cobbled together from scripts developed by Xiang Zhou for his attempt at this project, but the current version is written by IGR. 



## Tools used##

Asides from basic shell, we used all of the following in our analyses:

* Mapping, filtering, QCing
    * BWA
    * GEM
    * liftOver
    * Picard
* mtDNA assembly
    * MIRA 4
    * MITObim 1.8
* Statistical analyses:
    * R 3.2.2
        * bioconductor []
        * plyr
        * data.table
        * genomicRanges





# Contact information #
* __Irene Gallego Romero__ (irenegallego AT gmail DOT com)
* __Shyam Gopalakrishnan__ (shyam.g AT gmail DOT com)
