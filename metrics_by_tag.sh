#! /bin/bash

## File: quality_by_tag.sh 
## Author: Shyam Gopalakrishnan
## Date: 17th Mar 2015
## This code is used to take a list of samfiles 
## and generate quality plots and feature counts
## from them. It also uses, optionally, a tag 
## to filter the bam files, so that only reads
## with that tag are retained. It takes 1/2 
## arguments-
## $1: The prefix for the chosen bams. *required*
## $2: The tag to filter on. *optional*

## Variables
tmp_dir='/mnt/gluster/data/internal_supp/atac_seq/picard_temp'
bamtools='/mnt/lustre/home/shyamg/bin/bamtools'
feature_counts='/mnt/gluster/home/ireneg/bin/subread-1.4.6-p1-Linux-x86_64/bin/'

## Count the number of arguments and decide whether a tag is used or not. 
if [ $# -ne 2 -o $# -ne 1 ]; then
  echo "USAGE: quality_by_tag.sh InputBamPrefix [filterTag]"
  exit 1
fi
if [ $# -eq 1 ]; then 
  echo "LOG: No filter applied, using the whole bam files."
  inprefix=$1
  tag=none
elif [ $# -eq 2 ]; then
  echo "LOG: Tag $2 filtered, using only reads with this tag."
  inprefix=$1
  tag=$2
fi


if [ $tag == "none" ]; then
## No need to do any filtering, so run the final stuff directly-
## once we know what the various metric we need to compute are.
## SG: My thought is that it is better to run the individual 
## metric computation on single files as an array job. And the
## run the feature count as a joint single job. That way we parallelize
## the first step. 
echo "#!/bin/bash

#$ -cwd
#$ -j y 
#$ -l h_vmem=8g
#$ -N $inprefix.$tag.metrics

Calculating the distribution of fragment sizes in $sampName.sort.rg.merge.rmdup.bam
if [ -s $fragmentoutdir/$sampName.sort.rg.merge.rmdup.fragments ]; then
  $java -Xmx2g -jar $picardpath CollectInsertSizeMetrics INPUT=$outdir/$sampName.sort.rg.rmdup.bam OUTPUT= $fragmentoutdir/$sampName.sort.rg.merge.rmdup.fragments HISTOGRAM_FILE=$fragmentoutdir/$basename.sort.rg.merge.rmdup.fragments.hist.pdf METRIC_ACCUMULATION_LEVEL=ALL_READS VALIDATION_STRINGENCY=LENIENT
fi
exitstatus=\$?
if [ \$exitstatus != 0 ]; then
  echo 'ERROR: Failed to generate the insert size distribution with exit code \$exitstatus.'
else
  echo 'Successfully generated insert size metrics.'
fi
