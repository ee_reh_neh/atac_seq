#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

# IGR, 03.03.15
# Concatenate split files across lanes respecting R1 and R2
# Assumes data are nested one directory deep given the reference path
# Launch fastQC for the two ATAC-seq runs 
# 1. C47ECACXX (run in the FGF core)
# 2. C47FKACXX (run by Claudia)

# This file is needlessly complicated because I was going to use it to also launch fastqc jobs, but it meant too many intermediates, so I decided to write a separate one with very similar structure to do that. 

scalar @ARGV >= 1 or die<<USAGE;
Usage:
perl -w launch_fastqc [Path to flowcell] [Path to data outdir]
USAGE

# Merge multiple R1 and R2 within a lane, generate 8 fastq.gz per individual:
my $flowcell = $ARGV[0];
my $outdir = $ARGV[1];

print "Concatenating files by individual by lane...\n";

# Read in directories and file names:
sub list
{
  my ($dir) = @_;
  return unless -d $dir;

  my @files;
  if (opendir my $dh, $dir)
  {
    # Capture entries first, so we don't descend with an
    # open dir handle.
    my @list;
    my $file;
    while ($file = readdir $dh)
    {
      push @list, $file;
    }
    closedir $dh;

    for $file (@list)
    {
      # Unix file system considerations.
      next if $file eq '.' || $file eq '..' || $file eq 'SampleSheet.csv';

      # Swap these two lines to follow symbolic links into
      # directories.  Handles circular links by entering an
      # infinite loop.
      push @files, "$dir/$file"        if -f "$dir/$file";
      push @files, concat ("$dir/$file") if -d "$dir/$file";
    }
  }

  return @files;
}

sub concat
{
  my ($dir) = @_;
  return unless -d $dir;

  my @files;
  if (opendir my $dh, $dir)
  {
    # Capture entries first, so we don't descend with an
    # open dir handle.
    my @list;
    my $file;
    while ($file = readdir $dh)
    {
      next if $file eq '.' || $file eq '..' || $file eq 'SampleSheet.csv';
      push @list, $file;
    }
    closedir $dh;

    # Concatenate files within an individual that come from the same direction and lane
    my @R1reads;
    my @R2reads;

    for my $fastq (@list){
        if ($fastq =~ "R1"){
            push @R1reads, $fastq;
        }
        elsif ($fastq =~ "R2"){
            push @R2reads, $fastq;
        }   
    }
    # Issue cat command, add directory from file name 
    # First, break up the file name by split, keep the first element
    my @indname = split "_", $R1reads[0];

    #Then check that the output directories exist:
	if($indname[1] eq "day20"){
        	my $indoutdir = $outdir . "/" . $indname[0] . "_day20/";
        	unless(-d $indoutdir){
        	    system("mkdir $indoutdir");
		}        
        
    		local $" = ' ';
    		my $R1cat = "zcat @R1reads > $indoutdir" . $indname[0] . "_day20_R1.fastq";   
    		my $R2cat = "zcat @R2reads > $indoutdir" . $indname[0] . "_day20_R2.fastq";   

    		system("echo \' $R1cat \' | qsub -l h_vmem=6g -N cat_${indname[0]}_day20_R1 -e ~/cat_${indname[0]}_day20_R1.err -o ~/cat_${indname[0]}_R1_day20.out -V -wd $flowcell/Sample_${indname[0]}_day20\n");
   		system("echo \' $R2cat \' | qsub -l h_vmem=6g -N cat_${indname[0]}_day20_R2 -e ~/cat_${indname[0]}_day20_R2.err -o ~/cat_${indname[0]}_dat_20_R2.out -V -wd $flowcell/Sample_${indname[0]}_day20\n");
    	}

	else{
        	my $indoutdir = $outdir . "/" . $indname[0] . "/";
        	unless(-d $indoutdir){
        	    system("mkdir $indoutdir");
		}        
        
    	local $" = ' ';
    	my $R1cat = "zcat @R1reads > $indoutdir" . $indname[0] . "_R1.fastq";   
    	my $R2cat = "zcat @R2reads > $indoutdir" . $indname[0] . "_R2.fastq";   

    	system("echo \' $R1cat \' | qsub -l h_vmem=6g -N cat_${indname[0]}_R1 -e ~/cat_${indname[0]}_R1.err -o ~/cat_${indname[0]}_R1.out -V -wd $flowcell/Sample_${indname[0]}\n");
    	system("echo \' $R2cat \' | qsub -l h_vmem=6g -N cat_${indname[0]}_R2 -e ~/cat_${indname[0]}_R2.err -o ~/cat_${indname[0]}_R2.out -V -wd $flowcell/Sample_${indname[0]}\n");
    
 	 }
  }
  return @files;
}

list ($flowcell);
exit 0;
