### Looking at binding at the transcription factor family level, both genome-wide and in the high quality subset.
### IGR 2017.11.26

### 0. CLEAN AND PREPARE WORKSPACE. ###
### 1. READ FILES: PWM FILES, AND SUPPORT: TSS, GENE EXPRESSION, PWM TO GENE IDENTITIES. 
### 2. MERGING AND COMPARING WITHIN FAMILIES AND SUBFAMILIES IN THE HIGH PWM DATA SET. ###
### 3. MERGING AND COMPARING WITHIN FAMILIES AND SUBFAMILIES IN THE WHOLE DATA SET. ###
### 4. AND SOME STATISTICS ON THOSE BINDING PATTERNS. ###

#######################################
### 0. CLEAN AND PREPARE WORKSPACE. ###
#######################################

#rm(list=ls())
masterGap <- as.numeric(commandArgs(trailingOnly = TRUE))
# masterGap <- 0 # test case

library(plyr)
library(data.table)
library(GenomicRanges)
library(biomaRt)
library(ggplot2)
library(reshape2)
library(RColorBrewer)
library(gplots)
library(matrixStats)

setwd("~/atac_seq/centipede/output/HOCOMOCO")

source("~/repos/atac_seq/heatmap.3.func.R")
options(width=150)

############################################################################################
### 1. READ FILES: PWM FILES, AND SUPPORT: TSS, GENE EXPRESSION, PWM TO GENE IDENTITIES. ###
############################################################################################

highPWMAll <- read.table("all_motifs_over_12.out", header=T, stringsAsFactors=F)
highPWMAll$PWM <- sapply(strsplit(as.character(highPWMAll$PWM.ID), "-"), "[[", 1)
briefStatsAll <- read.table("overall_summary_stats.txt", header=T, stringsAsFactors=F)

loessBase <- read.table("~/atac_seq/rpkm/topSpecies.loess.norm.norandom_ipsc_final_no_ribo.out", header=T, stringsAsFactors=F)
load(file="~/atac_seq/rpkm/voom.RPKM.loess.norm_ipsc_final_no_ribo.Rda")

orthoTSS <- read.table("~/atac_seq/liftOvers/orthoTSS/hg19_ensembl89_filteredTSS.bed", header=F, stringsAsFactors=F) # Human centric for now
pwmMeta <- read.table("~/atac_seq/reference/HOCOMOCO/HOCOMOCOv10_annotation_HUMAN_mono.tsv", stringsAsFactors=F, sep="\t", header=T)
pwmList <- read.table("~/atac_seq/reference/HOCOMOCO/hocomoco_pwms.txt", header=F, stringsAsFactors=F)
pwmList$uniprot <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 1)
pwmList$type <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 3)

# REX1 is missing, because it is not in the v10 release. We add it back in manually before we do any merging:
    rex1Meta <- read.table("~/atac_seq/reference/HOCOMOCO/HOCOMOCOv11_full_annotation_HUMAN_mono.tsv", stringsAsFactors=F, sep="\t", header=T)
    rex1Meta <- rex1Meta[rex1Meta$Transcription.factor %in% "ZFP42",]
    rex1Meta <- rex1Meta[,colnames(rex1Meta) %in% colnames(pwmMeta),]
    rex1Meta$Model <- gsub(".0", "", rex1Meta$Model) # Can now be merged with pwmList
    pwmMeta <- rbind.fill(pwmMeta, rex1Meta) # God bless plyr

pwmList2 <- merge(pwmList, pwmMeta, by.x="V2", by.y="Model", all.x=T, all.y=F)
ensembl75 <- useMart(host="grch37.ensembl.org", biomart="ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl")
pwmUniprot <- getBM(attributes = c('ensembl_gene_id', 'uniprot_gn', 'hgnc_id', 'entrezgene'), mart=ensembl75, filter='hgnc_id', values=pwmList2$HGNC)

pwmListFinal <- merge(pwmList2, pwmUniprot, by.x="HGNC", by.="hgnc_id", all.x=T, all.y=F)
pwmKeep <- merge(pwmListFinal, loessBase, by.x="ensembl_gene_id", by.y="genes", all=F)

# Rescuing OCT4 and Nanog
onMeta <- pwmListFinal[pwmListFinal$Transcription.factor %in% c("POU5F1", "NANOG"),]
pwmKeep <- rbind.fill(pwmKeep, onMeta) # God bless plyr pt 2

pwmKeep <- pwmKeep[!duplicated(pwmKeep$V1),]
pwmKeep <- pwmKeep[pwmKeep$V1 %in% briefStatsAll$motifName,] # Get rid of those three annoying PWMs that didn't run 
pwmKeep <- pwmKeep[!pwmKeep$type %in% "S",] # Also get rid of the s-class motifs
pwmKeep <- pwmKeep[order(pwmKeep$V1),]
dim(pwmKeep)
# [1] 301  29

highPWM <- highPWMAll[highPWMAll$PWM %in% pwmKeep$V1,]

# Define iRanges intervals:
orthoRange <- GRanges(seqnames=orthoTSS$V1, ranges=IRanges(start=orthoTSS$V2+499, end=orthoTSS$V3-500), names=orthoTSS$V4) # Stupid 0 indexing
highPWMRange <- GRanges(seqnames=highPWM$Chr.Hs, ranges=IRanges(start=highPWM$Start.Hs, end=highPWM$Stop.Hs), names=highPWM$PWM.ID)

highPWMTSS5k <- findOverlaps(orthoRange, highPWMRange, maxgap=5000)
highPWMTSS.df <- data.frame(orthoTSS[queryHits(highPWMTSS5k),], highPWM[subjectHits(highPWMTSS5k),])

dir.create("~/atac_seq/centipede/analyses/hocomoco_families")
setwd("~/atac_seq/centipede/analyses/hocomoco_families")

pwmKeepHigh <- pwmKeep[pwmKeep$V1 %in% unique(highPWM$PWM),]
pwmKeepHigh <- pwmKeepHigh[!duplicated(pwmKeepHigh$V1),]
pwmKeepHigh <- pwmKeepHigh[order(pwmKeepHigh$V1),]

pwmKeepHigh$TF.family <- as.factor(pwmKeepHigh$TF.family)
pwmKeepHigh$TF.subfamily <- as.factor(pwmKeepHigh$TF.subfamily)

dim(pwmKeepHigh)
# [1] 172  29

##########################################################################################
### 2. MERGING AND COMPARING WITHIN FAMILIES AND SUBFAMILIES IN THE HIGH PWM DATA SET. ###
##########################################################################################

processFamilies <- function(filtering){
    pwmFam <- pwmKeepHigh[pwmKeepHigh$TF.family %in% filtering | pwmKeepHigh$TF.subfamily %in% filtering,]
    famPWM <- highPWMAll[highPWMAll$PWM %in% pwmFam$V1,]

    if (length(unique(famPWM$PWM)) > 1) {
        # Process ranges and get max log posterior associated with the PWMs:
        famRangeHs <- GRanges(seqnames=famPWM$Chr.Hs, ranges=IRanges(start=famPWM$Start.Hs, end=famPWM$Stop.Hs), names=famPWM$PWM.ID)
        mcols(famRangeHs) <- DataFrame(id=famPWM$PWM.ID,LogPosOddsHs=famPWM$LogPosOdds.Hs)
        famRangeHsReduce <- reduce(famRangeHs, with.revmap=T)
        famRangeHsMeta <- mcols(famRangeHsReduce)$revmap
        famPWMHsKey <- relist(mcols(famRangeHs)[unlist(famRangeHsMeta), 1], famRangeHsMeta) 
        famPosOddsHsKey <- relist(mcols(famRangeHs)[unlist(famRangeHsMeta), 2], famRangeHsMeta) 
        mcols(famRangeHsReduce) <- DataFrame(id=famPWMHsKey, logPosOddsHs=unlist(lapply(famPosOddsHsKey, max)))

        famRangePt <- GRanges(seqnames=famPWM$Chr.Pt, ranges=IRanges(start=famPWM$Start.Pt, end=famPWM$Stop.Pt), names=famPWM$PWM.ID)
        mcols(famRangePt) <- DataFrame(id=famPWM$PWM.ID,LogPosOddsPt=famPWM$LogPosOdds.Pt)
        famRangePtReduce <- reduce(famRangePt, with.revmap=T)
        famRangePtMeta <- mcols(famRangePtReduce)$revmap
        famPWMPtKey <- relist(mcols(famRangePt)[unlist(famRangePtMeta), 1], famRangePtMeta) 
        famPosOddsPtKey <- relist(mcols(famRangePt)[unlist(famRangePtMeta), 2], famRangePtMeta) 
        mcols(famRangePtReduce) <- DataFrame(id=famPWMPtKey, logPosOddsPt=unlist(lapply(famPosOddsPtKey, max)))

        processedHs <- as.data.frame(famRangeHsReduce)
        processedPt <- as.data.frame(famRangePtReduce)

        pwmsCombined <- length(unique(pwmFam$V1))
        beforeAll <- nrow(famPWM)
        beforeBoundHs <- nrow(famPWM[famPWM$LogPosOdds.Hs >= log(0.99/0.01),])
        beforeBoundPt <- nrow(famPWM[famPWM$LogPosOdds.Pt >= log(0.99/0.01),])
        afterHs <- nrow(processedHs)
        afterPt <- nrow(processedPt)
        afterBoundHs <- nrow(processedHs[processedHs$logPosOddsHs >= log(0.99/0.01),])
        afterBoundPt <- nrow(processedPt[processedPt$logPosOddsPt >= log(0.99/0.01),])

        summaryResults <- c(as.character(filtering), pwmsCombined, beforeAll, beforeBoundHs, beforeBoundPt, afterHs, afterBoundHs, afterPt, afterBoundPt)
        return(summaryResults)

    }

}

# At the family level...
allFams <- unique(pwmKeepHigh$TF.family)
familyLevel <- lapply(allFams, processFamilies)
familyLevel <- familyLevel[!sapply(familyLevel, is.null)] 
familyLevel <- as.data.frame(matrix(unlist(familyLevel), nrow = length(familyLevel), byrow=T), stringsAsFactors=F)
names(familyLevel) <- c("TF.family", "PWMsCombined", "beforeAll", "beforeBoundHs", "beforeBoundPt", "afterHs", "afterBoundHs", "afterPt", "afterBoundPt")
familyLevel[,2:9] <- apply(familyLevel[,2:9], 2, function(x) as.numeric(x))

familyLevel$changeHs <- (familyLevel[,7]/familyLevel[,6]) - (familyLevel[,4]/familyLevel[,3]) 
familyLevel$changePt <- (familyLevel[,9]/familyLevel[,8]) - (familyLevel[,5]/familyLevel[,3]) 

familyLevel$fractBeforeHs <- familyLevel[,4]/familyLevel[,3] 
familyLevel$fractBeforePt <- familyLevel[,5]/familyLevel[,3] 
familyLevel$fractAfterHs <- familyLevel[,7]/familyLevel[,6] 
familyLevel$fractAfterPt <- familyLevel[,9]/familyLevel[,8] 
write.table(familyLevel, file="family_level_cobinding_high_pwm.out", quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

ggplotdataHs <- melt(familyLevel[c(1,12,14)])
ggplotdataHs$TF.family <- as.factor(ggplotdataHs$TF.family)

ggplotdataPt <- melt(familyLevel[c(1,13,15)])
ggplotdataPt$TF.family <- as.factor(ggplotdataPt$TF.family)

pdf(file="binding_before_and_after_family_high_PWM.pdf")
    ggplot(ggplotdataHs, aes(y=value, x=variable)) +
        geom_violin(fill = "yellow3") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Hs (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)

    ggplot(ggplotdataPt, aes(y=value, x=variable)) +
        geom_violin(fill = "plum4") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Pt (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)
dev.off()

# And the subfamily level:
allSubFams <- unique(pwmKeepHigh$TF.subfamily)
subfamilyLevel <- lapply(allSubFams, processFamilies)
subfamilyLevel <- subfamilyLevel[!sapply(subfamilyLevel, is.null)] 
subfamilyLevel <- as.data.frame(matrix(unlist(subfamilyLevel), nrow = length(subfamilyLevel), byrow=T), stringsAsFactors=F)
names(subfamilyLevel) <- c("TF.subfamily", "PWMsCombined", "beforeAll", "beforeBoundHs", "beforeBoundPt", "afterHs", "afterBoundHs", "afterPt", "afterBoundPt")
subfamilyLevel[,2:9] <- apply(subfamilyLevel[,2:9], 2, function(x) as.numeric(x))

subfamilyLevel$changeHs <- (subfamilyLevel[,7]/subfamilyLevel[,6]) - (subfamilyLevel[,4]/subfamilyLevel[,3]) 
subfamilyLevel$changePt <- (subfamilyLevel[,9]/subfamilyLevel[,8]) - (subfamilyLevel[,5]/subfamilyLevel[,3]) 

subfamilyLevel$fractBeforeHs <- subfamilyLevel[,4]/subfamilyLevel[,3] 
subfamilyLevel$fractBeforePt <- subfamilyLevel[,5]/subfamilyLevel[,3] 
subfamilyLevel$fractAfterHs <- subfamilyLevel[,7]/subfamilyLevel[,6] 
subfamilyLevel$fractAfterPt <- subfamilyLevel[,9]/subfamilyLevel[,8] 
write.table(subfamilyLevel, file="subfamily_level_cobinding_high_pwm.out", quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

ggplotdataHs <- melt(subfamilyLevel[c(1,12,14)])
ggplotdataHs$TF.subfamily <- as.factor(ggplotdataHs$TF.subfamily)

ggplotdataPt <- melt(subfamilyLevel[c(1,13,15)])
ggplotdataPt$TF.subfamily <- as.factor(ggplotdataPt$TF.subfamily)

pdf(file="binding_before_and_after_subfamily_high_PWM.pdf")
    ggplot(ggplotdataHs, aes(y=value, x=variable)) +
        geom_violin(fill = "yellow3") +
        geom_point() +
        geom_line(aes(group=TF.subfamily)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Hs (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in subfamily", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)

    ggplot(ggplotdataPt, aes(y=value, x=variable)) +
        geom_violin(fill = "plum4") +
        geom_point() +
        geom_line(aes(group=TF.subfamily)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Pt (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in subfamily", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)
dev.off()


summary(familyLevel)
 #  TF.family         sitesCombined      beforeAll      beforeBoundHs   beforeBoundPt      afterHs        afterBoundHs      afterPt      
 # Length:33          Min.   : 2.000   Min.   :   249   Min.   :   33   Min.   :   15   Min.   :   249   Min.   :   33   Min.   :   249  
 # Class :character   1st Qu.: 2.000   1st Qu.:  3135   1st Qu.:  508   1st Qu.:  222   1st Qu.:  2928   1st Qu.:  402   1st Qu.:  2928  
 # Mode  :character   Median : 3.000   Median :  7607   Median : 1569   Median :  669   Median :  6821   Median : 1297   Median :  6821  
 #                    Mean   : 4.667   Mean   : 26880   Mean   : 7773   Mean   : 4860   Mean   : 21945   Mean   : 5792   Mean   : 21944  
 #                    3rd Qu.: 4.000   3rd Qu.: 17771   3rd Qu.: 4108   3rd Qu.: 2187   3rd Qu.: 16255   3rd Qu.: 3251   3rd Qu.: 16254  
 #                    Max.   :29.000   Max.   :144218   Max.   :63405   Max.   :58997   Max.   :129813   Max.   :54537   Max.   :129802  
 #  afterBoundPt      changeHs             changePt          fractBeforeHs     fractBeforePt      fractAfterHs      fractAfterPt    
 # Min.   :   15   Min.   :-0.0761803   Min.   :-0.0900297   Min.   :0.09479   Min.   :0.03782   Min.   :0.09713   Min.   :0.04009  
 # 1st Qu.:  192   1st Qu.:-0.0026850   1st Qu.:-0.0030764   1st Qu.:0.13631   1st Qu.:0.05852   1st Qu.:0.13643   1st Qu.:0.05780  
 # Median :  589   Median : 0.0001148   Median :-0.0001672   Median :0.19379   Median :0.07062   Median :0.19382   Median :0.06803  
 # Mean   : 3132   Mean   :-0.0053863   Mean   :-0.0076382   Mean   :0.21413   Mean   :0.12005   Mean   :0.20874   Mean   :0.11241  
 # 3rd Qu.: 1429   3rd Qu.: 0.0015235   3rd Qu.: 0.0002788   3rd Qu.:0.24935   3rd Qu.:0.13046   3rd Qu.:0.24751   3rd Qu.:0.12182  
 # Max.   :23007   Max.   : 0.0147007   Max.   : 0.0035049   Max.   :0.57414   Max.   :0.54825   Max.   :0.56141   Max.   :0.53323  

summary(subfamilyLevel)
 # TF.subfamily        PWMsCombined     beforeAll      beforeBoundHs   beforeBoundPt        afterHs        afterBoundHs        afterPt      
 # Length:34          Min.   :2.000   Min.   :   488   Min.   :  119   Min.   :   83.0   Min.   :   479   Min.   :  118.0   Min.   :   479  
 # Class :character   1st Qu.:2.000   1st Qu.:  2222   1st Qu.:  440   1st Qu.:  195.8   1st Qu.:  2207   1st Qu.:  411.5   1st Qu.:  2207  
 # Mode  :character   Median :2.000   Median :  4542   Median : 1080   Median :  431.0   Median :  3414   Median :  856.5   Median :  3413  
 #                    Mean   :2.412   Mean   : 15906   Mean   : 5006   Mean   : 3244.3   Mean   : 12900   Mean   : 3582.1   Mean   : 12899  
 #                    3rd Qu.:2.750   3rd Qu.: 14819   3rd Qu.: 2899   3rd Qu.: 1433.0   3rd Qu.: 11036   3rd Qu.: 2840.8   3rd Qu.: 11036  
 #                    Max.   :5.000   Max.   :122857   Max.   :56683   Max.   :53806.0   Max.   :119118   Max.   :27126.0   Max.   :119116  
 #  afterBoundPt        changeHs            changePt          fractBeforeHs    fractBeforePt      fractAfterHs     fractAfterPt    
 # Min.   :   83.0   Min.   :-0.060001   Min.   :-6.785e-02   Min.   :0.1135   Min.   :0.04142   Min.   :0.1143   Min.   :0.04172  
 # 1st Qu.:  184.8   1st Qu.:-0.003940   1st Qu.:-2.945e-03   1st Qu.:0.1556   1st Qu.:0.06481   1st Qu.:0.1556   1st Qu.:0.06532  
 # Median :  407.5   Median : 0.000000   Median :-2.582e-05   Median :0.2077   Median :0.08439   Median :0.2124   Median :0.08564  
 # Mean   : 1965.5   Mean   :-0.004372   Mean   :-5.331e-03   Mean   :0.2661   Mean   :0.15649   Mean   :0.2617   Mean   :0.15116  
 # 3rd Qu.: 1356.0   3rd Qu.: 0.001221   3rd Qu.: 2.642e-04   3rd Qu.:0.2833   3rd Qu.:0.14717   3rd Qu.:0.2819   3rd Qu.:0.14573  
 # Max.   :20664.0   Max.   : 0.006720   Max.   : 2.521e-03   Max.   :0.8859   Max.   :0.76675   Max.   :0.8859   Max.   :0.74267  


#######################################################################################
### 3. MERGING AND COMPARING WITHIN FAMILIES AND SUBFAMILIES IN THE WHOLE DATA SET. ###
#######################################################################################

# We need to read all the data in:
getBindingData <- function (motif.name){
    chimp <- read.table(gzfile(paste0("~/atac_seq/centipede/output/HOCOMOCO/", motif.name, ".chimp.post.gz")), header=T, stringsAsFactors=F)
    human <- read.table(gzfile(paste0("~/atac_seq/centipede/output/HOCOMOCO/", motif.name, ".human.post.gz")), header=T, stringsAsFactors=F)
    human.bed <- read.table(gzfile(paste0("~/atac_seq/reference/HOCOMOCO/scanMotif/", motif.name, ".H.final.bed.gz")), header=F, stringsAsFactors=F)
    chimp.bed <- read.table(gzfile(paste0("~/atac_seq/reference/HOCOMOCO/scanMotif/", motif.name, ".C.final.sort.bed.gz")), header=F, stringsAsFactors=F)
    chimp.bed <- chimp.bed[-1,] # Why did we have to do this? I know it's true, but...
    chimp.all <- cbind(chimp, chimp.bed)
    human.bed <- human.bed[-1,] 
    human.all <- cbind(human, human.bed)
    both.merge <- merge(human.all[,c(1:4,13,5:8,12)], chimp.all[,c(1:4,13,5:8,12)], by.x="V4", by.y="V4", all=F, sort=F, suffixes=c(".Hs",".Pt"))
    names(both.merge)[c(1,6,15)] <- c("PWM.ID", "PWM.score.Hs", "PWM.score.Pt")
    return(both.merge)
} 


# filtering <- "Sp1-like factors{2.3.1.1}" # Short test case

processFamiliesAll <- function(filtering){
    pwmFam <- pwmKeep[pwmKeep$TF.family %in% filtering | pwmKeep$TF.subfamily %in% filtering,]
    if (length(unique(pwmFam$V1)) > 1) {
        famPWMlist <- lapply(pwmFam$V1, getBindingData)
        famPWM <- ldply(famPWMlist)

        # Process ranges and get max log posterior associated with the PWMs:
        famRangeHs <- GRanges(seqnames=famPWM$Chr.Hs, ranges=IRanges(start=famPWM$Start.Hs, end=famPWM$Stop.Hs), names=famPWM$PWM.ID)
        mcols(famRangeHs) <- DataFrame(id=famPWM$PWM.ID,LogPosOddsHs=famPWM$LogPosOdds.Hs)
        famRangeHsReduce <- reduce(famRangeHs, with.revmap=T)
        famRangeHsMeta <- mcols(famRangeHsReduce)$revmap
        famPWMHsKey <- relist(mcols(famRangeHs)[unlist(famRangeHsMeta), 1], famRangeHsMeta) 
        famPosOddsHsKey <- relist(mcols(famRangeHs)[unlist(famRangeHsMeta), 2], famRangeHsMeta) 
        mcols(famRangeHsReduce) <- DataFrame(id=famPWMHsKey, logPosOddsHs=unlist(lapply(famPosOddsHsKey, max)))

        famRangePt <- GRanges(seqnames=famPWM$Chr.Pt, ranges=IRanges(start=famPWM$Start.Pt, end=famPWM$Stop.Pt), names=famPWM$PWM.ID)
        mcols(famRangePt) <- DataFrame(id=famPWM$PWM.ID,LogPosOddsPt=famPWM$LogPosOdds.Pt)
        famRangePtReduce <- reduce(famRangePt, with.revmap=T)
        famRangePtMeta <- mcols(famRangePtReduce)$revmap
        famPWMPtKey <- relist(mcols(famRangePt)[unlist(famRangePtMeta), 1], famRangePtMeta) 
        famPosOddsPtKey <- relist(mcols(famRangePt)[unlist(famRangePtMeta), 2], famRangePtMeta) 
        mcols(famRangePtReduce) <- DataFrame(id=famPWMPtKey, logPosOddsPt=unlist(lapply(famPosOddsPtKey, max)))

        processedHs <- as.data.frame(famRangeHsReduce)
        processedPt <- as.data.frame(famRangePtReduce)

        pwmsCombined <- length(unique(pwmFam$V1))
        beforeAll <- nrow(famPWM)
        beforeBoundHs <- nrow(famPWM[famPWM$LogPosOdds.Hs >= log(0.99/0.01),])
        beforeBoundPt <- nrow(famPWM[famPWM$LogPosOdds.Pt >= log(0.99/0.01),])
        afterHs <- nrow(processedHs)
        afterPt <- nrow(processedPt)
        afterBoundHs <- nrow(processedHs[processedHs$logPosOddsHs >= log(0.99/0.01),])
        afterBoundPt <- nrow(processedPt[processedPt$logPosOddsPt >= log(0.99/0.01),])

        summaryResults <- c(as.character(filtering), pwmsCombined, beforeAll, beforeBoundHs, beforeBoundPt, afterHs, afterBoundHs, afterPt, afterBoundPt)
        return(summaryResults)

    }

}

# At the family level...
allFams <- unique(pwmKeep$TF.family)
familyLevel <- lapply(allFams, processFamiliesAll)
familyLevel <- familyLevel[!sapply(familyLevel, is.null)] 
familyLevel <- as.data.frame(matrix(unlist(familyLevel), nrow = length(familyLevel), byrow=T), stringsAsFactors=F)
names(familyLevel) <- c("TF.family", "PWMsCombined", "beforeAll", "beforeBoundHs", "beforeBoundPt", "afterHs", "afterBoundHs", "afterPt", "afterBoundPt")
familyLevel[,2:9] <- apply(familyLevel[,2:9], 2, function(x) as.numeric(x))

familyLevel$changeHs <- (familyLevel[,7]/familyLevel[,6]) - (familyLevel[,4]/familyLevel[,3]) 
familyLevel$changePt <- (familyLevel[,9]/familyLevel[,8]) - (familyLevel[,5]/familyLevel[,3]) 

familyLevel$fractBeforeHs <- familyLevel[,4]/familyLevel[,3] 
familyLevel$fractBeforePt <- familyLevel[,5]/familyLevel[,3] 
familyLevel$fractAfterHs <- familyLevel[,7]/familyLevel[,6] 
familyLevel$fractAfterPt <- familyLevel[,9]/familyLevel[,8] 
write.table(familyLevel, file="family_level_cobinding.out", quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

ggplotdataHs <- melt(familyLevel[c(1,12,14)])
ggplotdataHs$TF.family <- as.factor(ggplotdataHs$TF.family)

ggplotdataPt <- melt(familyLevel[c(1,13,15)])
ggplotdataPt$TF.family <- as.factor(ggplotdataPt$TF.family)

pdf(file="binding_before_and_after_family.pdf")
    ggplot(ggplotdataHs, aes(y=value, x=variable)) +
        geom_violin(fill = "yellow3") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Hs (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)

    ggplot(ggplotdataPt, aes(y=value, x=variable)) +
        geom_violin(fill = "plum4") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Pt (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)
dev.off()

# And the subfamily level:
allSubFams <- unique(pwmKeep$TF.subfamily)
subfamilyLevel <- lapply(allSubFams, processFamiliesAll)
subfamilyLevel <- subfamilyLevel[!sapply(subfamilyLevel, is.null)] 
subfamilyLevel <- as.data.frame(matrix(unlist(subfamilyLevel), nrow = length(subfamilyLevel), byrow=T), stringsAsFactors=F)
names(subfamilyLevel) <- c("TF.subfamily", "PWMsCombined", "beforeAll", "beforeBoundHs", "beforeBoundPt", "afterHs", "afterBoundHs", "afterPt", "afterBoundPt")
subfamilyLevel[,2:9] <- apply(subfamilyLevel[,2:9], 2, function(x) as.numeric(x))

subfamilyLevel$changeHs <- (subfamilyLevel[,7]/subfamilyLevel[,6]) - (subfamilyLevel[,4]/subfamilyLevel[,3]) 
subfamilyLevel$changePt <- (subfamilyLevel[,9]/subfamilyLevel[,8]) - (subfamilyLevel[,5]/subfamilyLevel[,3]) 

subfamilyLevel$fractBeforeHs <- subfamilyLevel[,4]/subfamilyLevel[,3] 
subfamilyLevel$fractBeforePt <- subfamilyLevel[,5]/subfamilyLevel[,3] 
subfamilyLevel$fractAfterHs <- subfamilyLevel[,7]/subfamilyLevel[,6] 
subfamilyLevel$fractAfterPt <- subfamilyLevel[,9]/subfamilyLevel[,8] 
write.table(subfamilyLevel, file="subfamily_level_cobinding.out", quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

ggplotdataHs <- melt(subfamilyLevel[c(1,12,14)])
ggplotdataHs$TF.subfamily <- as.factor(ggplotdataHs$TF.subfamily)

ggplotdataPt <- melt(subfamilyLevel[c(1,13,15)])
ggplotdataPt$TF.subfamily <- as.factor(ggplotdataPt$TF.subfamily)

pdf(file="binding_before_and_after_subfamily.pdf")
    ggplot(ggplotdataHs, aes(y=value, x=variable)) +
        geom_violin(fill = "yellow3") +
        geom_point() +
        geom_line(aes(group=TF.subfamily)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Hs (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in subfamily", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)

    ggplot(ggplotdataPt, aes(y=value, x=variable)) +
        geom_violin(fill = "plum4") +
        geom_point() +
        geom_line(aes(group=TF.subfamily)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Pt (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Sum across all TFs in subfamily", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)
dev.off()


#########################################################
### 4. AND SOME STATISTICS ON THOSE BINDING PATTERNS. ###
#########################################################

shareFamHigh <- read.table("family_level_cobinding_high_pwm.out", sep="\t", header=T)
sharesubFamHigh <- read.table("subfamily_level_cobinding_high_pwm.out", sep="\t", header=T)

shareFam <- read.table("family_level_cobinding.out", sep="\t", header=T)
sharesubFam <- read.table("subfamily_level_cobinding.out", sep="\t", header=T)

# But to really test, you want to quantify the variance before and after - that is, how similar were the estimates for each family member before you merged them, as opposed to the cumulative sum?

# For that, we need both briefStats and highPWM, and we can do this with a subset of pwmKeep:
briefStatsAll <- read.table("~/atac_seq/centipede/output/HOCOMOCO/overall_summary_stats.txt", header=T, stringsAsFactors=F)
briefStats <- briefStatsAll[briefStatsAll$motifName %in% pwmKeep$V1,]

allShares <- pwmKeep[,c(3,4,7,16,17)]

blah <- merge(allShares, briefStats[,c(1,2,3,14:21)], by.x="V1", by.y="motifName", all=T)
blah <- merge(blah, shareFamHigh, by.x="TF.family", by.y="TF.family")
blah$hsByMotifBefore <- blah$boundHs/blah$totalPWM
blah$ptByMotifBefore <- blah$boundPt/blah$totalPWM

# With repeats allowed (bad idea, but highly significant)
t.test(blah$hsByMotifBefore, blah$fractAfterHs)
t.test(blah$ptByMotifBefore, blah$fractAfterPt)

# Average before and after, instead of sum - significant in humans but not in chimps.
allMeans <- ddply(blah[,c(1,28:31)], .(as.factor(blah$TF.family)), numcolwise(mean))
t.test(allMeans$fractAfterHs, allMeans$hsByMotifBefore)
t.test(allMeans$fractAfterPt, allMeans$ptByMotifBefore)

# And a nice plot to go with it...
names(allMeans)[1] <- "TF.family"

ggplotdataHs <- melt(allMeans[c(1,4,2)])
ggplotdataHs$TF.family <- as.factor(ggplotdataHs$TF.family)

ggplotdataPt <- melt(allMeans[c(1,5,3)])
ggplotdataPt$TF.family <- as.factor(ggplotdataPt$TF.family)

pdf(file="binding_before_and_after_family_high_PWM_average.pdf")
    ggplot(ggplotdataHs, aes(y=value, x=variable)) +
        geom_violin(fill = "yellow3") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Hs (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Mean across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)

    ggplot(ggplotdataPt, aes(y=value, x=variable)) +
        geom_violin(fill = "plum4") +
        geom_point() +
        geom_line(aes(group=TF.family)) +
        theme_bw() + 
        labs(title="", y="Fraction of sites bound in Pt (PWM > 12)", x="") + 
        scale_x_discrete(labels=c("Mean across all TFs in family", "After controlling for redundancy")) +
        theme(legend.title=element_blank(), axis.text.y = element_text(angle = 90, hjust = 0.5), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
        guides(fill=F)
dev.off()


