#! /bin/bash

## File: macs2qc.sh 
## Author: IGR
## Date: 18.04.30
## This code is used to run MACS2 for each bam file and make sure the reads go where they should. 
## The arguments come from Darren Cusanovich. 
## It takes 2 arguments-
## $1: The path to the chosen bam files. *required*
## $2: The path to the macs2 outdirectory. *required*

# picardpath="/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar"
# java="/usr/bin/java"
# samtools="/usr/local/bin/samtools"
# qcdir="/mnt/gluster/data/internal_supp/atac_seq/mapped/atac_seq/mapping_qc_200"
# bamtools="/mnt/lustre/home/shyamg/bin/bamtools"
# featurecounts="/mnt/gluster/home/ireneg/bin/subread-1.4.6-p1-Linux-x86_64/bin/featureCounts"

chimpgenome="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3/panTro3.autoXM.no_Y_random.fa"
humangenome="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19/hg19.autoXM.no_Y_random.fa"

if [ $# -ne 2 ]; then
    echo "USAGE: macs2qc.sh InputBamDirectory OutputDirectory"
    exit 1
fi

bamdirectory=$1
macsqcdirectory=$2

if [ ! -d $macsqcdirectory/logs ]; then
    mkdir -p $macsqcdirectory/logs
fi

for tsampName in $bamdirectory/*.merged.nochrM.rmdup.filter.bam; do
    sampName=`basename $tsampName .merged.nochrM.rmdup.filter.bam`

    # Set the species:
    if echo $tsampName | grep -q "^C"; then
        genome=$chimpgenome
    elif echo $splitName | grep -q "^H"; then
        genome=$humangenome    
    fi   

    # RunMACS2
    echo -e "#! /bin/bash

    echo 'Running macs2 for $sampName.'
    if [ -s $macsqcdirectory/$sampName._peaks.narrowPeak.gz ]; then
        echo 'MACS2 has already been run for $sampName.'
    
    else
        echo 'Running MACS2 for $sampName. Cross your fingers.'
        cd $macsqcdirectory
        macs2 callpeak -t $bamdirectory/$tsampName -f BAM -g $genome --nomodel --keep-dup all --extsize 200 --shift -100 --call-summits -n ${sampName};
        sort -k 8gr,8gr ${sampName}_peaks.narrowPeak | awk 'BEGIN{OFS="\t"}{$4="Peak_"NR ; print $0}' | gzip -c > ${sampName}_peaks.narrowPeak.gz;

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed CollectInsertSizeMetrics with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully calculated fragment size distribution for $sampName.
        fi
    fi
    " | qsub -l h_vmem=8g -o $macsqcdirectory/logs/$sampName.fragments.processed.out -N macs2$sampName -cwd -j y 
done

