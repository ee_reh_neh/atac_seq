#!/bin/bash

# File: scan.motifs.by.sample.sh
# Date: 18th January 2016
# Author: IGR

# Feeds a handful of PWM files to slop and intersectBed to see if all libraries and flow cells are contributing equally across the genome, or some are particularly noisy and not specific. See http://ireneg.github.io/filtering_pwms/ for the reason I thought this would be worth doing. 

# Relies on scan.motifs.batch.sh having already been run. 

# PWM related files:
pwmpath="/data/external_public/pwm_models"
pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif" 
pwmlist="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/high_ic_transfac_pwms.txt"

outdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif/by_sample"

if [ ! -d $outdir ]; then
  mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
  mkdir -p $outdir/logs
fi

# bam files directory
mappeddir="/mnt/gluster/home/ireneg/atac_seq/mapped/atac_seq/filtered/"

# Get the species for each bam for each species:
echo -e "#! /bin/bash
#$ -j y
#$ -t 1-25
#$ -l h_vmem=5g
#$ -N human.chimp.scan_motif.sample
#$ -o $outdir/logs/\$TASK_ID.scan_by_name.log
#$ -V

motif=\`awk NR==\$SGE_TASK_ID $pwmlist | cut -f1 \`

# First decompress the zip files so the next steps can run:
gunzip $pwmdir/\$motif.H.final.bed
gunzip $pwmdir/\$motif.C.final.bed

for samplename in $mappeddir/*bam; do
    shortname=\`basename \$samplename\`
    echo Processing sample \$shortname for \$motif.

    if echo \$shortname | grep -q \"^C\"; then
        # The chimp file requires an additional sorting step, because otherwise it breaks when things are out of order
        sortBed -i $pwmdir/\$motif.C.final.bed | slopBed -i stdin -b 100 -g ~/atac_seq/reference/panTro3_sizes | intersectBed -a stdin -b \$samplename -sorted -g ~/atac_seq/reference/panTro3_sizes -wa -u | intersectBed -a $pwmdir/\$motif.C.final.bed -b stdin -wa -u | gzip > $outdir/\$motif.\$shortname.final.filter.bed.gz

    elif echo \$shortname | grep -q \"^H\"; then
        slopBed -i $pwmdir/\$motif.H.final.bed -b 100 -g ~/atac_seq/reference/hg19_sizes | intersectBed -a stdin -b \$samplename -sorted -g ~/atac_seq/reference/hg19_sizes -wa -u | intersectBed -a $pwmdir/\$motif.H.final.bed -b stdin -wa -u | gzip > $outdir/\$motif.\$shortname.H.final.filter.bed.gz

   
    fi

done
" | qsub




