#! /bin/bash

## File: featurecounts.to.bedgraph.sh 
## Author: IGR
## Date: 26.03.15
## For examining whether there is an excess of reads in repetitive elements of the genome. 
## This file takes a list of sorted bam files (produced by mapping.stats.and.featurecounts.sh)
## and launches featureCounts using 10kb and 50kb windows both for each individual file, and
## for the species as a whole. It then converts that output into bedGraph files for easy
## visualisation in the UCSC genome browser. 
## bedGraph format is described by UCSC in: 
## https://genome.ucsc.edu/goldenPath/help/bedgraph.html

## Keep in mind that this is only informative if run on the intermediate bam files, prior to
## removing duplicates.
## See also lines 37-48 to control output names and input files.

rscriptpath="/mnt/gluster/home/ireneg/repos/atac_seq/make.cumulative.bedgraph.R"
featurecounts="/mnt/gluster/home/ireneg/bin/subread-1.4.6-p1-Linux-x86_64/bin/featureCounts"
chimpwindows50="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3_50000bp_window_clean.txt"
humanwindows50="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19_50000bp_window_clean.txt"
chimpwindows10="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3_10000bp_window_clean.txt"
humanwindows10="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19_10000bp_window_clean.txt"

if [ $# -ne 2 ]; then
    echo "USAGE: featurecounts.to.bedgraph.sh InputBamDirectory OutputBedGraphDirectory"
    exit 1
fi

bamdirectory=$1
fcdirectory=$2

if [ ! -d $fcdirectory/logs ]; then
    mkdir -p $fcdirectory/logs
fi

# To prevent overwriting files when running before and after rmdup, comment lines out as appropriate:
# chimpsuffix=chimp.rmdup
# humansuffix=human.rmdup
chimpsuffix=chimp
humansuffix=human

# First get the list of split files by species and launch the individual featureCounts jobs:
# EDIT THESE LINES FOR RMDUP AND NOCHRM!
for longfcNames in $bamdirectory/*.merged.nochrM.bam; do
    sampfcName=`basename $longfcNames .merged.nochrM.bam`
    # To not overwrite files when running before and after rmdup, this line needs to stay uncommented:
    # sampfcName=$sampfcName.rmdup
   
    if echo $sampfcName | grep -q "^C"; then

        # Populate the big list of species samples as you go, for use below. 
        cfcNames="$longfcNames $cfcNames"

        # Launch the 50kb window job and format the output
        if [ ! -s "$fcdirectory/$sampfcName.50.bedGraph" ]; then
            echo "$featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $chimpwindows50 $longfcNames | sed '1,2d' | awk '{print \$2 \"\t\" \$3 \"\t\" \$4 \"\t\" \$7}' | sed '1 s/^/track type=bedGraph name=$sampfcName.50 visibility=full color=100,100,0 autoScale=on graphType=bar\n/g' > $fcdirectory/$sampfcName.50.bedGraph" | qsub -l h_vmem=4g -o $fcdirectory/logs/$sampfcName.50.featureCounts.out -N fc.$sampfcName.50.clean -cwd -j y -V
        fi

        # Launch the 10kb window job and format the output
        if [ ! -s "$fcdirectory/$sampfcName.10.bedGraph" ]; then
            echo "$featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $chimpwindows10 $longfcNames | sed '1,2d' | awk '{print \$2 \"\t\" \$3 \"\t\" \$4 \"\t\" \$7}' | sed '1 s/^/track type=bedGraph name=$sampfcName.10 visibility=full color=100,100,0 autoScale=on graphType=bar\n/g' > $fcdirectory/$sampfcName.10.bedGraph" | qsub -l h_vmem=4g -o $fcdirectory/logs/$sampfcName.10.featureCounts.out -N fc.$sampfcName.10.clean -cwd -j y -V 
        fi

    elif echo $sampfcName | grep -q "^H"; then
        hfcNames="$longfcNames $hfcNames"
    
        if [ ! -s "$fcdirectory/$sampfcName.50.bedGraph" ]; then
            echo "$featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $humanwindows50 $longfcNames | sed '1,2d' | awk '{print \$2 \"\t\" \$3 \"\t\" \$4 \"\t\" \$7}' | sed '1 s/^/track type=bedGraph name=$sampfcName.50 visibility=full color=100,100,0 autoScale=on graphType=bar\n/g' > $fcdirectory/$sampfcName.50.bedGraph" | qsub -l h_vmem=4g -o $fcdirectory/logs/$sampfcName.50.featureCounts.out -N fc.$sampfcName.50.clean -cwd -j y -V 
        fi

        # Launch the 10kb window job and format the output
        if [ ! -s "$fcdirectory/$sampfcName.10.bedGraph" ]; then
            echo "$featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $humanwindows10 $longfcNames | sed '1,2d' | awk '{print \$2 \"\t\" \$3 \"\t\" \$4 \"\t\" \$7}' | sed '1 s/^/track type=bedGraph name=$sampfcName.10 visibility=full color=100,100,0 autoScale=on graphType=bar\n/g' > $fcdirectory/$sampfcName.10.bedGraph" | qsub -l h_vmem=4g -o $fcdirectory/logs/$sampfcName.10.featureCounts.out -N fc.$sampfcName.10.clean -cwd -j y -V 
        fi

    fi    

done

#Also submit the big joint jobs for each species. Once done, the format can be adjusted in R, by summing across all rows etc...
    # Chimpanzees:
    echo -e "#! /bin/bash

    # First, run featureCounts
    $featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $chimpwindows50 $cfcNames | sed '1,2d' > $fcdirectory/$chimpsuffix.50.fc

    # Then launch R:
    Rscript $rscriptpath $fcdirectory $chimpsuffix.50.fc $chimpsuffix.50.bedGraph 

    # Finally, clean output:
    sed '1 s/^/track type=bedGraph name=$chimpsuffix.50 visibility=full color=200,100,0 autoScale=on graphType=bar\n/g' $fcdirectory/$chimpsuffix.50.bedGraph > $fcdirectory/$chimpsuffix.50.final.bedGraph  
    sed '1 s/^/track type=bedGraph name=$chimpsuffix.50.log visibility=full color=100,100,100 autoScale=on graphType=bar\n/g' $fcdirectory/$chimpsuffix.50.bedGraph.log > $fcdirectory/$chimpsuffix.50.final.bedGraph.log

    " | qsub -l h_vmem=8g -o $fcdirectory/logs/$chimpsuffix.50.featureCounts.out -N fc.$chimpsuffix.50.clean -cwd -j y -V 

    # Launch the 10kb window job 
    echo -e "#! /bin/bash

    # First, run featureCounts
    $featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $chimpwindows10 $cfcNames | sed '1,2d' > $fcdirectory/$chimpsuffix.10.fc

    # Then launch R:
    Rscript $rscriptpath $fcdirectory $chimpsuffix.10.fc $chimpsuffix.10.bedGraph 

    # Finally, clean output:
    sed '1 s/^/track type=bedGraph name=$chimpsuffix.10 visibility=full color=200,100,0 autoScale=on graphType=bar\n/g' $fcdirectory/$chimpsuffix.10.bedGraph > $fcdirectory/$chimpsuffix.10.final.bedGraph  
    sed '1 s/^/track type=bedGraph name=$chimpsuffix.10.log visibility=full color=100,100,100 autoScale=on graphType=bar\n/g' $fcdirectory/$chimpsuffix.10.bedGraph.log > $fcdirectory/$chimpsuffix.10.final.bedGraph.log 

    " | qsub -l h_vmem=8g -o $fcdirectory/logs/$chimpsuffix.10.featureCounts.out -N fc.chimp.10.clean -cwd -j y -V 

    # Humans:
    echo -e "#! /bin/bash

    # First, run featureCounts
    $featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $humanwindows50 $hfcNames | sed '1,2d' > $fcdirectory/$humansuffix.50.fc

    # Then launch R:
    Rscript $rscriptpath $fcdirectory $humansuffix.50.fc $humansuffix.50.bedGraph 

    # Finally, clean output:
    sed '1 s/^/track type=bedGraph name=$humansuffix.50 visibility=full color=200,100,0 autoScale=on graphType=bar\n/g' $fcdirectory/$humansuffix.50.bedGraph > $fcdirectory/$humansuffix.50.final.bedGraph  
    sed '1 s/^/track type=bedGraph name=$humansuffix.50.log visibility=full color=100,100,100 autoScale=on graphType=bar\n/g' $fcdirectory/$humansuffix.50.bedGraph.log > $fcdirectory/$humansuffix.50.final.bedGraph.log  

    " | qsub -l h_vmem=8g -o $fcdirectory/logs/$humansuffix.50.featureCounts.out -N fc.$humansuffix.50.clean -cwd -j y -V 

    # Launch the 10kb window job 
    echo -e "#! /bin/bash

    # First, run featureCounts
    $featurecounts --read2pos 5 -F SAF -o /dev/stdout -a $humanwindows10 $hfcNames | sed '1,2d' > $fcdirectory/$humansuffix.10.fc

    # Then launch R:
    Rscript $rscriptpath $fcdirectory $humansuffix.10.fc $humansuffix.10.bedGraph 

    # Finally, clean output:
    sed '1 s/^/track type=bedGraph name=$humansuffix.10 visibility=full color=200,100,0 autoScale=on graphType=bar\n/g' $fcdirectory/$humansuffix.10.bedGraph > $fcdirectory/$humansuffix.10.final.bedGraph  
    sed '1 s/^/track type=bedGraph name=$humansuffix.10.log visibility=full color=100,100,100 autoScale=on graphType=bar\n/g' $fcdirectory/$humansuffix.10.bedGraph.log > $fcdirectory/$humansuffix.10.final.bedGraph.log

    " | qsub -l h_vmem=8g -o $fcdirectory/logs/$humansuffix.10.featureCounts.out -N fc.$humansuffix.10.clean -cwd -j y -V 



