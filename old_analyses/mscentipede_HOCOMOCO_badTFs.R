###
### For looking at the 6 bad TFs identified through mscentipede_HOCOMOCO_cobinding_a.R
###

### Irene Gallego Romero

### Last update: 2018.06.30: New script, modelled very closely from mscentipede_HOCOMOCO_BadTFs.R

### CONTENTS:
### 0. CLEAN AND PREPARE WORKSPACE. 
### 1. READ FILES: PWM FILES, AND SUPPORT: TSS, GENE EXPRESSION, PWM TO GENE IDENTITIES.
### 2. A BETTER LOOK AT PLURIPOTENCY BINDING ACROSS SPECIES. 
### 3. BadTFs ETC BINDING VS DIFFERENTIAL EXPRESSION.


#######################################
### 0. CLEAN AND PREPARE WORKSPACE. ###
#######################################

rm(list=ls())

library(plyr)
library(data.table)
library(GenomicRanges)
library(biomaRt)
library(rtracklayer)
library(ggplot2)
library(ggridges)
library(reshape2)
library(RColorBrewer)
library(scales) # for ggplot
library(gridExtra) # for ggplot 

setwd("~/atac_seq/centipede/output/HOCOMOCO")

options(width=150)

############################################################################################
### 1. READ FILES: PWM FILES, AND SUPPORT: TSS, GENE EXPRESSION, PWM TO GENE IDENTITIES. ###
############################################################################################

briefStatsAll <- read.table("overall_summary_stats.txt", header=T, stringsAsFactors=F)
highPWMAll <- read.table("all_motifs_over_12.out", header=T, stringsAsFactors=F)
highPWMAll$PWM <- sapply(strsplit(as.character(highPWMAll$PWM.ID), "-"), "[[", 1)

orthoTSS <- read.table("~/atac_seq/liftOvers/orthoTSS/hg19_ensembl89_filteredTSS.bed", header=F, stringsAsFactors=F) # Human centric for now
pwmMeta <- read.table("~/atac_seq/reference/HOCOMOCO/HOCOMOCOv10_annotation_HUMAN_mono.tsv", stringsAsFactors=F, sep="\t", header=T)
pwmList <- read.table("~/atac_seq/reference/HOCOMOCO/hocomoco_pwms.txt", header=F, stringsAsFactors=F)
pwmList$uniprot <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 1)
pwmList$type <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 3)

# REX1 is missing, because it is not in the v10 release. We add it back in manually before we do any merging:
    rex1Meta <- read.table("~/atac_seq/reference/HOCOMOCO/HOCOMOCOv11_full_annotation_HUMAN_mono.tsv", stringsAsFactors=F, sep="\t", header=T)
    rex1Meta <- rex1Meta[rex1Meta$Transcription.factor %in% "ZFP42",]
    rex1Meta <- rex1Meta[,colnames(rex1Meta) %in% colnames(pwmMeta),]
    rex1Meta$Model <- gsub(".0", "", rex1Meta$Model) # Can now be merged with pwmList
    pwmMeta <- rbind.fill(pwmMeta, rex1Meta) # God bless plyr

loessBase <- read.table("~/atac_seq/rpkm/topSpecies.loess.norm.norandom_ipsc_final_no_ribo.out", header=T, stringsAsFactors=F)
loessSig <- loessBase[loessBase$adj.P.Val < 0.01,]

load(file="~/atac_seq/rpkm/voom.RPKM.loess.norm_ipsc_final_no_ribo.Rda")

# Change directory here for the sake of writing all the output somewhere less burdensome:
setwd("~/atac_seq/centipede/analyses/hocomoco")

pwmList2 <- merge(pwmList, pwmMeta, by.x="V2", by.y="Model", all.x=T, all.y=F)
ensembl75 <- useMart(host="grch37.ensembl.org", biomart="ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl")
pwmUniprot <- getBM(attributes = c('ensembl_gene_id', 'uniprot_gn', 'hgnc_id', 'entrezgene'), mart=ensembl75, filter='hgnc_id', values=pwmList2$HGNC)

pwmListFinal <- merge(pwmList2, pwmUniprot, by.x="HGNC", by.="hgnc_id", all.x=T, all.y=F)

table(pwmListFinal$type)


###################################################################
### 2. A BETTER LOOK AT THE BAD TFS AND DE DATA ACROSS SPECIES. ###
###################################################################

pwmBadTFs <- pwmListFinal[grepl("BARX1|CPEB1|FOXO1|PAX7|POU6F1|ZFHX3", pwmListFinal$Transcription.factor),] # These are all the PWMs that survived the cull 
pwmBadTFs <- pwmBadTFs[!duplicated(pwmBadTFs$V1),]
pwmBadTFs <- pwmBadTFs[pwmBadTFs$V1 != "M01350",] # drop that weird Nanog S class PWM

# That looks slim! Now we filter the data sets:
highPWM <- highPWMAll[highPWMAll$PWM %in% pwmBadTFs$V1,]
highPWM$cobound <- highPWM$LogPosOdds.Hs >= log(0.99/0.01) & highPWM$LogPosOdds.Pt >= log(0.99/0.01) # A quick summary for later
highPWM$boundEither <- highPWM$LogPosOdds.Hs >= log(0.99/0.01) | highPWM$LogPosOdds.Pt >= log(0.99/0.01) # A quick summary for later
highPWM$boundHsEx <- highPWM$LogPosOdds.Hs >= log(0.99/0.01) & highPWM$LogPosOdds.Pt < log(0.99/0.01) # A quick summary for later
highPWM$boundPtEx <- highPWM$LogPosOdds.Hs < log(0.99/0.01) & highPWM$LogPosOdds.Pt >= log(0.99/0.01) # A quick summary for later
highPWM$type <- as.factor(pwmBadTFs[match(highPWM$PWM, pwmBadTFs$V1),]$type)

# Finally, our ranges:
orthoRange <- GRanges(seqnames=orthoTSS$V1, ranges=IRanges(start=orthoTSS$V2+499, end=orthoTSS$V3-500), names=orthoTSS$V4) # Stupid 0 indexing
highPWMRange <- GRanges(seqnames=highPWM$Chr.Hs, ranges=IRanges(start=highPWM$Start.Hs, end=highPWM$Stop.Hs), names=highPWM$PWM.ID)

# Look at binding sharing patterns between the two species across all PWMs (including the crappy ones)
briefStatsFracts <- briefStatsAll[briefStatsAll$motifName %in% pwmBadTFs$V1,]

# The oTSS data first:
briefStatsFracts$fractCoBoundTSS <- briefStatsFracts$boundBothTSS/briefStatsFracts$TSS.PWM
briefStatsFracts$fractPtExTSS <- (briefStatsFracts$boundPtTSS - briefStatsFracts$boundBothTSS) / briefStatsFracts$TSS.PWM
briefStatsFracts$fractHsExTSS <- (briefStatsFracts$boundHsTSS - briefStatsFracts$boundBothTSS) / briefStatsFracts$TSS.PWM
briefStatsFracts$fractUnboundTSS <- (briefStatsFracts$TSS.PWM - briefStatsFracts$boundEitherTSS) / briefStatsFracts$TSS.PWM
briefStatsFracts$fractCoBoundTSS2 <- briefStatsFracts$boundBothTSS/briefStatsFracts$boundEitherTSS
briefStatsFracts$fractPtExTSS2 <- (briefStatsFracts$boundPtTSS - briefStatsFracts$boundBothTSS) / briefStatsFracts$boundEitherTSS
briefStatsFracts$fractHsExTSS2 <- (briefStatsFracts$boundHsTSS - briefStatsFracts$boundBothTSS) / briefStatsFracts$boundEitherTSS

# Then the genomewide fractions
briefStatsFracts$fractCoBound <- briefStatsFracts$boundBoth/briefStatsFracts$totalPWM
briefStatsFracts$fractPtEx <- (briefStatsFracts$boundPt - briefStatsFracts$boundBoth) / briefStatsFracts$totalPWM
briefStatsFracts$fractHsEx <- (briefStatsFracts$boundHs - briefStatsFracts$boundBoth) / briefStatsFracts$totalPWM
briefStatsFracts$fractUnbound <- (briefStatsFracts$totalPWM - briefStatsFracts$boundEither) / briefStatsFracts$totalPWM
briefStatsFracts$fractCoBound2 <- briefStatsFracts$boundBoth/briefStatsFracts$boundEither
briefStatsFracts$fractPtEx2 <- (briefStatsFracts$boundPt - briefStatsFracts$boundBoth) / briefStatsFracts$boundEither
briefStatsFracts$fractHsEx2 <- (briefStatsFracts$boundHs - briefStatsFracts$boundBoth) / briefStatsFracts$boundEither

# Finally, the merge:
briefStatsFracts <- merge(briefStatsFracts, pwmBadTFs[,c(3,6,8)], by.x="motifName", by.y="V1")

# And now with only the high quality sites:
getSinglePWMStats <- function(x, threshold){
    possible <- length(x$PWM.score.Hs)
    boundHs <- length(which(x$LogPosOdds.Hs >= threshold))
    boundPt <- length(which(x$LogPosOdds.Pt >= threshold))
    boundBoth <- length(which(x$LogPosOdds.Hs >= threshold & x$LogPosOdds.Pt >= threshold))
    boundEither <- length(which(x$LogPosOdds.Hs >= threshold| x$LogPosOdds.Pt >= threshold)) 
    boundHsEx <- length(which(x$LogPosOdds.Hs >= threshold & x$LogPosOdds.Pt < threshold))
    boundPtEx <- length(which(x$LogPosOdds.Hs < threshold & x$LogPosOdds.Pt >= threshold))
    allStats <- c(possible, boundHs, boundPt, boundBoth, boundEither, boundHsEx, boundPtEx)
    return(allStats)
}

byPWMList <- dlply(highPWM, .(highPWM$PWM))

# And a wrapper function for this one:
plotHistograms <- function(threshold, thresholdLab){
    byPWM <- ldply(lapply(byPWMList, getSinglePWMStats, threshold))
    names(byPWM) <- c("gene", "possible", "boundHs", "boundPt", "boundBoth", "boundEither", "boundHsEx", "boundPtEx")

    byPWM$fractCoBound <- byPWM$boundBoth / byPWM$possible
    byPWM$fractPtEx <- (byPWM$boundPtEx) / byPWM$possible
    byPWM$fractHsEx <- (byPWM$boundHsEx) / byPWM$possible
    byPWM$fractUnbound <- (byPWM$possible - byPWM$boundEither) / byPWM$possible 
    byPWM <- merge(byPWM, pwmBadTFs[,c(3,6)], by.x="gene", by.y="V1")
    byPWM$fractCoBound2 <- byPWM$boundBoth/byPWM$boundEither
    byPWM$fractPtEx2 <- (byPWM$boundPtEx) / byPWM$boundEither
    byPWM$fractHsEx2 <- (byPWM$boundHsEx) / byPWM$boundEither

    pdf(file=paste0("badTFs_regulators_highPWM_gw_", thresholdLab, "_.pdf"))
        ggPlotData <- melt(byPWM[,c(9:13)])
        ggPlotData$variable <- factor(ggPlotData$variable, levels(ggPlotData$variable)[c(4:1)]) # Reorder the levels

        print(ggplot(ggPlotData, aes(x=Transcription.factor, y=value, fill=variable)) +
            geom_bar(stat='identity') +
            scale_fill_manual(values=c("white", "yellow3", "plum4", "firebrick"), labels=c("Unbound", "bound only in Hs", "bound only in Pt", "bound in both species")) + 
            ylim(0,1) +
            theme_bw() + 
            labs(title="", y="fraction PWM sites (gw, PWM score > 12)", x="") + 
            theme(legend.title=element_blank(), axis.text.x = element_text(angle = 45, hjust = 1), panel.grid.major = element_blank(), panel.grid.minor = element_blank())
            )

        ggPlotData <- melt(byPWM[,c(13:16)])
        ggPlotData$variable <- factor(ggPlotData$variable, levels(ggPlotData$variable)[c(3:1)]) # Reorder the levels

        print(ggplot(ggPlotData, aes(x=Transcription.factor, y=value, fill=variable)) +
            geom_bar(stat='identity') +
            scale_fill_manual(values=c("yellow3", "plum4", "firebrick"), labels=c("bound only in Hs", "bound only in Pt", "bound in both species")) + 
            ylim(0,1) +
            theme_bw() + 
            labs(title="", y="fraction PWM sites bound (gw, PWM score > 12)", x="") + 
            theme(legend.title=element_blank(), axis.text.x = element_text(angle = 45, hjust = 1), panel.grid.major = element_blank(), panel.grid.minor = element_blank())
        )
    dev.off()
}

plotHistograms(log(0.99/0.01), 0.99)
plotHistograms(log(0.95/0.05), 0.95)
plotHistograms(log(0.90/0.10), "0.90")


#########################################################
### 3. BadTFs ETC BINDING VS DIFFERENTIAL EXPRESSION. ###
#########################################################

# Let's look at the genes that have a binding site for any of these PWMs in their oTSS, and see if they are enriched/depleted for differential expression.

highPWMTSS <- findOverlaps(orthoRange, highPWMRange, maxgap=5000)
highPWMTSS.df <- data.frame(orthoTSS[queryHits(highPWMTSS),], highPWM[subjectHits(highPWMTSS),])

print("Distribution of high quality binding sites by PWM")
print(table(highPWMTSS.df$PWM))

# Flipped from above - now we're looking by gene, as opposed to by PWM, and so report slightly different statistics.
getSingleGeneStats <- function(x, threshold){
    possible <- length(x$PWM.score.Hs)
    boundHs <- length(which(x$LogPosOdds.Hs >= threshold))
    boundPt <- length(which(x$LogPosOdds.Pt >= threshold))
    boundBoth <- length(which(x$LogPosOdds.Hs >= threshold & x$LogPosOdds.Pt >= threshold))
    boundEither <- length(which(x$LogPosOdds.Hs >= threshold| x$LogPosOdds.Pt >= threshold)) 
    boundHsEx <- length(which(x$LogPosOdds.Hs >= threshold & x$LogPosOdds.Pt < threshold))
    boundPtEx <- length(which(x$LogPosOdds.Hs < threshold & x$LogPosOdds.Pt >= threshold))
    meandPWM <- mean(x$PWM.score.Hs - x$PWM.score.Pt)
    sitesPWM <- length(which(abs(x$PWM.score.Hs - x$PWM.score.Pt) > 1))
    allStats <- c(possible, boundHs, boundPt, boundBoth, boundEither, boundHsEx, boundPtEx, meandPWM, sitesPWM)
    return(allStats)
}

byGeneList <- dlply(highPWMTSS.df, .(highPWMTSS.df$V4))

# Trying a handful of thresholds, because the 0.99 might be too conservative - look at those tails!
geneOverlaps <- function(threshold){
    byGene <- ldply(lapply(byGeneList, getSingleGeneStats, threshold))
    names(byGene) <- c("gene", "possible", "boundHs", "boundPt", "boundBoth", "boundEither", "boundHsEx", "boundPtEx", "meandPWM", "highTurnoverSites")
    print("Number of genes with at least one binding site for BadTFs etc:")
    print(dim(byGene))

    byGene2 <- merge(byGene, loessBase, by.x="gene", by.y="genes", all=F)
    byGene2$diffBound <- byGene2$boundPtEx + byGene2$boundHsEx 
    byGene2 <- data.table(byGene2)
    print("Number of expressed genes with at least one binding site for BadTFs etc:")
    print(dim(byGene2))

    print("Number of expressed genes with at least one *BOUND* site for BadTFs etc:")
    print(dim(byGene2[byGene2$boundEither >= 1,]))

    print("Number of binding events in at least one species, by gene, broken by p-value")
    print(table(byGene2$boundEither, cut(byGene2$adj.P.Val, breaks=c(0,0.01,1))))

    print("Some correlation tests... These shouldn't really be significant.")
    # print(cor.test(byGene2$adj.P.Val, byGene2$diffBound))
    # print(cor.test(abs(byGene2$logFC), byGene2$diffBound))
    print(cor.test(byGene2$adj.P.Val, byGene2$boundPtEx))
    print(cor.test(abs(byGene2$logFC), byGene2$boundPtEx))

    # 
    print("Permutation tests - is there an excess of DE genes in these subsets? A depletion?")        
    allPerm <- data.frame(perm.success = rep(NA, 10000), permHs.success=rep(NA, 10000), permPt.success=rep(NA, 10000))
    for (i in 1:10000){
        if (i %% 1000 == 0){print(i)}
        pluri.draw <- sample(1:nrow(byGene2), nrow(byGene2[byGene2$boundEither >= 1,]))
        pluriHs.draw <- sample(1:nrow(byGene2), nrow(byGene2[byGene2$boundHsEx >= 1,]))
        pluriPt.draw <- sample(1:nrow(byGene2), nrow(byGene2[byGene2$boundPtEx >= 1,]))

        perm.success <- length(which(pluri.draw <= nrow(byGene2[byGene2$adj.P.Val <= 0.01,])))
        permHs.success <- length(which(pluriHs.draw <= nrow(byGene2[byGene2$adj.P.Val <= 0.01,])))
        permPt.success <- length(which(pluriPt.draw <= nrow(byGene2[byGene2$adj.P.Val <= 0.01,])))
        thesePerms <- c(perm.success, permHs.success, permPt.success)
        allPerm[i,] <- thesePerms
    }

    print("Overrepresentation:")
    print("Bound in either species")
    print(length(which(allPerm$perm.success >= nrow(byGene2[byGene2$boundEither >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 
    print("Bound Hs only")
    print(length(which(allPerm$permHs.success >= nrow(byGene2[byGene2$boundHsEx >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 
    print("Bound Pt only")
    print(length(which(allPerm$permPt.success >= nrow(byGene2[byGene2$boundPtEx >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 

    print("p values for underrepresentation:")
    print("Bound in either species")
    print(length(which(allPerm$perm.success < nrow(byGene2[byGene2$boundEither >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 
    print("Bound Hs only")
    print(length(which(allPerm$permHs.success < nrow(byGene2[byGene2$boundHsEx >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 
    print("Bound Pt only")
    print(length(which(allPerm$permPt.success < nrow(byGene2[byGene2$boundPtEx >= 1 & byGene2$adj.P.Val <= 0.01,])))/10000) 
    return(allPerm)
}

permute99 <- geneOverlaps(log(0.99/0.01))
permute95 <- geneOverlaps(log(0.95/0.05))
permute90 <- geneOverlaps(log(0.90/0.10))