#!/bin/bash

# IGR 16/08/10
# File: extract_coverage_featureCounts.sh

# Launches featureCounts for all files, separately by species, generates matrices of results both clean and dirty. 

# $1: Path to mapped reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 2 ]; then
    echo "USAGE: featureCounts_wysocka_rna.sh InputRawDirectory OutputDirectory"
    exit 1
fi

bamdirectory=$1
outdir=$2

# Location of reference file roots:
panTroOrtho="/mnt/gluster/home/ireneg/reference/hg19v89_panTro3_ortho/hg19_ensembl89_orthoexon_0.97_ortho_FALSE_pc.txt"
humanOrtho="/mnt/gluster/home/ireneg/reference/hg19v89_panTro3_ortho/panTro3_ensembl89_orthoexon_0.97_ortho_FALSE_pc.txt"

# Create directories for the output
if [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi

## Parse the samples by species:
for longList in `ls $bamdirectory`; do
    sampName=`basename $longList`
    if echo $sampName | grep -q "^C"; then
        panTroList="$bamdirectory/${sampName}/accepted_hits.bam $panTroList"
    elif echo $sampName | grep -q "^H"; then
        humanList="$bamdirectory/${sampName}/accepted_hits.bam $humanList"
    fi    
done

# Chimpanzees:
echo -e "#! /bin/bash

echo 'Launching featureCounts for all panTro samples.'
if [ -s $outdir/panTro_counts_matrix_wysocka.out ]; then
    echo 'featureCounts has already been run for this species.'
else
    # Launch featureCounts
    # Building the command:
    # -F: format of the feature file; set to SAF. See the manual for format specification.
    # -a: annotation file name - here, species-specific ortho exon file.
    # -s: assign strandedness to data set. Set to 0 manually, library does not seem to be stranded.  
    # -o: sets output file name

    featureCounts -F SAF -a $panTroOrtho -s 0 -o $outdir/panTro_counts_matrix_wysocka.out $panTroList        
    # Clean up output for R
    sed 's:\/::g' $outdir/panTro_counts_matrix_wysocka.out | sed 's/mntglusterhomeirenegatac_seqmappedwysocka_rna_seq//g' | sed 's/.gzaccepted_hits\.bam//g' | sed 's/\.bam//g' | cut -f 1,6-21> $outdir/panTro_counts_matrix_wysocka_clean.out

    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then
        echo ERROR: Failed featureCounts with exit code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully ran featureCounts for all panTro samples.
    fi
fi
" | qsub -l h_vmem=8g -o $outdir/logs/panTro_featureCounts.out -N fcPanTro -v PATH -V -j y

# Humans
echo -e "#! /bin/bash

echo 'Launching featureCounts for all human samples.'
if [ -s $outdir/human_counts_matrix_wysocka.out ]; then
    echo 'featureCounts has already been run for this species.'
else
    # Launch featureCounts
    # Building the command:
    # -F: format of the feature file; set to SAF. See the manual for format specification.
    # -a: annotation file name - here, species-specific ortho exon file.
    # -s: assign strandedness to data set. Set to 0 manually, library does not seem to be stranded.  
    # -o: sets output file name

    featureCounts -F SAF -a $humanOrtho -s 0 -o $outdir/human_counts_matrix_wysocka.out $humanList        
    # Clean up output for R
    sed 's:\/::g' $outdir/human_counts_matrix_wysocka.out | sed 's/mntglusterhomeirenegatac_seqmappedwysocka_rna_seq//g' | sed 's/.gzaccepted_hits\.bam//g' | sed 's/\.bam//g' | cut -f 1,6-21> $outdir/human_counts_matrix_wysocka_clean.out

    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then
        echo ERROR: Failed featureCounts with exit code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully ran featureCounts for all human samples.
    fi
fi
" | qsub -l h_vmem=8g -o $outdir/logs/human_featureCounts.out -N fcPanTro -v PATH -V -j y
