
# pwm.filtering.R
# For testing the effect of filtering PWM sites to only those with 1 or more reads in the 100 bp around a genomic motif
# Relies on scan.motifs.batch.sh and scan.motifs.by.sample.sh

# IGR, 19.1.2016
# Last update: 20.1.2016

# filtered_read_counts_by_ind.txt
# in ~/atac_seq/reference/centipede/scanMotif/by_sample
# for i in `ls *.gz`; do echo $i >> filtered_read_counts_by_ind.txt; zcat $i | wc -l >> filtered_read_counts_by_ind.txt ; done

# read_depth_all_samples
# in /mnt/gluster/home/ireneg/atac_seq/mapped/atac_seq/filtered
# for i in `ls *bam`; do echo $i >> read_depth_all_samples; samtools view -c $i >> read_depth_all_samples; done

# filtered_read_counts.txt
# in ~/atac_seq/reference/centipede/scanMotif
# for i in {1..25}; do motif=`head logs/$i.scan_all.log -n 1 | sed 's/This job has the task ID [0-9]*, and corresponds to //g'` ; for j in `ls $motif*gz`; do echo $j >> filtered_read_counts.txt; zcat $j | wc -l >> filtered_read_counts.txt ;done   ; done

setwd("~/Dropbox/Data/atac_seq/filtered")


overall <- read.table("filtered_read_counts.txt")
overall <- overall[-1,]

overall.m <- data.frame(matrix(as.character(overall), ncol=2, byrow=T))
overall.m$X2 <- as.numeric(as.character(overall.m$X2))

overall.m$pwm <- sapply(strsplit(as.character(overall.m$X1), "\\."), "[[", 1)
overall.m$window <- sapply(strsplit(as.character(overall.m$X1), "\\."), "[[", 5)
overall.m$species <- sapply(strsplit(as.character(overall.m$X1), "\\."), "[[", 2)

overall.m.filt <- overall.m[!grepl("filter.bed", as.character(overall.m$X1)),] 

# From the internet, a colour-blind safe palette:
# http://www.ucl.ac.uk/~zctpep9/Archived%20webpages/Cookbook%20for%20R%20»%20Colors%20(ggplot2).htm
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

pdf("filtered_motifs_by_pwm.pdf")
plot(overall.m.filt$X2, pch=c(16,16,16,17,17,17), col=cbbPalette[1:3], ylab="Number of motifs", xlab="", xaxt="n", cex=1.2)
axis(1, at=seq(3.5, 148,6), labels=overall.m.filt[!duplicated(overall.m.filt$pwm),3], las=2)
legend(x="topleft", legend=c("Chimpanzee", "Human", "Unfiltered", ">=1 read motif +/- 100 bp", ">=1 read motif +/-50 bp"), pch=c(1,2,18,18,18), col=c(1,1,cbbPalette[1:3]), pt.cex=1.2, cex=0.8)
dev.off()

samples <- read.table("filtered_read_counts_by_ind.txt")
all.sums <- read.table("read_depth_all_samples") 

samples.m <- data.frame(matrix(as.character(samples$V1), ncol=2, byrow=T))
samples.m$X2 <- as.numeric(as.character(samples.m$X2))
samples.m$pwm <- sapply(strsplit(as.character(samples.m$X1), "\\."), "[[", 1)
samples.m$ind <- sapply(strsplit(as.character(samples.m$X1), "\\."), "[[", 2)

all.sums.m <- data.frame(matrix(as.character(all.sums$V1), ncol=2, byrow=T))
all.sums.m$X2 <- as.numeric(as.character(all.sums.m$X2))
all.sums.m$ind <- sapply(strsplit(as.character(all.sums.m$X1), "\\."), "[[", 1)

things.to.drop <- c(".merged.nochrM.rmdup.filter|.TAG_RG_|.bam|.final.filter.bed.gz")
samples.m$long <- sapply(strsplit(gsub(things.to.drop, "", samples.m$X1), "\\."), "[[", 2)
all.sums.m$long <- sapply(strsplit(gsub(things.to.drop, "", all.sums.m$X1), "\\."), "[[", 1)

samples.m.pch <- ifelse(grepl("^C", samples.m$ind), 16, 17)
samples.m.col <- ifelse(grepl("day20", samples.m$ind), cbbPalette[2], ifelse(grepl("^HGl", samples.m$ind), cbbPalette[3], ifelse(!grepl("TAG", samples.m$X1), "black", cbbPalette[4])))

pdf(file="filtered_read_counts_by_ind_by_pwm.pdf")
for (i in 1:25){
    motif.name <- unique(samples.m$pwm)[i]
    plot(rev(samples.m[samples.m$pwm %in% motif.name ,]$X2/total.sizes[total.sizes$pwm %in% motif.name, 1]) ~ rev(all.sums.m$X2), xlab="Reads sequenced", pch=rev(samples.m.pch), ylab="Fraction >= 1 read 100 bp +/- motif", col=rev(samples.m.col), main=paste0(motif.name, " (", total.sizes[total.sizes$pwm %in% motif.name, 1], " total motif matches)"), cex=1.2)
    legend(x="bottomright", legend=c("Chimpanzee", "Human", "Single library", "Summed ind, iPSC", "Summed ind, hearts", "Greenleaf et al data"), pch=c(1,2,18,18,18,18), col=c("black","black",cbbPalette[4], "black", cbbPalette[2], cbbPalette[3]), cex=0.9, pt.cex=1.2)
}
dev.off()