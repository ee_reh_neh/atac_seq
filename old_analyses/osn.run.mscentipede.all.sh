   
#!/bin/bash

# File: osn.run.mscentipede.all.sh
# Date: 30th December 2015
# Author: IGR
# This is the batched version of run_mscentipede.sh, which submits both steps of the msCentipede job as well as the final footprint plotting step.
# Running options agreed upon with Yoav (and Jonathan by proxy, email on 9th December 2015): "Jonathan thinks < 1e-5 is ok, so lets proceed with that. Certainly, lets try to subset reads and see what impact this has on the results. Talk to Anil some more about the multiple restart issue, and dont hesitate CCing Jonathan if you want his input as well."

# To do: filter human BAMs to the same number of reads as chimps and see what that does to running time and binding predictions. 
# To do: fix matplotlib need to open an X session when plotting, causing the command to fail. 

pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif/OSN" 
pwmlist="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/oct_sox_nanog_pwms_final.txt"
mappeddir="/mnt/gluster/home/ireneg/atac_seq/centipede/mapped_filtered"
outdir="/mnt/gluster/home/ireneg/atac_seq/centipede/output/OSN"
centipededir="/mnt/gluster/home/ireneg/bin/msCentipede"
scriptsdir="/mnt/gluster/home/ireneg/repos/atac_seq"


PYBIN="/usr/bin/python2.6"

# Make directory structure
if [ ! -d $outdir ]; then
  mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
  mkdir -p $outdir/logs
elif [ ! -d $outdir/plots ]; then
  mkdir -p $outdir/plots
fi

# Run msCentipede separately by species:

# Humans: 
    echo -e "#! /bin/bash
    
    motif=\`awk NR==\$SGE_TASK_ID $pwmlist | cut -f1 \`

    # Use only human files:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^H\"; then
            humanbams=\"\$samplename \$humanbams\"
        fi    
    done
 
    echo These are the bam files you are working with: \$humanbams

    echo This job has the task ID \$SGE_TASK_ID, and corresponds to \$motif in humans.

    # First learn the binding profile for each PWM.
    if [ -s $outdir/\$motif.human.pkl ]; then
        echo Model for \$motif has already been learned in humans
    else 
        echo Learning binding model in humans for motif \$motif.
        $PYBIN $centipededir/call_binding.py --task learn --protocol ATAC_seq --restarts 3 --model msCentipede --mintol 1e-5 --model_file $outdir/\$motif.human.pkl --posterior_file $outdir/\$motif.human.post.gz --log_file $outdir/\$motif.human.log $pwmdir/\$motif.H.final.cent.bed.gz \$humanbams
    
        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to learn binding model for \$motif in humans with error code \${exitstatus}
            rm $outdir/\$motif.human*
            exit \${exitstatus}
        else 
            echo Successfully learned binding model for \$motif in humans.
        fi
    fi


    # Then call binding on each PWM
    if [ -s $outdir/\$motif.human.post.gz ]; then
        echo Binding for \$motif has already been called
    
    else
        echo Predicting binding in humans for motif \$motif.
        $PYBIN $centipededir/call_binding.py --task infer --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/\$motif.human.pkl --posterior_file $outdir/\$motif.human.post.gz --log_file $outdir/\$motif.human.log $pwmdir/\$motif.H.final.cent.bed.gz \$humanbams

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to estimate binding for \$motif in humans with error code \${exitstatus}
            rm $outdir/\$motif.human.post.gz
            exit \${exitstatus}
        else 
            echo Successfully estimated binding for \$motif in humans.
        fi
    fi


    # Finally, plot the PWM footprint, if it works... 
    if [ -s $outdir/plots/\$motif.human.*.pdf ]; then
        echo Footprint for \$motif has already been plotted
    
    else

        echo Plotting human footprint profile for motif \$motif.
        $PYBIN $scriptsdir/plot_accessibility_profile_new.py --protocol ATAC_seq $outdir/\$motif.human.post.gz $outdir/\$motif.human.pkl

        mv $outdir/\$motif.human*.pdf $outdir/plots/\$motif.human*.pdf

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to plot footprint for \$motif in humans with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully plotted footprint for \$motif in humans.
        fi
    fi
   
  " | qsub -j y -N human.run.centipede -q blades.q -V -o $outdir/logs/\$TASK_ID.human.centipede.log -t 1-21 -l h_vmem=2g


# Chimpanzees:
    echo -e "#! /bin/bash
    
    motif=\`awk NR==\$SGE_TASK_ID $pwmlist | cut -f1 \`

    # Use only chimp files:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^C\"; then
            chimpbams=\"\$samplename \$chimpbams\"
        fi    
    done
 
    echo These are the bam files you are working with: \$chimpbams

    echo This job has the task ID \$SGE_TASK_ID, and corresponds to \$motif in chimps.

    # First learn the binding profile for each PWM.
    if [ -s $outdir/\$motif.chimp.pkl ]; then
        echo Model for \$motif has already been learned in chimps
    else 
       echo Learning binding model in chimps for motif \$motif.
        $PYBIN $centipededir/call_binding.py --task learn --protocol ATAC_seq --restarts 3 --model msCentipede --mintol 1e-5 --model_file $outdir/\$motif.chimp.pkl --posterior_file $outdir/\$motif.chimp.post.gz --log_file $outdir/\$motif.chimp.log $pwmdir/\$motif.C.final.cent.bed.gz \$chimpbams
    
        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to learn binding model for \$motif in chimps with error code \${exitstatus}
            rm $outdir/\$motif.chimp*
            exit \${exitstatus}
        else 
            echo Successfully learned binding model for \$motif in chimps.
        fi
    fi


    # Then call binding on each PWM
    if [ -s $outdir/\$motif.chimp.post.gz ]; then
        echo Binding for \$motif has already been called
    
    else
        echo Predicting binding in chimps for motif \$motif.
        $PYBIN $centipededir/call_binding.py --task infer --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/\$motif.chimp.pkl --posterior_file $outdir/\$motif.chimp.post.gz --log_file $outdir/\$motif.chimp.log $pwmdir/\$motif.C.final.cent.bed.gz \$chimpbams

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to estimate binding for \$motif in chimps with error code \${exitstatus}
            rm $outdir/\$motif.chimp.post.gz
            exit \${exitstatus}
        else 
            echo Successfully estimated binding for \$motif in chimps.
        fi
    fi


    # Finally, plot the PWM footprint, if it works... 
    if [ -s $outdir/plots/\$motif.chimp.*.pdf ]; then
        echo Footprint for \$motif has already been plotted
    
    else

        echo Plotting chimp footprint profile for motif \$motif.
        $PYBIN $scriptsdir/plot_accessibility_profile_new.py --protocol ATAC_seq $outdir/\$motif.chimp.post.gz $outdir/\$motif.chimp.pkl

        mv $outdir/\$motif.chimp*.pdf $outdir/plots/\$motif.chimp*.pdf

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to plot footprint for \$motif in chimps with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully plotted footprint for \$motif in chimps.
        fi
    fi
   
   " | qsub -N chimp.run.centipede -q blades.q -V -o $outdir/logs/\$TASK_ID.chimp.centipede.log -j y -t 1-21 -l h_vmem=2g