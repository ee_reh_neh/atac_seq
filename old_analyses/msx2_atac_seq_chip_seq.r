### For analysis of H3K27ac and H3K27me3 ChIP-seq data from human and chimpanzee iPSCs 
### IGR 15.06.04
### Last update: 15.06.05
### Finished main analysis script.
###
### load libraries
library(gplots)
library(plyr)
library(RColorBrewer)
library(EDASeq) #1.10.0
library(edgeR, lib="~/R_libs") #3.6.4
library(biomaRt)
library(VennDiagram)
library(gdata)
library(SparseM, lib="~/R_libs")
library(GO.db, lib="/mnt/gluster/home/ireneg/R_libs")
library(org.Hs.eg.db, lib="/mnt/gluster/home/ireneg/irene_gluster_Rlibs/R_libs")
library(topGO, lib="~/R_libs")
rm(list=ls())
setwd("~/ipsc_chip_seq/output")
chip <- read.table("merge_idv.txt", header=T)

# Load the RPKM expression data:
load(file="~/ipsc_paper/rpkm/voom.RPKM.loess.norm_ipsc_final_no_ribo.Rda")
ac <- chip[,1:7]
me3 <- chip[,c(1,8:13)]

sample.names <- gsub("H3K27ac_", "", names(ac)[2:7])
species.palette <- c(rep("plum4", 3), rep("yellow3", 3))
species.pch <- c(16,16,16,15,15,15)

ensembl.mart <- useMart("ensembl", dataset='hsapiens_gene_ensembl')
msx2 <- read.table("/mnt/gluster/data/internal_supp/atac_seq/centipede/analyses/MSX2_associated_genes_5KTSS.out", header=T)
chimp.ac.keep <- rowSums(chip[,2:4] > 1)
chimp.me3.keep <- rowSums(chip[,8:10] > 1)
human.ac.keep <- rowSums(chip[,5:7] > 1)
human.me3.keep <- rowSums(chip[,11:13] > 1)
head(chimp.ac.keep)
names(chimp.ac.mean) <- chip[,1]
names(chimp.me3.mean) <- chip[,1]
names(human.ac.mean) <- chip[,1]
names(human.me3.mean) <- chip[,1]
names(chimp.ac.keep) <- chip[,1]
names(chimp.me3.keep) <- chip[,1]
names(human.ac.keep) <- chip[,1]
names(human.me3.keep) <- chip[,1]

all.summaries <- data.frame(chip[,1], chimp.ac.keep, chimp.me3.keep, human.ac.keep, human.me3.keep)
names(all.summaries)[1] <- "ensg"

blah <- merge(msx2, all.summaries, by.x="V4", by.y="ensg", all.x=T, all.y=F)

msx2.chimp <- blah[blah$LogPosOdds.Pt >= log(.99/.01) & blah$LogPosOdds.Hs < log(0.99/0.01),]

msx2.chimp.ac <- msx2.chimp[msx2.chimp$chimp.ac.keep > 1 & msx2.chimp$human.ac.keep < 2,]
msx2.chimp.me3 <- msx2.chimp[msx2.chimp$chimp.me3.keep > 1 & msx2.chimp$human.me3.keep < 2,]
msx2.human.me3 <- msx2.chimp[msx2.chimp$chimp.me3.keep < 2 & msx2.chimp$human.me3.keep > 1,]
msx2.human.ac <- msx2.chimp[msx2.chimp$chimp.ac.keep < 2 & msx2.chimp$human.ac.keep > 1,]

msx2.chimp.ac.annot <- getBM(attributes = c('ensembl_gene_id', 'gene_biotype', 'external_gene_name', 'phenotype_description', 'description', 'hgnc_symbol'), filters = 'ensembl_gene_id', values = msx2.chimp.ac$V4, mart= ensembl.mart)
msx2.chimp.me3.annot <- getBM(attributes = c('ensembl_gene_id', 'gene_biotype', 'external_gene_name', 'phenotype_description', 'description', 'hgnc_symbol'), filters = 'ensembl_gene_id', values = msx2.chimp.me3$V4, mart= ensembl.mart)
msx2.human.ac.annot <- getBM(attributes = c('ensembl_gene_id', 'gene_biotype', 'external_gene_name', 'phenotype_description', 'description', 'hgnc_symbol'), filters = 'ensembl_gene_id', values = msx2.human.ac$V4, mart= ensembl.mart)
msx2.human.me3.annot <- getBM(attributes = c('ensembl_gene_id', 'gene_biotype', 'external_gene_name', 'phenotype_description', 'description', 'hgnc_symbol'), filters = 'ensembl_gene_id', values = msx2.human.me3$V4, mart= ensembl.mart)


