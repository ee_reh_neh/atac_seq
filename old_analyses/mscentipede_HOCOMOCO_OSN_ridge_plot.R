###
### Makes a ridge plot for all pluripotency associated PWMs, including those that do not have a single site with PWM score > 12.
### The one for only the 6 with a high scoring PWM can be found in mscentipede_HOCOMOCO_OSN.R

### Irene Gallego Romero
### 2017.11.11

#######################################
### 0. CLEAN AND PREPARE WORKSPACE. ###
#######################################

rm(list=ls())

library(plyr)
library(data.table)
library(GenomicRanges)
library(biomaRt)
library(rtracklayer)
library(ggplot2)
library(ggridges)
library(reshape2)
library(RColorBrewer)
library(scales) # for ggplot
library(gridExtra) # for ggplot 

setwd("~/atac_seq/centipede/output/HOCOMOCO")

options(width=150)

#######################################################################################################
### 1. FILTER TO DESIRED PWMS FROM MASTER PLURIPOTENCY LIST AND READ IN BINDING DATA FOR ALL SITES. ###
#######################################################################################################

pwmMeta <- read.table("~/atac_seq/reference/HOCOMOCO/HUMAN_mono_full_motifs.tsv", stringsAsFactors=F, sep="\t", header=T)
pwmList <- read.table("~/atac_seq/reference/HOCOMOCO/hocomoco_pwms.txt", header=F, stringsAsFactors=F)
pwmList$uniprot <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 1)
pwmList$type <- sapply(strsplit(as.character(pwmList$V2), "\\."), "[[", 3)

# And now we filter down to OSN...
pwmList2 <- merge(pwmList, pwmMeta, by.x="uniprot", by.y="UniProt.ID", all.x=T, all.y=F)
pwmList3 <- pwmList2[pwmList2$Model.rank == 0,]

ensembl75 <- useMart(host="grch37.ensembl.org", biomart="ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl")
pwmUniprot <- getBM(attributes = c('ensembl_gene_id', 'uniprot_gn', 'hgnc_id', 'entrezgene'), mart=ensembl75, filter='hgnc_id', values=pwmList3$HGNC)

pwmListFinal <- merge(pwmList3, pwmUniprot, by.x="HGNC", by.="hgnc_id", all.x=T, all.y=F)

table(pwmListFinal$type)

pwmOSN <- pwmListFinal[grepl("POU5F1\\>|SOX2\\>|NANOG|ESRRB|KLF4|STAT3|SMAD1|NR5A2|MYC|E2F1|ZFP281|PRDM14|SALL4|TCF3|ZFX|REX1|ZFP42", pwmListFinal$Transcription.factor),] # These are all the PWMs that survived the cull 
pwmOSN <- pwmOSN[!duplicated(pwmOSN$V1),]
pwmOSN <- pwmOSN[pwmOSN$V1 != "M01350",] # drop that weird Nanog S class PWM

getBindingData <- function (motif.name){
    chimp <- read.table(gzfile(paste0(motif.name, ".chimp.post.gz")), header=T, stringsAsFactors=F)
    human <- read.table(gzfile(paste0(motif.name, ".human.post.gz")), header=T, stringsAsFactors=F)

    human.bed <- read.table(gzfile(paste0("~/atac_seq/reference/HOCOMOCO/scanMotif/", motif.name, ".H.final.bed.gz")), header=F, stringsAsFactors=F)
    chimp.bed <- read.table(gzfile(paste0("~/atac_seq/reference/HOCOMOCO/scanMotif/", motif.name, ".C.final.sort.bed.gz")), header=F, stringsAsFactors=F)

    chimp.bed <- chimp.bed[-1,] # Why did we have to do this? I know it's true, but...
    chimp.all <- cbind(chimp, chimp.bed)

    human.bed <- human.bed[-1,] 
    human.all <- cbind(human, human.bed)

    both.merge <- merge(human.all[,c(1:4,13,5:8,12)], chimp.all[,c(1:4,13,5:8,12)], by.x="V4", by.y="V4", all=F, sort=F, suffixes=c(".Hs",".Pt"))
    names(both.merge)[c(1,6,15)] <- c("PWM.ID", "PWM.score.Hs", "PWM.score.Pt")

    return(both.merge)
} 

allPWMs <- lapply(pwmOSN$V1, getBindingData)

allPWMsDF <- ldply(allPWMs)
allPWMsDF$PWM <- sapply(strsplit(as.character(allPWMsDF$PWM.ID), "-"), "[[", 1)
allPWMsDF$type <- as.factor(pwmOSN[match(allPWMsDF$PWM, pwmOSN$V1),]$type)

# Change directory here for the sake of writing all the output somewhere less burdensome:
setwd("~/atac_seq/centipede/analyses/hocomoco")

pdf("ridge_plots_OSN_all.pdf", height=6)
    ggplot(allPWMsDF, aes(x=LogPosOdds.Hs, y=PWM, fill=type)) +
        geom_density_ridges(scale=4) +
        geom_vline(xintercept=log(0.99/0.01), linetype="dashed", color="grey30") +
        scale_fill_brewer(palette="Dark2", labels=c("A", "B", "C", "D", "S"), guide="legend") +
        theme_ridges() +
        theme(axis.text.y = element_text(size=rel(0.5))) +
        xlim(-10,20)

    ggplot(allPWMsDF, aes(x=LogPosOdds.Pt, y=PWM, fill=type)) +
        geom_density_ridges(scale=4) +
        geom_vline(xintercept=log(0.99/0.01), linetype="dashed", color="grey30") +
        geom_vline(xintercept=log(0.95/0.05), linetype="dashed", color="grey70") +
        scale_fill_brewer(palette="Dark2", labels=c("A", "B", "C", "D", "S"), guide="legend") +
        theme_ridges() +
        theme(axis.text.y = element_text(size=rel(0.5))) +
        xlim(-10,20)
dev.off()
