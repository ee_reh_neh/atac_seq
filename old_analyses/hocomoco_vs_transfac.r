### For examining some of the PWM data that underlies most of this paper
### Both HOCOMOCO vs Transfac as well as the Oct, Sox, Nanog PWMs in HOCOMOCO
### IGR, 17.4.17

### Last update: 17.4.17: File created

library(plyr)
library(data.table)
library(GenomicRanges)
library(ggplot2)
library(reshape2)

rm(list=ls())

options(width=150)

setwd("~/atac_seq/reference/HOCOMOCO")

tPWMs <- read.table("../centipede/high_ic_transfac_pwms.txt", stringsAsFactors=F)

hPWMs <- read.table("hocomoco_pwms.txt")
hCounts <- read.table("hocomoco_counts.txt", stringsAsFactors=F)
mhCounts <- data.frame(matrix(hCounts$V1, ncol=2, byrow=T), stringsAsFactors=F)
mhCounts[,2] <- as.numeric(mhCounts[,2]) 
mhCounts$PWM <- sapply(strsplit(as.character(mhCounts$X1), "\\."), "[[", 1)
hPWMs <- merge(hPWMs, mhCounts, by.x="V1", by.y="PWM")

# hPWMs[grepl("POU", hPWMs$V2),]
# hPWMs[grepl("PO5", hPWMs$V2),]
# hPWMs[grepl("SOX2", hPWMs$V2),]
# hPWMs[grepl("NAN", hPWMs$V2),]

#################################### 
### OCT, SOX, NANOG comparisons. ###
####################################

oct4 <- read.table(gzfile("scanMotif/M01453.H.final.bed.gz"))
sox2 <- read.table(gzfile("scanMotif/M01516.H.final.bed.gz"))
nanogA <- read.table(gzfile("scanMotif/M01349.H.final.bed.gz"))
# nanogS <- read.table(gzfile("scanMotif/M01350.H.final.bed.gz")) # NanogS is terrible, we don't use it. 

# Find and plot pairwise overlaps and PWM score distributions:
# First list everything you want to test:
testPWMs <- list("oct" = oct4, "sox" = sox2, "nanA" = nanogA)

getOverlaps <- function(PWMlist){
    bigList <- list()
    for (i in 1:length(PWMlist)){
        for (j in 2:length(PWMlist)){
            if (j > i) {
                print(paste0("Comparing ", names(PWMlist)[i], " and ", names(PWMlist)[j], "."))
                range1 <- GRanges(seqnames=PWMlist[[i]]$V1, ranges=IRanges(start=PWMlist[[i]]$V2, end=PWMlist[[i]]$V3), names=PWMlist[[i]]$V4, score=PWMlist[[i]]$V5)
                range2 <- GRanges(seqnames=PWMlist[[j]]$V1, ranges=IRanges(start=PWMlist[[j]]$V2, end=PWMlist[[j]]$V3), names=PWMlist[[j]]$V4, score=PWMlist[[j]]$V5)

                Overlap <- findOverlaps(range1, range2, ignore.strand=T, maxgap=0)
                Overlap1 <- data.frame(PWMlist[[i]][queryHits(Overlap),])
                Overlap2 <- data.frame(PWMlist[[j]][subjectHits(Overlap),])
                names(Overlap1) <- paste(names(PWMlist[i]), names(Overlap1), sep=".")
                names(Overlap2) <- paste(names(PWMlist[j]), names(Overlap2), sep=".")
                OverlapBoth <- data.frame(Overlap1, Overlap2)

                # Make the plots...
                print(ggplot(OverlapBoth, aes(x=OverlapBoth[,5], y=OverlapBoth[,11])) +
                    stat_density_2d(aes(fill=..level..), geom="polygon") +
                    labs(x=paste0("PWM scores in overlaps relative to ", names(PWMlist[i])), y=paste0("PWM scores in overlaps relative to ", names(PWMlist[j])), title = paste0(dim(OverlapBoth)[1], " overlapping PWMs GW")))

                print(ggplot() +
                   geom_density(data=PWMlist[[i]], aes(x=V5, fill="1")) + 
                   geom_density(data=PWMlist[[j]], aes(x=V5, fill="2")) + 
                   geom_density(data=Overlap1, aes(x=Overlap1[,5], fill="3")) + 
                   geom_density(data=Overlap2, aes(x=Overlap2[,5], fill="4")) + 
                   labs(x="PWM score") + 
                   scale_fill_manual(name = "", labels = c(paste0("all hits ", names(PWMlist)[i]), paste0("all hits ", names(PWMlist)[j]), paste0("overlapping hits ", names(PWMlist)[i]), paste0("overlapping hits ", names(PWMlist)[j])), values = c(alpha("blue", 0.3), alpha("green", 0.3), alpha("pink", 0.3), alpha("orange", 0.3))) +
                   guides(fill = guide_legend(nrow=2, override.aes = list(alpha = 0.1))) + 
                   theme(legend.position="bottom"))

                print(ggplot() +
                   geom_density(data=PWMlist[[i]], aes(x=V5, fill="1")) + 
                   geom_density(data=Overlap1, aes(x=Overlap1[,5], fill="2")) + 
                   labs(x="PWM score", title=paste0(dim(Overlap1)[1], " overlapping out of ", dim(PWMlist[[i]])[1], " possible")) + 
                   scale_fill_manual(name = "", labels = c(paste0("all hits ", names(PWMlist)[i]), paste0("overlapping hits ", names(PWMlist)[i])), values = c(alpha("blue", 0.3), alpha("green", 0.3))) +
                   guides(fill = guide_legend(override.aes = list(alpha = 0.1))) + 
                   theme(legend.position="bottom"))
 
                print(ggplot() +
                   geom_density(data=PWMlist[[j]], aes(x=V5, fill="1")) + 
                   geom_density(data=Overlap2, aes(x=Overlap2[,5], fill="2")) + 
                   labs(x="PWM score", title=paste0(dim(Overlap2)[1], " overlapping out of ", dim(PWMlist[[j]])[1], " possible")) +
                   scale_fill_manual(name = "", labels = c(paste0("all hits ", names(PWMlist)[j]), paste0("overlapping hits ", names(PWMlist)[j])), values = c(alpha("blue", 0.3), alpha("green", 0.3))) +
                   guides(fill = guide_legend(override.aes = list(alpha = 0.1))) + 
                   theme(legend.position = "bottom"))

                bigList <- c(bigList, list(OverlapBoth))
                dfname <- paste0(names(PWMlist)[i], ".", names(PWMlist)[j])
                names(bigList) <- c(names(bigList)[-length(names(bigList))], dfname)

            } else {
                print(paste0("Can't really compare ", names(PWMlist)[i], " and ", names(PWMlist)[j], "."))
            }
        }
    }
    return(bigList)
}

pdf(file="PWM_densities.pdf")
onion <- getOverlaps(testPWMs)
dev.off()

#########################################
### TRANSFAC vs HOCOMOCO COMPARISONS. ###
#########################################

# Pick some HOCOMOCO PWMs at different qualities, maybe cut down on all of my analyses...

hPWMs$type <- sapply(strsplit(as.character(hPWMs$V2), "\\."), "[[", 3)
hPWMs$roughName <- sapply(strsplit(as.character(hPWMs$V2), "_"), "[[", 1)

tPWMs <- tPWMs[order(as.character(tPWMs$V2)),]
tPWMs$allCaps <- toupper(tPWMs$V2)

reconciled <- merge(hPWMs, tPWMs, by.x="roughName", by.y="allCaps", suffix=c(".H", ".T"), all=F)

aPWMs <- reconciled[ sample( which( reconciled$type == "A" ) , 4 ) , ]
bPWMs <- reconciled[ sample( which( reconciled$type == "B" ) , 4 ) , ]
cPWMs <- reconciled[ sample( which( reconciled$type == "C" ) , 4 ) , ]
dPWMs <- reconciled[ sample( which( reconciled$type == "D" ) , 4 ) , ]
sPWMs <- reconciled[ sample( which( reconciled$type == "S" ) , 4 ) , ]

pdf(file="ic_vs_type.pdf")
ggplot(reconciled, aes(x=type, y=V4)) + geom_boxplot() + theme_bw() + labs(y="TRANSFAC PWM IC", x="HOCOMOCO quality score")
ggplot(reconciled, aes(x=log10(X2), y=log10(V5), colour=type)) + 
    geom_point() + 
    theme_bw() + 
    labs(x="HOCOMOCO matches (log10)", y="TRANSFAC matches (log10)") + 
    geom_abline(slope=1, colour="purple", lty=2) + 
    stat_smooth(method="lm", colour="blue", lty=3) +
    xlim(2.5,7) + 
    ylim(2.5,7)
ggplot(hPWMs, aes(x=type, y=X2, fill=type)) + geom_boxplot() + labs(y="GW PWM matches score > 7") + guides(fill=guide_legend(title="HOCOMOCO qual"))
ggplot(tPWMs, aes(x=V4, y=log10(V5))) + geom_point() + labs(y="GW PWM matches score > 7 (log10)", x="PWM IC")
dev.off()

pwmsIn <- function(pwmEquiv){
    mediumList <- list()
    for (i in 1:dim(pwmEquiv)[1]){
        print(paste0("reading in row ", i))
        hocomoco <- read.table(gzfile(paste0("scanMotif/", pwmEquiv[i,4])))
        transfac <- read.table(gzfile(paste0("../centipede/scanMotif/", pwmEquiv[i,7], ".H.final.bed.gz")))
        smallList <- list(hocomoco, transfac)
        names(smallList) <- c(as.character(pwmEquiv[i,3]), as.character(pwmEquiv[i,8]))
        mediumList <- c(mediumList, smallList)
    }
    return(mediumList)
}

ctcf <- pwmsIn(reconciled[grepl("CTCF", reconciled[,1]),][1,])
aData <- pwmsIn(aPWMs)
bData <- pwmsIn(bPWMs)
cData <- pwmsIn(cPWMs)
dData <- pwmsIn(dPWMs)
sData <- pwmsIn(sPWMs)

pdf(file="ctcf_overlaps.pdf")
ctcfOver <- getOverlaps(ctcf)
dev.off()

pdf(file="apwms_overlaps.pdf")
aOver <- getOverlaps(aData[1:2])
aOver <- c(aOver, getOverlaps(aData[3:4]))
aOver <- c(aOver, getOverlaps(aData[5:6]))
aOver <- c(aOver, getOverlaps(aData[7:8]))
dev.off()

pdf(file="bpwms_overlaps.pdf")
bOver <- getOverlaps(bData[1:2])
bOver <- c(bOver, getOverlaps(bData[3:4]))
bOver <- c(bOver, getOverlaps(bData[5:6]))
bOver <- c(bOver, getOverlaps(bData[7:8]))
dev.off()

pdf(file="cpwms_overlaps.pdf")
cOver <- getOverlaps(cData[1:2])
cOver <- c(cOver, getOverlaps(cData[3:4]))
cOver <- c(cOver, getOverlaps(cData[5:6]))
cOver <- c(cOver, getOverlaps(cData[7:8]))
dev.off()

pdf(file="dpwms_overlaps.pdf")
dOver <- getOverlaps(dData[1:2])
dOver <- c(dOver, getOverlaps(dData[3:4]))
dOver <- c(dOver, getOverlaps(dData[5:6]))
dOver <- c(dOver, getOverlaps(dData[7:8]))
dev.off()

pdf(file="spwms_overlaps.pdf")
sOver <- getOverlaps(sData[1:2])
sOver <- c(sOver, getOverlaps(sData[3:4]))
sOver <- c(sOver, getOverlaps(sData[5:6]))
sOver <- c(sOver, getOverlaps(sData[7:8]))
dev.off()

#################################
### HOCOMOCO PWMs in general. ###
#################################

# Look at how classes A, B, C, D and S look - how many sites, how good are they, read up on how they were graded again. Maybe dump all the shitty ones, or present only analyses of the high quality PWMs vs all PWMs (for supplement) - this seems like a smart idea!

setwd("~/atac_seq/centipede/analyses/hocomoco")

briefStats <- read.table("~/atac_seq/centipede/output/HOCOMOCO/overall_summary_stats.txt", header=T, stringsAsFactors=F)
briefStats <- merge(briefStats, hPWMs[,c(1,5)], by.x="motifName", by.y="V1")

briefStats$hsGw <- briefStats$boundHs/briefStats$totalPWM
briefStats$ptGw <- briefStats$boundPt/briefStats$totalPWM 
briefStats$hstss <- briefStats$boundHsTSS/briefStats$TSS.PWM 
briefStats$pttss <- briefStats$boundPtTSS/briefStats$TSS.PWM 

pdf(file="binding_vs_hocomoco_qualities.pdf")
ggplot(briefStats, aes(x=as.factor(type), y=hsGw, fill=as.factor(type))) +
  geom_violin(trim=T) +
  geom_boxplot(width=0.05, fill="white") + 
  scale_fill_brewer(palette="Set3") +
  scale_x_discrete(labels=c("A", "B", "C", "D", "S")) +
  ylim(0,1) +
  theme_bw() + 
  labs(title="", y="fraction PWM sites bound in human GW", x="") + 
  theme(legend.title=element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=F)

ggplot(briefStats, aes(x=as.factor(type), y=ptGw, fill=as.factor(type))) +
  geom_violin(trim=T) +
  geom_boxplot(width=0.05, fill="white") + 
  scale_fill_brewer(palette="Set3") +
  scale_x_discrete(labels=c("A", "B", "C", "D", "S")) +
  ylim(0,1) +
  theme_bw() + 
  labs(title="", y="fraction PWM sites bound in chimp GW", x="") + 
  theme(legend.title=element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=F)

ggplot(briefStats, aes(x=as.factor(type), y=hstss, fill=as.factor(type))) +
  geom_violin(trim=T) +
  geom_boxplot(width=0.05, fill="white") + 
  scale_fill_brewer(palette="Set3") +
  scale_x_discrete(labels=c("A", "B", "C", "D", "S")) +
  ylim(0,1) +
  theme_bw() + 
  labs(title="", y="fraction PWM sites bound in human oTSS", x="") + 
  theme(legend.title=element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=F)

ggplot(briefStats, aes(x=as.factor(type), y=pttss, fill=as.factor(type))) +
  geom_violin(trim=T) +
  geom_boxplot(width=0.05, fill="white") + 
  scale_fill_brewer(palette="Set3") +
  scale_x_discrete(labels=c("A", "B", "C", "D", "S")) +
  ylim(0,1) +
  theme_bw() + 
  labs(title="", y="fraction PWM sites bound in chimp oTSS", x="") + 
  theme(legend.title=element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=F)

  


