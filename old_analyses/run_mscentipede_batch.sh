#!/bin/bash

### For testing the effect of the --batch parameter on model learning performance/time in iPSC ATAC-seq data.
### Only runs the model fitting step.

pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif" 
mappeddir="/mnt/gluster/home/ireneg/atac_seq/centipede/mapped_filtered"
outdir="/mnt/gluster/home/ireneg/atac_seq/centipede/output"
centipededir="/mnt/gluster/home/ireneg/bin/msCentipede"
scriptsdir="/mnt/gluster/home/ireneg/repos/atac_seq"

PYBIN=/usr/bin/python2.6

### Separate all mapped files into chimp and humans:

for samplename in $mappeddir/*.bam; do
    shortname=`basename $samplename`
    if echo $shortname | grep -q "^C"; then
        chimpbams="$samplename $chimpbams"
    elif echo $shortname | grep -q "^H"; then
        humanbams="$samplename $humanbams"
    fi    
done

# Run msCentipede separately by species:

for motif in M03895 M04539 M04459 M05589 M01123 M04735;
do
  
  for batch in 1000 5000 10000 15000 20000;
    do 
        echo -e "#! /bin/bash
            echo 'Learning binding model in humans for motif $motif with batch size $batch.'
            $PYBIN $centipededir/call_binding.py --task learn --protocol ATAC_seq --batch $batch --restarts 5 --model msCentipede --mintol 1e-6 --model_file $outdir/${motif}.${batch}.human.pkl --posterior_file $outdir/${motif}.${batch}.human.post.gz --log_file $outdir/${motif}.${batch}.human.log $pwmdir/$motif.H.final.bed.gz $humanbams

           exitstatus=( \$? )
            if [ \${exitstatus}  != 0 ]; then
                echo ERROR: Failed to estimate binding for $motif in humans with error code \${exitstatus}
                exit \${exitstatus}
            else 
                echo Successfully estimated binding for $motif in humans.
            fi
        fi
        " | qsub -j y -l h_vmem=5g -N $motif.$batch.h.centipede -o $outdir/logs/$motif.$batch.h.centipede.out -V -q blades.q

        echo -e "#! /bin/bash
        #$ -q blades.q
            echo 'Learning binding model in chimps for motif $motif with batch size $batch.'
            python $centipededir/call_binding.py --task learn --protocol ATAC_seq --batch $batch --restarts 5 --model msCentipede --mintol 1e-6 --model_file $outdir/${motif}.${batch}.chimp.pkl --posterior_file $outdir/${motif}.${batch}.chimp.post.gz --log_file $outdir/${motif}.${batch}.chimp.log $pwmdir/$motif.C.final.bed.gz $chimpbams

           exitstatus=( \$? )
            if [ \${exitstatus}  != 0 ]; then
                echo ERROR: Failed to estimate binding for $motif in chimps with error code \${exitstatus}
                exit \${exitstatus}
            else 
                echo Successfully estimated binding for $motif in chimps.
            fi
        fi

        " | qsub -j y -l h_vmem=5g -N $motif.$batch.c.centipede -o $outdir/logs/$motif.$batch.c.centipede.out -V -q blades.q
    done

done
