#!/bin/bash

# File: run.macs2.sh
# Date: 26th September 2016
# Author: IGR
# Submits MACS2 jobs to call both broad and narrow peaks on one species relative to the other. 

mappeddir="/mnt/gluster/home/ireneg/atac_seq/centipede/mapped_filtered" # This is just a symlink to the individual-level merged mapped and processed bam files, so I didn't see the need to duplicate it
outdir="/mnt/gluster/home/ireneg/atac_seq/macs2/output"
PYBIN="/usr/local/bin/python2"

# Make directory structure
if [ ! -d $outdir ]; then
  mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
  mkdir -p $outdir/logs
fi

# Run MACS2.10 separately by species:

# Humans: 
    echo -e "#! /bin/bash
    
    # Use only human files:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^H\"; then
            humanbams=\"\$samplename \$humanbams\"
        fi    
    done
 
    # echo These are the bam files you are working with: \$humanbams

    # First run narrow and broad peak jobs with default options and without second species as output .
    if [ -s $outdir/\$motif.human.pkl ]; then
        echo Model for \$motif has already been learned in humans
    else 
        echo Running MACS2 on humans only. Calling narrow and broad peaks. 

        # Building the commands
        # -t : treatment files, note the lack of control.
        # -f : format, bam
        # -g : genome size, set to hs, which is 2.9*10^9 - the data going in has not been filtered for orthology, although perhaps it would be wise to do so. Or can filter the peaks post-hoc, either one works
        # -n : output file name
        # -o : outdir
        # NOT IN USE: -q : q-value threshold, set to 0.05 because this data is kind of shady and the arbitrary thresholds do not inspire much confidencen. 
        # -p : p-value threshold, set to 0.001. Another possibility is using -p 0.01 after Anshul Kundaje and ENCODE's pipeline, see here for more: https://groups.google.com/forum/#!topic/klab_genomic_pipelines_discuss/pG5jvqgzhts   
        # --broad : call broad peaks rather than narrow single TF peaks. 

        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$humanbams -f BAM -g hs -n human_solo --outdir $outdir -p 0.001
        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$humanbams -f BAM -g hs -n human_solo_broad --outdir $outdir -p 0.001 --broad

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to run MACS2 for humans with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran MACS2 for humans only.
        fi
    fi

" | qsub -j y -N hsMACS2solo -q blades.q -V -o $outdir/logs/hsMACSsolo.log -l h_vmem=10g


# Chimps: 
    echo -e "#! /bin/bash
    
    # Use only chimp files:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^C\"; then
            chimpbams=\"\$samplename \$chimpbams\"
        fi    
    done
 
    # echo These are the bam files you are working with: \$chimpbams

    # First run narrow and broad peak jobs with default options and without second species as output .
    if [ -s $outdir/\$motif.chimp.pkl ]; then
        echo Model for \$motif has already been learned in chimps
    else 
        echo Running MACS2 on chimps only. Calling narrow and broad peaks. 

        # Building the commands
        # -t : treatment files, note the lack of control.
        # -f : format, bam
        # -g : genome size, set to hs, which is 2.9*10^9 - the data going in has not been filtered for orthology, although perhaps it would be wise to do so. Or can filter the peaks post-hoc, either one works
        # -n : output file name
        # -o : outdir
        # NOT IN USE: -q : q-value threshold, set to 0.05 because this data is kind of shady and the arbitrary thresholds do not inspire much confidencen. 
        # -p : p-value threshold, set to 0.001. Another possibility is using -p 0.01 after Anshul Kundaje and ENCODE's pipeline, see here for more: https://groups.google.com/forum/#!topic/klab_genomic_pipelines_discuss/pG5jvqgzhts   
        # --broad : call broad peaks rather than narrow single TF peaks. 

        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$chimpbams -f BAM -g hs -n chimp_solo --outdir $outdir -p 0.001
        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$chimpbams -f BAM -g hs -n chimp_solo_broad --outdir $outdir -p 0.001 --broad

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to run MACS2 for chimps with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran MACS2 for chimps only.
        fi
    fi

" | qsub -j y -N ptMACS2solo -q blades.q -V -o $outdir/logs/ptMACSsolo.log -l h_vmem=10g



# Humans vs chimps: 
    echo -e "#! /bin/bash
    
    # Parse species:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^H\"; then
            humanbams=\"\$samplename \$humanbams\"
        elif echo \$shortname | grep -q \"^C\"; then
            chimpbams=\"\$samplename \$chimpbams\"
        fi    
    done

    # Now run a direct comparison with the other species. 
    if [ -s $outdir/\$motif.human.pkl ]; then
        echo Model for \$motif has already been learned in humans
    else 
        echo Running MACS2 on humans vs chimps. Calling narrow and broad peaks. 

        # Building the commands
        # -t : treatment files, note the lack of control.
        # -c : control files, in this case, the second species.
        # -f : format, bam
        # -g : genome size, set to hs, which is 2.9*10^9 - the data going in has not been filtered for orthology, although perhaps it would be wise to do so. Or can filter the peaks post-hoc, either one works
        # -n : output file name
        # -o : outdir
        # NOT IN USE: -q : q-value threshold, set to 0.05 because this data is kind of shady and the arbitrary thresholds do not inspire much confidencen. 
        # -p : p-value threshold, set to 0.001. Another possibility is using -p 0.01 after Anshul Kundaje and ENCODE's pipeline, see here for more: https://groups.google.com/forum/#!topic/klab_genomic_pipelines_discuss/pG5jvqgzhts   
        # --broad : call broad peaks rather than narrow single TF peaks. 

        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$humanbams -c \$chimpbams -f BAM -g hs -n human_chimp --outdir $outdir -p 0.001
        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$humanbams -c \$chimpbams -f BAM -g hs -n human_chimp_broad --outdir $outdir -p 0.001 --broad

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to run MACS2 for humans vs chimps with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran MACS2 for humans vs chimps.
        fi
    fi

" | qsub -j y -N hsMACS2pt -q blades.q -V -o $outdir/logs/hsMACSpt.log -l h_vmem=10g


# Chimps vs humans: 
    echo -e "#! /bin/bash
    
    # Parse species:
    for samplename in $mappeddir/*.bam; do
        shortname=\`basename \$samplename\`
        if echo \$shortname | grep -q \"^H\"; then
            humanbams=\"\$samplename \$humanbams\"
        elif echo \$shortname | grep -q \"^C\"; then
            chimpbams=\"\$samplename \$chimpbams\"
        fi    
    done
 
    # echo These are the bam files you are working with: \$chimpbams

    # First run narrow and broad peak jobs with default options and without second species as output .
    if [ -s $outdir/\$motif.chimp.pkl ]; then
        echo Model for \$motif has already been learned in chimps
    else 
        echo Running MACS2 on chimps vs humans. Calling narrow and broad peaks. 

        # Building the commands
        # -t : treatment files, note the lack of control.
        # -c : control files, in this case, the second species.
        # -f : format, bam
        # -g : genome size, set to hs, which is 2.9*10^9 - the data going in has not been filtered for orthology, although perhaps it would be wise to do so. Or can filter the peaks post-hoc, either one works
        # -n : output file name
        # -o : outdir
        # NOT IN USE: -q : q-value threshold, set to 0.05 because this data is kind of shady and the arbitrary thresholds do not inspire much confidencen. 
        # -p : p-value threshold, set to 0.001. Another possibility is using -p 0.01 after Anshul Kundaje and ENCODE's pipeline, see here for more: https://groups.google.com/forum/#!topic/klab_genomic_pipelines_discuss/pG5jvqgzhts   
        # --broad : call broad peaks rather than narrow single TF peaks. 

        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$chimpbams -c \$humanbams -f BAM -g hs -n chimp_human --outdir $outdir -p 0.001
        /mnt/gluster/data/tools/anaconda2/bin/macs2 callpeak -t \$chimpbams -c \$humanbams -f BAM -g hs -n chimp_human_broad --outdir $outdir -p 0.001 --broad

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to run MACS2 for chimps vs humans with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran MACS2 for chimps vs humans.
        fi
    fi

" | qsub -j y -N ptMACS2hs -q blades.q -V -o $outdir/logs/ptMACShs.log -l h_vmem=10g




