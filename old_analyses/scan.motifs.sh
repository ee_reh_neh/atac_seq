#!/bin/bash

# File: scan.motifs.sh
# Date: 16th June 2015
# Author: IGR
# This bash script is used to call PWM hits on the genomes of human and chimpanzees, to use with the ATAC-seq data. Given a list of motifs it scans for them in the genome using scanPwm and eventually will filter them down to a set of orthologous motifs that can be robustly called across the two species. Whether I want to do this at the window level or not remains to be decided. 

# Last update: 30.11.15: Zipped bedfile is now ready for msCentipede output. 

# To do: find a way to scan the HT-SELEX motifs, since scanPwm will not
# handle them, although the new if-else test below is functional.  

# To do: generate these files in a chimp-centric way, compare.

#transfac="/data/share/TRANSFAC/pwms/TRANSFAC.2011.3"
pwmpath="/data/external_public/pwm_models"
pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif" 
hgenome="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19/hg19.autoXM.no_Y_random.fa"
cgenome="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3/panTro3.autoXM.no_Y_random.fa"
    XB="/data/tools/ucsctools/extractBedFromXbFile"
    MAPFILEH="/data/share/tridnase/human/hg19index/crossMapp.x8b"
    MAPFILEC="/data/share/tridnase/chimp/panTro3index/crossMapp.x8b"
RCODE="~/repos/atac_seq/selectmapp.R"
CHAINHC=/mnt/gluster/home/ireneg/atac_seq/liftOvers/hg19ToPanTro3.over.chain.gz
CHAINCH=/mnt/gluster/home/ireneg/atac_seq/liftOver/panTro3ToHg19.over.chain.gz  

if [ ! -d $pwmdir ]; then
  mkdir -p $pwmdir
elif [ ! -d $pwmdir/logs ]; then
  mkdir -p $pwmdir/logs
fi

#Step 1: Scan motifs:
for motif in M03895 M04539 M04459 M05589 M01123 M04735;

# new high IC tester motifs from Anil, equivalencies are
# M03895: CTCF
# M04539: SOX2
# M01123: NANOG
# M04735: Oct3 (maps to the right Ensembl ID... sort of)
# M04459: ESRRB
# M05589: SALL2

do 
    echo -e "#! /bin/bash
    echo 'Calculating PWM hits in human and chimpanzee genomes.'
    if [ -s $pwmdir/$motif.C.bed ]; then
        echo 'PWM hits for $motif have already been calculated.'

    else
        if [[ $motif == M* ]]; then
            echo 'Motif $motif is a TRANSFAC motif'  
            motifpath=\`echo $pwmpath/TRANSFAC.2015.2/pwms\`
        else
            echo 'Motif $motif is a SELEX motif'  
            motifpath=\`echo $pwmpath/HTSELEX/pwms\`
        fi

        /home/rpique/bin/x86_64/scanPwm \$motifpath/$motif.dat -p=0.02 -t=7 $hgenome > $pwmdir/$motif.H.bed
        /home/rpique/bin/x86_64/scanPwm \$motifpath/$motif.dat -p=0.02 -t=7 $cgenome > $pwmdir/$motif.C.bed

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to scan genomes for PWM matches with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully scanned human and chimp genomes for PWM matches.
        fi
    fi
    " | qsub -j y -l h_vmem=15g -N $motif.scan -o $pwmdir/logs/$motif.scan.out


# Step 2: Filter for orthology:
# 2a: filter for orthology using Xiang's approach and code 

# This is a human-centric approach - I don't like it. It's only testing the PWMs found in humans that mapped to chimps, not the ones in chimps that have no match in human. 
    # Problem or informed decision to reflect the difference in assembly qualities?

    echo -e "#! /bin/bash
    if [ -s $pwmdir/$motif.C.filt.bed ]; then
        echo 'PWM hits have already been filtered for orthology.'
    else
    echo 'Filtering human hits for orthology overlap.'
    awk -v id=${motif} '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \"'$motif'\" \"-\" NR \"\t\" \$5 \"\t\" \$4 }' ${pwmdir}/${motif}.H.bed >  ${pwmdir}/${motif}.H.map.bed
    liftOver ${pwmdir}/${motif}.H.map.bed ${CHAINHC} ${pwmdir}/${motif}.HC.map.bed ${pwmdir}/${motif}.HC.unmap.bed
    intersectBed -a ${pwmdir}/${motif}.HC.map.bed -b ${pwmdir}/${motif}.C.bed -wa -wb -f 1 -r | awk '{print \$7 \"\t\" \$8 \"\t\" \$9 \"\t\" \$4 \"\t\" \$11 \"\t\" \$10}' > ${pwmdir}/${motif}.C.map.bed

    ### THIS IS THE PROBLEMATIC SECTION OF THE CODE THAT CAUSES THE MASSIVE SLOWDOWN.
    # Commented and modified by shyam
    # t=0
    # while read ITEM; do
    #     ARR=($(echo \${ITEM}))
    #     ID=\${ARR[3]}
    #     line1=\`cat ${pwmdir}/${motif}.C.map.bed | grep -w \${ID} | wc -l \${INFILE} | cut -f1 -d' '\`
    #     if [ \${line1} -eq 1 ]; then
    #         let t=\$t+1
    #         cat ${pwmdir}/${motif}.H.map.bed | grep -w \${ID} | awk -v id=${motif} -v s=\${t} '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" id\"-\"s \"\t\" \$5 \"\t\" \$6}' >> ${pwmdir}/${motif}.H.filt.bed

    #         cat ${pwmdir}/${motif}.C.map.bed | grep -w \${ID} | awk -v id=${motif} -v s=\${t} '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" id\"-\"s \"\t\" \$5 \"\t\" \$6}' >> ${pwmdir}/${motif}.C.filt.bed
    #     fi
    # done < ${pwmdir}/${motif}.H.map.bed
    # Replacement code
    awk 'NR==FNR {hash[\$4]++} NR!=FNR && hash[\$4]==1 {print \$0}' ${pwmdir}/${motif}.C.map.bed ${pwmdir}/${motif}.H.map.bed > ${pwmdir}/${motif}.H.filt.bed
    awk 'NR==FNR {hash[\$4]++} NR!=FNR && hash[\$4]==1 {print \$0}' ${pwmdir}/${motif}.H.filt.bed ${pwmdir}/${motif}.C.map.bed > ${pwmdir}/${motif}.C.filt.bed

    # rm ${pwmdir}/${motif}*map.bed

    fi 
    " | qsub -j y -l h_vmem=15g -N $motif.ortho -o $pwmdir/logs/$motif.ortho.out -V -hold_jid $motif.scan



# 2a cont: filter for mappability (this bit of code is taken from calcmapp.sh, written by Xiang, and then reorganised to fit the Shyam approach)

    echo -e "#! /bin/bash
    echo 'Calculating mappability around 100 bp windows of PWM match.'
    if [ -s ${pwmdir}/${motif}.C.mapp.txt ]; then
        echo 'Mappability scores for $motif have already been calculated.'
    else
            $XB $MAPFILEH ${pwmdir}/${motif}.H.filt.bed stdout -window=100 -readsize=50 | awk '{s=0; for (i=1; i<=NF; i++) {if (\$i==1) {s++}}; print s/NF }' > ${pwmdir}/${motif}.H.mapp.txt
            $XB $MAPFILEC ${pwmdir}/${motif}.C.filt.bed stdout -window=100 -readsize=50 | awk '{s=0; for (i=1; i<=NF; i++) {if (\$i==1) {s++}}; print s/NF }' > ${pwmdir}/${motif}.C.mapp.txt
        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then   
            echo ERROR: Failed to calculate mappability scores for PWM matches with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully calculated mappability scores for PWM matches.
        fi
    fi
    " | qsub -j y -l h_vmem=15g -N $motif.calc.mapp -o $pwmdir/logs/$motif.calc_mapp.out -hold_jid $motif.ortho

#2a continued: retain only those PWM matches that can be lifted over > 80% in both species. Uses selectmapp.sh and selectmapp.R from Xiang, again. 
    echo -e "#! /bin/bash
    echo 'Filtering PWMs that do not meet mappability thresholds.'
    if [ -s $pwmdir/$motif.C.final.bed ]; then
        echo 'Mappability scores for $motif have already been filtered.'
    else
        Rscript --verbose $RCODE $motif
 
        awk '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \$6 \"\t\" \$5 }' $pwmdir/$motif.C.final.bed | gzip > $pwmdir/$motif.C.final.bed.gz
        awk '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \$6 \"\t\" \$5 }' $pwmdir/$motif.H.final.bed | gzip > $pwmdir/$motif.H.final.bed.gz

        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to filter mappability scores for PWM matches with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully filtered mappability scores for PWM matches.
        fi
    fi
    " | qsub -j y -l h_vmem=15g -N $motif.select.mapp -o $pwmdir/logs/$motif.select_mapp.out -hold_jid $motif.calc.mapp

done
