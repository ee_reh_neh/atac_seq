#!/bin/bash
#$ -l hostname=!spudling47
#$ -q blades.q
#$ -l hostname=!spudling46

### Launches msCentipede in two steps, as recommended. 
### First it learns the binding patterns for each species
### Then calculates posteriors for binding in either species.

### IGR, July 5th 2015.
### First attempt.

### Last update: Dec 1st 2015
### Added plotting step from edited plot_accessibility_profile_new.py and added extensions to msCentipede output

pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/centipede/scanMotif" 
mappeddir="/mnt/gluster/home/ireneg/atac_seq/centipede/mapped_filtered"
outdir="/mnt/gluster/home/ireneg/atac_seq/centipede/output"
centipededir="/mnt/gluster/home/ireneg/bin/msCentipede"
scriptsdir="/mnt/gluster/home/ireneg/repos/atac_seq"

PYBIN=/usr/bin/python2.6

    # The contents of mappeddir have been filtered to remove bad files etc, and retain only the files that were deemed good after qc of the TSS data (see atac.seq.basic.limma.ortho.TSS, as well as lab meeting from April 2015); the equivalent R command was: 
    # tss.counts <- tss.counts[,!grepl("_100|_50|H18489|H19114FC4|C3649FC2|H20961FC1|C3651FC1|H19101FC1|C40210FC1", colnames(tss.counts))]

    # Retained files (on a flow-cell level) were reindexed with:
    # for i in `ls *bam`; do samtools index $i $i.bai; done

### Separate all mapped files into chimp and humans:

for samplename in $mappeddir/*.bam; do
    shortname=`basename $samplename`
    if echo $shortname | grep -q "^C"; then
        chimpbams="$samplename $chimpbams"
    elif echo $shortname | grep -q "^H"; then
        humanbams="$samplename $humanbams"
    fi    
done

# Run msCentipede separately by species:

for motif in M03895 M04539 M04459 M05589 M01123 M04735;

do 
    echo -e "#! /bin/bash
        echo 'Learning binding model in humans for motif $motif.'
        $PYBIN $centipededir/call_binding.py --task learn --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/${motif}.human.pkl --posterior_file $outdir/${motif}.human.post.gz --log_file $outdir/${motif}.human.log $pwmdir/$motif.H.final.bed.gz $humanbams

        echo 'Predicting binding in humans for motif $motif.'
        $PYBIN $centipededir/call_binding.py --task infer --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/${motif}.human.pkl --posterior_file $outdir/${motif}.human.post.gz --log_file $outdir/${motif}.human.log $pwmdir/$motif.H.final.bed.gz $humanbams

        echo 'Plotting human footprint profile for motif $motif.'
        $PYBIN $scriptsdir/plot_accessibility_profile_new.py --protocol ATAC_seq $outdir/${motif}.human.post.gz $outdir/${motif}.human.pkl

       exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to estimate binding for $motif in humans with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully estimated binding for $motif in humans.
        fi
    fi
    " | qsub -j y -l h_vmem=5g -N $motif.h.centipede -o $outdir/logs/$motif.h.centipede.out -wd $centipededir -V

    echo -e "#! /bin/bash
        echo 'Learning binding model in chimps for motif $motif.'
        python $centipededir/call_binding.py --task learn --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/${motif}.chimp.pkl --posterior_file $outdir/${motif}.chimp.post.gz --log_file $outdir/${motif}.chimp.log $pwmdir/$motif.C.final.bed.gz $chimpbams

        echo 'Predicting binding in chimps for motif $motif.'
        python $centipededir/call_binding.py --task infer --protocol ATAC_seq --model msCentipede --mintol 1e-5 --model_file $outdir/${motif}.chimp.pkl --posterior_file $outdir/${motif}.chimp.post.gz --log_file $outdir/${motif}.chimp.log $pwmdir/$motif.C.final.bed.gz $chimpbams

        echo 'Plotting chimpanzee footprint profile for motif $motif.'
        $PYBIN $scriptsdir/plot_accessibility_profile_new.py --protocol ATAC_seq $outdir/${motif}.chimp.post.gz $outdir/${motif}.chimp.pkl

       exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed to estimate binding for $motif in chimps with error code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully estimated binding for $motif in chimps.
        fi
    fi

    " | qsub -j y -l h_vmem=5g -N $motif.c.centipede -o $outdir/logs/$motif.c.centipede.out -wd $centipededir -V

done
