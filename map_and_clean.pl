#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

# IGR, 03.10.15
# Launch bwa 
# Checks species for deciding what index to map to.
# 1. FC1: C47ECACXX (run in the FGF core)
# 2. FC2: C47FKACXX (run by Claudia)
# 3. FC3: C5DGKACXX (Single individual, H1849)
# 4. FC4: (Single individual, H19114, original flow cell ID unclear) 
# 5. FC5: C6CD7ACXX (rerun of 8 individuals, bad lane 5)

# DO NOT USE DIRECTLY - SEE map_and_clean_wrapper.pl FOR INSTRUCTIONS!
# LOCATION OF INDEXES IS HARDCODED.

# Last udpate: 04.10.15: Added description of FC5, fixed the readgroup assignment step

# Check for successful completion of every step, clean up if failed:

# Receives the following input from command line:
  # ARGV[0]: Sample name ($indnamelong)
  # ARGV[1]: Path to fastq ($catpath)
  # ARGV[2]: outdir ($outdir)
  # ARGV[3]: Desired flowcell ID ($newfcid)  
  # ARGV[4]: lane ID ($laneid)

my $indnamelong = $ARGV[0];
my $catpath = $ARGV[1];
my $outdir = $ARGV[2];
my $newfcid = $ARGV[3];
my $laneid = $ARGV[4];

# Some basic variables:
my $picardpath = "/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar";
my $basename = "$indnamelong.$newfcid.$laneid";
my $indexpath = "/mnt/gluster/data/internal_supp/atac_seq/reference";
my $samplesheet = "/mnt/gluster/data/internal_supp/atac_seq/reference/read_group_info.txt";
my $picardtemp = "/mnt/gluster/data/internal_supp/atac_seq/picard_temp";
my $saioutdir; # Has to be outside for scope reasons. 
my $cleanoutdir;

#Determine species and index file to map to:
my $species;

if($indnamelong =~ m/^C/){
    $species = "panTro3";
}
elsif($indnamelong =~ m/^H/){
    $species = "hg19";
}
elsif($indnamelong =~ m/^R/){
    $species = "rheMac3";
}

# Check if last file in this step of pipeline exists. If not, look step by step and resume pipeline at right point:
unless (-f "$outdir/$basename.sort.rg.bam"){
  # 1. Launch bwa aln for both sets of reads:
    # basic command structure: bwa aln ref.fa short_read.fq > aln_sa.sai
    # Options:
    # -n 2: allow two mismatches in 50 bp read
    $saioutdir = "$outdir/sam";
    unless (-d $saioutdir){
      system("mkdir -p $saioutdir");
    }

      unless (-f "$saioutdir/$basename.R1.sai"){
        system("echo Mapping R1 for $basename");
        system("bwa aln -n 2 $indexpath/$species/$species.autoXM.no_Y_random.fa $catpath/$basename.R1.fastq.gz > $saioutdir/$basename.R1.sai");
        if ($? > 0){
          system("rm $saioutdir/$basename.R1.sai");
          die "Failed bwa aln R1";
        }  
      }

      unless (-f "$saioutdir/$basename.R2.sai"){
        system("echo Mapping R2 for $basename");
        system("bwa aln -n 2 $indexpath/$species/$species.autoXM.no_Y_random.fa $catpath/$basename.R2.fastq.gz > $saioutdir/$basename.R2.sai");
        if ($? > 0){
          system("rm $saioutdir/$basename.R2.sai");
          die "Failed bwa aln R2";
        }  
      }

  # 2. Launch bwa sampe and pipe output to picard
    # base command structure: bwa sampe ref.fa aln_sa1.sai aln_sa2.sai read1.fq read2.fq > aln-pe.sam
    # Options:
    # -a 5000: maximum fragment size
    # -n 3: Maximum number of alignments to output in the XA tag for reads paired properly. If a read has more than INT hits, the XA tag will not be written.
    # -N 10: Maximum number of alignments to output in the XA tag for disconcordant read pairs (excluding singletons). If a read has more than INT hits, the XA tag will not be written. 
    # -r '@RG\tID:$newfcid\tSM:$laneid' :  Specify the read group in a format like ‘@RG\tID:foo\tSM:bar’. [null]

    unless (-f "$outdir/$basename.sort.bam"){
      system("echo Running bwa sampe on $basename");
      system("bwa sampe -a 5000 -n 3 -N 10 -r " . '\'@RG\tID:'. $newfcid .'\tSM:' . $laneid . "\' $indexpath/$species/$species.autoXM.no_Y_random.fa $saioutdir/$basename.R1.sai $saioutdir/$basename.R2.sai $catpath/$basename.R1.fastq.gz $catpath/$basename.R2.fastq.gz > $saioutdir/$basename.sam");
      if ($? > 0){
        system("rm $saioutdir/$basename.sam");
        die "Failed bwa sampe";
      } 

      system("echo Finished running bwa sampe, converting sam to bam with picard");
      system("java -Xmx4g -jar $picardpath SortSam INPUT=$saioutdir/$basename.sam OUTPUT=$outdir/$basename.sort.bam SORT_ORDER=coordinate CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT TMP_DIR=$picardtemp");
      if ($? > 0){
        system("rm $outdir/$basename.sort.bam $outdir/$basename.sort.bai");
        die "Failed SortSam, see log file for more details";
      }
      elsif ($? == 0){
        system("rm $saioutdir/$basename.sam $saioutdir/$basename.R1.sai $saioutdir/$basename.R2.sai");
        system("echo deleted .sam and .sai files for $basename.");
      }
    }  

  # 3. Modify readgroup info to include individual, flowcell, lane, and library (this should've been done in the sampe or SortSam command, but I realised it too late, so it is added a separate step)
    unless (-f "$outdir/$basename.sort.rg.bam"){
      system("echo Modifying read group information for $basename");

      # Read in meta data sheet, push into array and pull out right column
      # These variables do not have to be defined, but I like having their explicit names - it makes looking at the code easier
      my $metafc;
      my $metaname;
      my $metacenter; 
      my $metalibrary; 
      my $metarun;

      open(IN, "<$samplesheet") or die ("Cannot open info file $samplesheet\n");
      while (defined (my $line = <IN>)) {
        next if $line eq '';
        chomp $line;
        my @tmp = split("\t", $line);
        if ($tmp[0] eq $newfcid){
          if ($tmp[1] eq $indnamelong){
            $metafc = $tmp[0];
            $metaname = $tmp[1];
            $metacenter = $tmp[2];
            $metalibrary = $tmp[3];
            $metarun = $tmp[4];
          }
        }
      }

      system("java -Xmx3g -jar $picardpath AddOrReplaceReadGroups INPUT=$outdir/$basename.sort.bam OUTPUT=$outdir/$basename.sort.rg.bam CREATE_INDEX=true SORT_ORDER=coordinate RGID=$metafc.$metalibrary RGLB=$metalibrary RGPL=$metarun RGPU=$laneid RGSM=$indnamelong RGCN=$metacenter VALIDATION_STRINGENCY=LENIENT TMP_DIR=$picardtemp");
      if ($? > 0){
        system("rm $outdir/$basename.sort.rg.bam");
        die "Failed to modify the read group information.";
      }
      elsif ($? == 0){
        system("rm $outdir/$basename.sort.bam");
        system("echo Finished updating read group information for $basename. Deleted $basename.sort.bam.");
      }
    }  
}

exit 0;