#!/bin/bash

# File: qc_and_filter.sh
# Date: 12th March 2015
# Author: Shyam Gopalakrishnan
# This bash script is used to 
# merge bam files by sample, then
# remove duplicates and finally
# filter by mapping quality and 
# uniqueness. Adapted from a script 
# by IGR.
# $1: Path to sort.rg.bams
# $2: Output directory name

# Date: 23rd March 2015
# Author: Shyam Gopalakrishnan
# Edit: Fixed bug with selection of file using sample name. 
# The name did not differentiate between samples with _ and 
# those without. Fixed!

if [ $# -ne 2 ]; then
  echo "USAGE: qc_and_filter.sh InputBamDirectory OutputDirectory"
  exit 1
fi

bamdirectory=$1
outdir=$2

# Some basic variables:
picardpath="/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar"
bamtools="/mnt/lustre/home/shyamg/bin/bamtools"
filterfile="/mnt/gluster/home/ireneg/repos/atac_seq/filter.mapq30.unique.json"
java="/usr/bin/java"
tmp_dir="/mnt/gluster/data/internal_supp/atac_seq/picard_temp"
samtools="/usr/local/bin/samtools"
mitofilter='/mnt/lustre/home/shyamg/projects/atac_seq/filter.nochrM.json'

# Create directories for the output logs of mark duplicates
# and place to store the rmduped bams
if [ ! -d $outdir ]; then
  mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
  mkdir -p $outdir/logs
fi

## Get the samples, and list of files for each sample.
## Generate the picard input command line for each sample.
## Read the files in directory, and
## put them in the merge command by sample.
for sampName in `ls $bamdirectory/*sort.rg.bam | xargs -i basename {} .sort.rg.bam| cut -f1 -d. | sort | uniq`; do
  ## Get all the .sort.rg.bam files for the sample
  infiles=`ls $bamdirectory/$sampName.*.sort.rg.bam | xargs -i echo -n " INPUT={}"`
  ## Generate the bam reheader commands
  firstfile=`ls $bamdirectory/$sampName.*.sort.rg.bam | head -1`
  headercmds="samtools view -H $firstfile | grep -v ^@RG | grep -v ^@PG > $tmp_dir/$sampName.header.txt\n"
  for fl in $bamdirectory/$sampName.*.sort.rg.bam; do 
    headercmds=$headercmds"samtools view -H $fl | grep ^@RG >> $tmp_dir/$sampName.header.txt\n"
  done
  ## Generate the qsub script inline
  echo -e "#! /bin/bash

#$ -cwd
#$ -j y
#$ -l h_vmem=15g
#$ -N $sampName.qc.filter
#$ -o $outdir/logs/$sampName.qc.filter.out

echo 'Starting the merge, rmdup, filter pipeline for $sampName.'
if [ -s $outdir/$sampName.merge.rmdup.filter.bam ]; then
  echo 'File $outdir/$sampName.merged.rmdup.filter.bam already exists. Not running command to recreate it.'
  echo 'If you want to run these commands again, delete the file and try again.'
  exit 1
fi
echo 'Gathering bam files.'
if [ -s $tmp_dir/$sampName.merged.bam ]; then
  echo 'The intermediate file $tmp_dir/$sampName.merged.bam exists, continuing from mito removal.'
else
  echo 'Generating header file $tmp_dir/$sampName.header.txt'
  # Generate the header for the merged file. 
  $headercmds
  # Remove accidental empty lines from header. 
  grep -v ^$ $tmp_dir/$sampName.header.txt > $tmp_dir/$sampName.tempheader.txt
  mv -f $tmp_dir/$sampName.tempheader.txt $tmp_dir/$sampName.header.txt
  # Merge the files and reheader
  $java -Xmx3g -jar $picardpath GatherBamFiles TMP_DIR=$tmp_dir OUTPUT=/dev/stdout $infiles VALIDATION_STRINGENCY=LENIENT | $samtools reheader $tmp_dir/$sampName.header.txt - | $java -Xmx8g -jar $picardpath SortSam TMP_DIR=$tmp_dir OUTPUT=$tmp_dir/$sampName.merged.bam INPUT=/dev/stdin SORT_ORDER=coordinate VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE MAX_RECORDS_IN_RAM=2000000
  exitstatus=( \${PIPESTATUS[@]} )
  if [ \${exitstatus[0]}  != 0 ]; then
    echo ERROR: Failed GatherBamFiles with exit code \${exitstatus[0]}
    exit \${exitstatus[0]}
  elif [ \${exitstatus[1]} != 0 ]; then
    echo ERROR: The samtools reheader command did not work. Exit code \${exitstatus[1]}
    exit \${exitstatus[1]}
  elif [ \${exitstatus[2]} != 0 ]; then
    echo ERROR: The SortSam command failed with exit code \${exitstatus[2]}
    exit \${exitstatus[2]}
  else 
    echo Successfully created the merged file $tmp_dir/$sampName.merged.bam.
  fi
fi

if [ -s $tmp_dir/$sampName.merged.bai ]; then
  echo 'The index file $tmp_dir/$sampName.merged.bai exists.'
else
  echo 'Creating index file.'
  $samtools index $tmp_dir/$sampName.merged.bam $tmp_dir/$sampName.merged.bai
  exitstatus=\$?
  if [ \$exitstatus != 0 ]; then
    echo ERROR: The samtools index command failed with exit code \$exitstatus.
    exit \$exitstatus
  else
    echo Successfully created index file $tmp_dir/$sampName.merged.bai.
  fi
fi

if [ '${sampName:0:1}' == 'C' ]; then
  retainChrs='chr1 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chr2A chr2B chr3 chr4 chr5 chr6 chr7 chr8 chr9 chrX'
elif [ '${sampName:0:1}' == 'H' ]; then
  retainChrs='chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX'
else
  echo Unknown species. The first letter of samplename, $sampName, is neither C nor H.
  exit 1
fi

if [ -s $tmp_dir/$sampName.merged.nochrM.bam ]; then 
  echo 'The intermediate file $tmp_dir/$sampName.merged.nochrM.bam exists, continuing from duplicate removal.'
else
  # Remove mitochondrial chromosome
  $samtools view -b -o $tmp_dir/$sampName.merged.nochrM.bam $tmp_dir/$sampName.merged.bam \$retainChrs 
  exitstatus=\$?
  if [ \$exitstatus != 0 ]; then 
    echo ERROR: The bamtools filter command to remove mito reads failed with exit code \$exitstatus.
    exit \$exitstatus
  else
    echo Succesfully removed mitochondrial reads and created file $tmp_dir/$sampName.merged.nochrM.bam.
  fi
fi

if [ -s $tmp_dir/$sampName.merged.nochrM.rmdup.bam ]; then
  echo 'The intermediate file $tmp_dir/$sampName.merged.nochrM.rmdup.bam already exists, continuing from mapq and uniqueness filtering.'
else
  $java -Xmx8g -jar $picardpath MarkDuplicates TMP_DIR=$tmp_dir INPUT=$tmp_dir/$sampName.merged.nochrM.bam OUTPUT=$tmp_dir/$sampName.merged.nochrM.rmdup.bam REMOVE_DUPLICATES=TRUE METRICS_FILE=$outdir/logs/$sampName.merged.nochrM.rmdup.out VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE
  exitstatus=\$?
  if [ \$exitstatus != 0 ]; then 
    echo ERROR: The picard remove duplicates command failed with exit code \$exitstatus.
    exit \$exitstatus
  else
    echo Succesfully removed PCR duplicated reads and created file $tmp_dir/$sampName.merged.nochrM.rmdup.bam.
  fi
fi

if [ -s $outdir/$sampName.merged.nochrM.rmdup.filter.bam ]; then
  echo 'The final file $outdir/$sampName.merged.nochrM.rmdup.filter.bam already exists. You are almost done, broth/sist -er.'
else
  $bamtools filter -in $tmp_dir/$sampName.merged.nochrM.rmdup.bam -out $outdir/$sampName.merged.nochrM.rmdup.filter.bam -script $filterfile
  exitstatus=\$?
  if [ \$exitstatus != 0 ]; then 
    echo ERROR: The bamtools command to filter mapq and uniqueness failed with exit code \$exitstatus.
    exit \$exitstatus
  else
    echo Succesfully filtered mapq and uniqueness and created file $outdir/$sampName.merged.nochrM.rmdup.filter.bam.
    echo Removing temporary files.
    totCount=\`$samtools view -c $tmp_dir/$sampName.merged.bam\`
    nomitoCount=\`$samtools view -c $tmp_dir/$sampName.merged.nochrM.bam\`
    rmdupCount=\`$samtools view -c $tmp_dir/$sampName.merged.nochrM.rmdup.bam\`
    finalCount=\`$samtools view -c $outdir/$sampName.merged.nochrM.rmdup.filter.bam\`
#    rm $tmp_dir/$sampName.merged.bam
#    rm $tmp_dir/$sampName.merged.bai
#    rm $tmp_dir/$sampName.merged.nochrM.bam
#    rm $tmp_dir/$sampName.merged.nochrM.rmdup.bam
#    rm $tmp_dir/$sampName.merged.nochrM.rmdup.bai
#    rm $tmp_dir/$sampName.header.txt 
  fi
fi

if [ -s $outdir/$sampName.merged.nochrM.rmdup.filter.bai ]; then
  echo 'The final index $outdir/$sampName.merged.nochrM.rmdup.filter.bai already exists. You are done, broth/sist -er.'
else
  echo 'Creating final index.'
  $samtools index $outdir/$sampName.merged.nochrM.rmdup.filter.bam $outdir/$sampName.merged.nochrM.rmdup.filter.bai
  exitstatus=\$?
  if [ \$exitstatus != 0 ]; then 
    echo ERROR: Samtools indexing for final bam failed with exit code \$exitstatus.
    exit \$exitstatus
  else
    echo Find me some atac-seq signal already.  
  fi
fi

echo \"Merged count: \$totCount\"
echo \"After mito removal: \$nomitoCount\"
echo \"After duplicate removal: \$rmdupCount\"
echo \"After filtering for mapq and uniqueness: \$finalCount\"
" | qsub
done
