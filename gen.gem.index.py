#! /usr/bin/env python

# File: gen.gem.index.py
# Author: Shyam Gopalakrishnan
# This script creates an index file for
# the gem mappability file. The window
# is given by user (default 700 kb)
# Here it is a multiple of 70, since the
# line size in gem mappability file is 70.
# The index files positions are 0-based
# Arguments:
# -g gem mappabiilty file
# -o output prefix
# -w window size

import sys
import argparse
import os

if __name__ != '__main__':
    print 'Cannot run this from python interpreter shell interactively.'
    sys.exit(1)

parser = argparse.ArgumentParser(description='Compute mappability in windows.')
parser.add_argument('-g', '--gem', metavar='gemMapFile', type=str, dest='gem',
                    help='Gem mappability file', required=True)
parser.add_argument('-w', '--window', metavar='windowSize', type=int, dest='window',
                    help='Window Size (multiple of 70)', required=False, default=700000)
parser.add_argument('-o', '--out', metavar='outfile', type=str, dest='outindex',
                    help='Output index file', required=False, default='')
args = parser.parse_args()

if args.outindex == '':
    args.outindex = args.gem+".gmapIndex"
if args.window%70 != 0:
    print 'Window size must be a multiple of 70. Resetting it to 700000.'
    args.window = 700000

GEM_INDEX_HEADER = os.path.basename(args.gem)+" GEM_MAP_INDEX\n"

gem = open(args.gem)
out = open(args.outindex, 'w')
out.write(GEM_INDEX_HEADER)
chrPos = 0
chrom = ''

print 'Generating index using window size', args.window, 'for gem mappability file', args.gem, '.'
while True:
    gemline = gem.readline().rstrip('\n')
    if gemline == '': break
    if gemline[0:4] == '~chr':
        print 'Done with', chrom
        chrPos = 0
        chrom = gemline[1:]
        out.write(chrom+"\t"+str(chrPos)+"\t"+str(gem.tell())+"\n")
    elif chrom != '':
        chrPos += len(gemline)
        if chrPos % args.window == 0:
            out.write(chrom+"\t"+str(chrPos)+"\t"+str(gem.tell())+"\n")

gem.close()
out.close()
