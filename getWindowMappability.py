#! /usr/bin/env python

# File: getWindowMappability.sh
# Author: Shyam Gopalakrishnan
# Date: 25th March 2015
# This script takes a gem mappability file, 
# a bed file with windows and computes the 
# mappability of the windows. Optionally, it
# also outputs the list of windows that pass
# the mappability threshold.

# Edit date: 30th March 2015
# Author: Shyam Gopalakrishnan
# Added capability to sort the output

# Edit date: 31st March 2015
# Author: Shyam Gopalakrishnan
# Added gem indexing for reasonably faster,
# random access to the mappability file for 
# handling unsorted and overlapping windows.

# Edit date: 3rd April 2015
# Author: Shyam Gopalakrishnan
# Bug fixes - minor bug fixes. 
# Typos, dictionary key errors and
# curSpecies error.

import sys
import os
import argparse
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

if __name__ != '__main__':
    print 'This script/module can only be run from the command line.'
    sys.exit(1)

# Fixed variables
UNIQUE_MAPPING = 1
ZERO_MAPPABILITY_INT = 32
WINDOW_SIZE = 700000
LINE_SIZE = 70

# Define the window mappability functions
def propUnique(x):
    a = np.mean(np.array(x)==UNIQUE_MAPPING)
    return ([a, a > args.thresh])

def mappMean(x):
    a = np.mean(x)
    return([a, a < args.thresh])

# Function to load the index file
def loadIndex(indexFile):
    index = {}
    for line in indexFile:
        (chrom, pos, filepos) = line.strip().split()
        pos = int(pos)
        filepos = int(filepos)
        if chrom in index:
            index[chrom][pos] = filepos
        else:
            index[chrom] = {}
            index[chrom][pos] = filepos
    return index

# Function to get mappability for a window
# given the gem file and the index
def getGemWindowMappability(chrom, start, end, gemfile, index):
    gemcurpos = gemfile.tell()
    destSpot = int(start/WINDOW_SIZE)*WINDOW_SIZE
    destOffset = start - destSpot
    filePos = index[chrom][destSpot] + destOffset
    gemfile.seek(filePos - gemcurpos, 1) # relative offsets
    readchars = end-start + int((end-start)/LINE_SIZE) + 1 # 1 character buffer to avoid newline issues
    gemString = gemfile.read(readchars).replace('\n', '')
    while len(gemString) < (end-start):
        curletter = gemfile.read(1)
        if curletter == '':
            break
        elif curletter == '\n':
            gemString += curletter
    gemString = [ord(x) - ZERO_MAPPABILITY_INT for x in gemString]
    return mapfun(gemString)

# Sort and write the output files
def sortingFunction(bedlineArray, chrs):
    ## The generation of the index to sort it by
    try:
        indexkey = (chrs.index(bedlineArray[0])*1e9+int(bedlineArray[1]))
    except:
        print "bedlineArray", bedlineArray
        indexkey = 1e12
    return indexkey

## Main processing 
parser = argparse.ArgumentParser(description='Compute mappability in windows.')
parser.add_argument('-g', '--gem', metavar='gemMapFile', type=str, dest='gems',
                    help='Gem mappability file', action='append', required=True)
parser.add_argument('-b', '--bed', metavar='bedFile', type=str, dest='beds',
                    help='Bed file with windows', action='append', required=True)
parser.add_argument('-w', '--writeOutput', action='store_true', dest='write',
                    help='Write output bed file')
parser.add_argument('-d', '--drawGraphs', metavar='filePrefix', type=str, dest='figs',
                    help='Output file prefix for graph files', required=False, default='')
parser.add_argument('-s', '--sortOutput', action='store_true', dest='sort',
                    help='SortOutputBeds')
parser.add_argument('-t', '--thresh', metavar='mapThreshold', type=float, dest='thresh',
                    help='Threshold for successful window', required=False, default=0.8)
parser.add_argument('-f', '--mapFunction', metavar='mapFunction', choices=['propUnique', 'mean'], dest='mapfun',
                    help='Function to compute mappability', required=False, default='propUnique')
args = parser.parse_args()

accepted_species = ['hg19', 'panTro3', 'rheMac3']
species = [os.path.basename(x).split('.')[0] for x in args.gems]
for cnt in range(len(species)):
    if not species[cnt] in accepted_species:
        print 'ERROR: Gem file', args.gems[cnt], 'not from accepted species: hg19, panTro3, rheMac3.'
        sys.exit(1)

accepted_chrs = {}
accepted_chrs['hg19'] = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX']
accepted_chrs['panTro3'] = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chr2A', 'chr2B', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']
accepted_chrs['rheMac3'] = ['chr1', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr2',  'chr20', 'chr21', 'chr22', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chrX']


if args.mapfun == 'propUnique':
    mapfun = propUnique
else:
    mapfun = mappMean

# First check if there exists a mappability index file
# If yes, load the index - only checks existence of file.
# If no, create the index and the load it. 
map_indexes = {}
for gemfile in args.gems:
    if gemfile in map_indexes:
        continue
    if not os.path.isfile(gemfile+".gmapIndex"):
        print 'Gem mappability file', gemfile, 'does not have an index. Create one before proceeding.'
        sys.exit(3)
    indfile = open(gemfile+".gmapIndex")
    header = indfile.readline().strip().split()
    if header[0] != os.path.basename(gemfile) or header[1] != "GEM_MAP_INDEX":
        print 'Invalid gem mappability index file, create a valid one before proceeding.'
    map_indexes[gemfile] = loadIndex(indfile)
    indfile.close()
    print gemfile, 'index loaded.'

# Names of windows that make it in all bedfiles
retainWindows = set()
# Load each bed file, get the windows that make the cut
# record, the name of the window and record the stats
for (bedname, gemname, curSpecies) in zip(args.beds, args.gems, species):
    bedfile = open(bedname)
    gemfile = open(gemname)
    tempRetainWindows = set() # current set of window names to be retained
    chrMapp = {} # initialize to avoid "if" in loop
    for chrom in accepted_chrs[curSpecies]: chrMapp[chrom] = []
    cnt = 0
    for bedline in bedfile:
        cnt += 1
        if cnt % 1e6 == 0:
            print cnt, "windows done."
        (chrom, start, end, winname) = bedline.strip().split()
        start = int(start)
        end = int(end)
        (stat, keep) = getGemWindowMappability(chrom, start, end, gemfile, map_indexes[gemname])
        if keep: 
            tempRetainWindows.add(winname)
        chrMapp[chrom].append(stat)
    bedfile.close()
    gemfile.close()
    print 'Done finding correct windows for', bedname
    # Retain windows that make the cut in all beds
    if len(retainWindows) == 0:
        retainWindows |= tempRetainWindows
    else:
        retainWindows &= tempRetainWindows
    print 'For bed', bedname, ': had', len(tempRetainWindows), 'windows.'
    print 'Number of windows remaining after intersection', len(retainWindows)
    tempRetainWindows.clear()
    if args.figs != '':
        ## Make the plot with distribution of stats by chromosome and the total 
        ## distribution of stats across the genome.
        ax = plt.boxplot([chrMapp[x] for x in accepted_chrs[curSpecies]], notch=True, patch_artist=True)
        plt.xticks(np.arange(len(accepted_chrs[curSpecies]))+1, accepted_chrs[curSpecies], rotation='vertical')
        plt.title(os.path.basename(bedname)[0:-4]+'\nWindow mappability vs Chromosome')
        plt.ylabel('Window mappability: '+args.mapfun)
        plt.xlabel('Chromosomes')
        plt.savefig(args.figs+"."+curSpecies+".byChr.png")
        plt.close()
        combinedStats = []
        [combinedStats.extend(x) for x in chrMapp.values()]
        n, bins, patches = plt.hist(combinedStats, bins=50)
        plt.setp(patches, 'facecolor', 'g', 'alpha', 0.5)
        plt.yscale('log', nonposy='clip')
        plt.title(os.path.basename(bedname)[0:-4]+'\nMappability distribution across the genome')
        plt.ylabel('Frequency (log-scale)')
        plt.xlabel('Window mappability score')
        plt.savefig(args.figs+"."+curSpecies+".mapdist.png")
        plt.close()
        del combinedStats[:]
# end for loop on bedfiles

print 'Starting output file writing section.'
## If write output is enabled, then write the retained windows
## The output names are made from input bed names, assuming a .bed extension
## and adding mapp(threshold).bed to it. 
if args.write:
    for (bedname, curSpecies) in zip(args.beds, species):
        outname = bedname[0:-4]+'.mapp'+str(args.thresh)+'.bed'
        outfile = open(outname, 'w')
        bedfile = open(bedname)
        tempBedlist = []
        cnt = 0
        for bedline in bedfile:
            cnt += 1
            if cnt % 1e6 == 0:
                print cnt, "windows done."
            bedtoks = bedline.strip().split()
            if bedtoks[3] in retainWindows:
                tempBedlist.append(bedtoks)
        bedfile.close()
        print 'Sorting the windows for', bedname, 'from species', curSpecies
        if args.sort:
            tempBedlist = sorted(tempBedlist, key=lambda x: sortingFunction(x, accepted_chrs[curSpecies]))
        for bedlist in tempBedlist:
            outfile.write("\t".join(bedlist))
            outfile.write("\n")
        outfile.close()
        print 'Wrote output file', outname

print 'Wrote all output files.'
