#! /usr/bin/env python
#################################################
# Filename: makeSelectFasta.py                  #
# This script extracts the selected chromsomes  #
# from a large fasta file and outputs it to a   #
# new fasta file. It can deal with gzipped or   #
# unzipped input fasta.                         #
# Author: Shyam Gopalakrishnan                  #
# Date: 03/05/2015                              #
#################################################

import argparse
import sys
import gzip

## Set up input arguments
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract sequences from large fasta file.')
    parser.add_argument('-f', '--fasta', metavar='InputFasta', type=str, dest='infasta', help='Input fasta filename', required=True)
    parser.add_argument('-o', '--out', metavar='OutputFilename', type=str, dest='out', help='Output fasta filename', required=True)
    parser.add_argument('-c', '--chr', metavar='ChromoName', type=str, dest='chr', action='append', help='Output fasta filename', required=True)
    args = parser.parse_args()
else:
    print "This script only works from command line."
    sys.exit(1)

## Open files
if args.infasta[-3:] == ".gz":
    infasta = gzip.open(args.infasta)
else:
    infasta = open(args.infasta)

outfasta = open(args.out, 'w')

## Variable indicating if the 
## current line should be kept or not. 
keep = True
## Go through each line of fasta
## Check if the line is a chromosome
## name line. If it is, check to see
## if this chromosome should be retained. 
## If yes, then set keep to True
## If no, the set Keep to False
for line in infasta:
    if line[0] == '>':
        chrname = line.strip()[1:]
        if chrname in args.chr:
            keep = True
            print "Retaining", chrname
        else:
            keep = False
    if keep:
        outfasta.write(line)

## Close the files
infasta.close()
outfasta.close()
