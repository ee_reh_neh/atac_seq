#!/bin/bash

# File: mtdna.processing.sh
# Date: 3rd April 2015
# Author: IGR
# This bash script is used to look only at the mtDNA reads generated for each chimpanzee through ATAC-seq and ultimately generate a fastq/fasta sequence for them using the MIRA/MITObim pipelines, so they can be combined with known reference sequences and we may haplotype the chimpanzees in our data. 
# Adapted from qc_and_filter.sh, by Shyam G. 

# the MITObim script is from here: https://github.com/chrishah/MITObim
# and should be cited accordingly.

# MITObim calls MIRA, which is in 
# /mnt/gluster/home/ireneg/bin/mira_4.0.2_linux-gnu_x86_64_static/bin
# This needs to be added to the user's .bashrc file, as MITObim calls it internally and it cannot easily be defined within the script. 

# Last update: 4.12.15: Fixed bug in -sample flag to MITObim: it CANNOT handle periods in the argument! Also toggled the clean, verbose and paired options.


if [ $# -ne 2 ]; then
  echo "USAGE: bash mtdna.processing.sh InputBamDirectory OutputDirectory"
  exit 1
fi

bamdirectory=$1
outdir=$2

# Some basic variables:
picardpath="/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar"
bamtools="/mnt/lustre/home/shyamg/bin/bamtools"
filterfile="/mnt/lustre/home/shyamg/projects/atac_seq/filter.mapq30.unique.json"
java="/usr/bin/java"
samtools="/usr/local/bin/samtools"
cmtdnarefseq="~/atac_seq/mtdna/panTro3_chrM.fa"
hmtdnarefseq="~/atac_seq/mtdna/hg19_chrM.fa"

# Create directories for the output logs of mark duplicates
# and place to store the rmduped bams
if [ ! -d $outdir ]; then
  mkdir -p $outdir
elif [ ! -d $outdir/logs ]; then
  mkdir -p $outdir/logs
fi

# Get the samples, and list of files for each sample.
# Generate the picard input command line for each sample.
# Read the files in directory, and
# put them in the merge command by sample.
for tsampName in `ls $bamdirectory/*.merged.bam`; do
    sampName=`basename $tsampName .merged.bam`

    ## Generate a new bam file with only mtDNA reads. 
    echo -e "#! /bin/bash

    echo 'Collecting mtDNA reads from $sampName.'
    # if [ -s outdir/$sampName.chrM.rmdup.filter.bam ]; then
    #   echo 'File outdir/$sampName.chrM.rmdup.filter.bam already exists. Not running command to recreate it.'
    #   echo 'If you want to run these commands again, delete the file and try again.'
    #   exit 1
    # fi

    if [ -s $outdir/$sampName.chrM.bam ]; then 
      echo 'The intermediate file $bamdirectory/$sampName.chrM.bam exists, continuing from duplicate removal.'
    else
      # Keep only mtDNA
      $samtools view -b -o $outdir/$sampName.chrM.bam $bamdirectory/$sampName.merged.bam chrM 
      exitstatus=\$?
      if [ \$exitstatus != 0 ]; then 
        echo ERROR: The bamtools filter command to retain mito reads failed with exit code \$exitstatus.
        exit \$exitstatus
      else
        echo Succesfully retained mitochondrial reads and created file $outdir/$sampName.chrM.bam.
      fi
    fi

    # Remove the mtDNA duplicates, because you really should
    if [ -s $outdir/$sampName.chrM.rmdup.bam ]; then
      echo 'The intermediate file $outdir/$sampName.chrM.rmdup.bam already exists, continuing from mapq and uniqueness filtering.'
    else
      $java -Xmx8g -jar $picardpath MarkDuplicates TMP_DIR=$outdir INPUT=$outdir/$sampName.chrM.bam OUTPUT=$outdir/$sampName.chrM.rmdup.bam REMOVE_DUPLICATES=TRUE METRICS_FILE=$outdir/logs/$sampName.chrM.rmdup.out VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE
      exitstatus=\$?
      if [ \$exitstatus != 0 ]; then 
        echo ERROR: The picard remove duplicates command failed with exit code \$exitstatus.
        exit \$exitstatus
      else
        echo Succesfully removed PCR duplicated reads and created file $outdir/$sampName.chrM.rmdup.bam.
      fi
    fi

    # Filter out low quality reads
    if [ -s $outdir/$sampName.chrM.rmdup.filter.bam ]; then
      echo 'The final file $outdir/$sampName.chrM.rmdup.filter.bam already exists. You are almost done, broth/sist -er.'
    else
      $bamtools filter -in $outdir/$sampName.chrM.rmdup.bam -out $outdir/$sampName.chrM.rmdup.filter.bam -script $filterfile
      exitstatus=\$?
      if [ \$exitstatus != 0 ]; then 
        echo ERROR: The bamtools command to filter mapq and uniqueness failed with exit code \$exitstatus.
        exit \$exitstatus
      else
        echo Succesfully filtered mapq and uniqueness and created file $outdir/$sampName.chrM.rmdup.filter.bam.
        totCount=\`$samtools view -c $bamdirectory/$sampName.merged.bam\`
        mitoCount=\`$samtools view -c $outdir/$sampName.chrM.bam\`
        rmdupCount=\`$samtools view -c $outdir/$sampName.chrM.rmdup.bam\`
        finalCount=\`$samtools view -c $outdir/$sampName.chrM.rmdup.filter.bam\`

        echo \"Merged count: \$totCount\"
        echo \"After mito removal: \$mitoCount\"
        echo \"After duplicate removal: \$rmdupCount\"
        echo \"After filtering for mapq and uniqueness: \$finalCount\"
      fi
    fi

    # Split each BAM into multiple fastq files
    if [ -d $outdir/$sampName.fastqs/ ]; then
      echo 'The fastq file $outdir/$sampName.chrM.fastq already exists.'
    else
      echo 'Generating mtDNA-only fastq file.'
      mkdir -p $outdir/$sampName.fastqs
      $java -Xmx5g -jar $picardpath SamToFastq INPUT=$outdir/$sampName.chrM.rmdup.filter.bam VALIDATION_STRINGENCY=SILENT INTERLEAVE=true RG_TAG=ID OUTPUT_PER_RG=true OUTPUT_DIR=$outdir/$sampName.fastqs  
      exitstatus=\$?
      if [ \$exitstatus != 0 ]; then 
        echo ERROR: Picard fastq file generation failed with exit code \$exitstatus.
        rm $outdir/$sampName.fastqs/*
        exit \$exitstatus
      else
        echo Finished splitting mtDNA reads by flow cell. 
        echo Cleaning up a bit before MITObim and MIRA.
        rm $outdir/$sampName.chrM.bam 
        rm $outdir/$sampName.chrM.rmdup.ba*
      fi
    fi
    " | qsub -cwd -j y -l h_vmem=10g -N $sampName.qc.filter -o $outdir/logs/$sampName.qc.filter.out -V

done    

# Because there are now multiple files in each fastqs directory, it was necessary for that qsub call to end there. To avoid having a two-pass script, this second job looks at the file names in the directory and serially aligns them in a single job, rather than launching a new alignment job for every read group, which would necessitate a two-pass script like mapping.stats.and.featurecounts.sh

for tsampName in `ls $bamdirectory/*.merged.bam`; do
  sampName=`basename $tsampName .merged.bam`

  # Check and assign species
  if echo $sampName | grep -q "^C"; then
      mtdnarefseq=$cmtdnarefseq
      refname=panTro3
  elif echo $sampName | grep -q "^H"; then
      mtdnarefseq=$hmtdnarefseq
      refname=hg19
  fi    

  mtdnadir=$outdir/$sampName.fastqs/

  echo -e "#! /bin/bash

  for tsplitName in \`ls $outdir/$sampName.fastqs\`; do
    
    if [ ! -d $mtdnadir/\$tsplitName ]; then
      splitName=\`basename \$tsplitName _1.fastq\`

      lsampName=$sampName.\$splitName
      samplehyphen=\`echo \$lsampName | sed 's/\./-/g'\` 
     
      if [ -d $mtdnadir/\$splitName ]; then
        echo 'Assembly directories for \$lsampName already exist. Delete them and try again if you want to rerun this step.'

      else
        mv $mtdnadir\$tsplitName $mtdnadir\$lsampName.fastq

        echo 'Assembling mtDNA with MIRA and MITObim.'
        mkdir $mtdnadir\$lsampName.mtDNA
        cd $mtdnadir\$lsampName.mtDNA
        ~/repos/atac_seq/MITObim/MITObim_1.8.pl -start 1 -end 30 -readpool $mtdnadir\$lsampName.fastq -ref $refname -sample \$samplehyphen --quick $mtdnarefseq --pair --clean &> assembly.log

        exitstatus=\$?
        if [ \$exitstatus != 0 ]; then 
          echo ERROR: MITObim and/or MIRA failed with error code \$exitstatus.
          #rm -R $mtdnadir/$sampName.mtDNA/*
          exit \$exitstatus
        else
          echo Finished assembling mtDNA sequence for \$lsampName 
        fi
      fi
    fi
  done " | qsub -cwd -j y -l h_vmem=10g -N $sampName.mtdna.assemble -o $outdir/logs/$sampName.qc.filter.out -V -hold_jid $sampName.qc.filter

done
