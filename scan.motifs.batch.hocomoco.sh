#!/bin/bash

# File: scan.motifs.batch.sh
# Date: 29th December 2015
# Author: IGR
# This is the batched version of scan.motifs.sh, which is used to call PWM hits on the genomes of human and chimpanzees, to use with the ATAC-seq data. Given a list of motifs it scans for them in the genome using scanPwm and eventually will filter them down to a set of orthologous motifs that can be robustly called across the two species. 

# To do: find a way to scan the HT-SELEX motifs, since scanPwm will not
# handle them, although the new if-else test below is functional.  

# To do: generate these files in a chimp-centric way, compare.

# PWM related files:
pwmpath="/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/pwms"
pwmdir="/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/scanMotif" 
pwmlist="/mnt/gluster/data/internal_supp/atac_seq/reference/HOCOMOCO/hocomoco_pwms.txt"

# Chains and mappability filtering files: 
hgenome="/mnt/gluster/data/internal_supp/atac_seq/reference/hg19/hg19.autoXM.no_Y_random.fa"
cgenome="/mnt/gluster/data/internal_supp/atac_seq/reference/panTro3/panTro3.autoXM.no_Y_random.fa"
    XB="/data/tools/ucsctools/extractBedFromXbFile"
    MAPFILEH="/data/share/tridnase/human/hg19index/crossMapp.x8b"
    MAPFILEC="/data/share/tridnase/chimp/panTro3index/crossMapp.x8b"
CHAINHC=/mnt/gluster/home/ireneg/atac_seq/liftOvers/hg19ToPanTro3.over.chain.gz
CHAINCH=/mnt/gluster/home/ireneg/atac_seq/liftOver/panTro3ToHg19.over.chain.gz  

# source code for R-based filtering
RCODE="~/repos/atac_seq/selectmapp.R"

if [ ! -d $pwmdir ]; then
  mkdir -p $pwmdir
elif [ ! -d $pwmdir/logs ]; then
  mkdir -p $pwmdir/logs
fi

# bam files directory, for step 3. 
mappeddir="/mnt/gluster/home/ireneg/atac_seq/centipede/mapped_filtered/"

#Step 1: Scan motifs:
echo -e "#! /bin/bash

#$ -j y
#$ -t 641
#$ -l h_vmem=5g
#$ -N human.chimp.scan_motif
#$ -o $pwmdir/logs/\$TASK_ID.scan_all.log
#$ -V

# Above, the -o command uses TASK_ID instead of SGE_TASK_ID for reasons I can't understand, but it is consistent with usage by Shyam in generate.liftovers.sh, so who am I to question?

motif=\`awk NR==\$SGE_TASK_ID $pwmlist | cut -f1 \`

echo This job has the task ID \$SGE_TASK_ID, and corresponds to \$motif.
echo Calculating PWM hits in human and chimpanzee genomes.

if [ -s $pwmdir/\$motif.C.bed ]; then
    echo PWM hits for \$motif have already been calculated.

elif [ -s $pwmdir/\$motif.C.final.bed ]; then
    echo \$motif has already been processed to the end of step 2. 

else
    /home/rpique/bin/x86_64/scanPwm $pwmpath/\${motif}_reformat.pfm -p=0.02 -t=7 $hgenome > $pwmdir/\$motif.H.bed
    /home/rpique/bin/x86_64/scanPwm $pwmpath/\${motif}_reformat.pfm -p=0.02 -t=7 $cgenome > $pwmdir/\$motif.C.bed

    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then
        echo ERROR: Failed to scan genomes for PWM matches with error code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully scanned human and chimp genomes for PWM matches.
    fi
fi

# Step 2: Filter for orthology:
# filter for orthology using Xiang's approach
# This is a human-centric approach - I don't like it. It's only testing the PWMs found in humans that mapped to chimps, not the ones in chimps that have no match in human. 
# Problem or informed decision to reflect the difference in assembly qualities?

if [ -s $pwmdir/\$motif.C.filt.bed ]; then
    echo PWM hits have already been filtered for orthology.

elif [ -s $pwmdir/\$motif.C.final.bed ]; then
    echo \$motif has already been processed to the end of step 2. 

else
    echo Filtering human hits for orthology overlap.
    awk -v id=\$motif '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \"'\$motif'\" \"-\" NR \"\t\" \$5 \"\t\" \$4 }' ${pwmdir}/\$motif.H.bed >  ${pwmdir}/\$motif.H.map.bed
    liftOver ${pwmdir}/\$motif.H.map.bed ${CHAINHC} ${pwmdir}/\$motif.HC.map.bed ${pwmdir}/\$motif.HC.unmap.bed
    intersectBed -a ${pwmdir}/\$motif.HC.map.bed -b ${pwmdir}/\$motif.C.bed -wa -wb -f 1 -r | awk '{print \$7 \"\t\" \$8 \"\t\" \$9 \"\t\" \$4 \"\t\" \$11 \"\t\" \$10}' > ${pwmdir}/\$motif.C.map.bed

    awk 'NR==FNR {hash[\$4]++} NR!=FNR && hash[\$4]==1 {print \$0}' ${pwmdir}/\$motif.C.map.bed ${pwmdir}/\$motif.H.map.bed > ${pwmdir}/\$motif.H.filt.bed
    awk 'NR==FNR {hash[\$4]++} NR!=FNR && hash[\$4]==1 {print \$0}' ${pwmdir}/\$motif.H.filt.bed ${pwmdir}/\$motif.C.map.bed > ${pwmdir}/\$motif.C.filt.bed

    rm ${pwmdir}/\$motif*map.bed
fi 

# cont: filter for mappability (this bit of code is taken from calcmapp.sh, written by Xiang, and then reorganised to fit the Shyam approach)

echo Calculating mappability around 100 bp windows of PWM match.
if [ -s ${pwmdir}/\$motif.C.mapp.txt ]; then
    echo Mappability scores for \$motif have already been calculated.

elif [ -s $pwmdir/\$motif.C.final.bed ]; then
    echo \$motif has already been processed to the end of step 2. 

else
        $XB $MAPFILEH ${pwmdir}/\$motif.H.filt.bed stdout -window=100 -readsize=50 | awk '{s=0; for (i=1; i<=NF; i++) {if (\$i==1) {s++}}; print s/NF }' > ${pwmdir}/\$motif.H.mapp.txt
        $XB $MAPFILEC ${pwmdir}/\$motif.C.filt.bed stdout -window=100 -readsize=50 | awk '{s=0; for (i=1; i<=NF; i++) {if (\$i==1) {s++}}; print s/NF }' > ${pwmdir}/\$motif.C.mapp.txt
    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then   
        echo ERROR: Failed to calculate mappability scores for PWM matches with error code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully calculated mappability scores for PWM matches.
    fi
fi

# continued: retain only those PWM matches that can be lifted over > 80% in both species. Uses selectmapp.sh and selectmapp.R from Xiang, again. 
echo 'Filtering PWMs that do not meet mappability thresholds.'
if [ -s $pwmdir/\$motif.C.final.bed ]; then
    echo Mappability scores for \$motif have already been filtered.
else
    Rscript --verbose $RCODE \$motif

    # Some cleaning up:
    rm ${pwmdir}/\$motif.C.bed
    rm ${pwmdir}/\$motif.H.bed
    rm ${pwmdir}/\$motif*filt.bed

    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then
        echo ERROR: Failed to filter mappability scores for PWM matches with error code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully filtered mappability scores for PWM matches.
    fi
fi

# Step 3: Sort the chimpanzee bed, generate zip files with and without IDs for running and mapping:
# This step 3 replaces the filtering down to read depth that came before, the results that led to abandoning it are documented on my blog posts. 
# Chimps and humans are treated differently; there are three chimp files (sorted relative to human orhtology, sorted internally, and for msCentipede) vs the two human files. 
# IGR 08.02.2016

if [ -s $pwmdir/\$motif.C.final.cent.bed.gz ]; then
    echo PWM files for \$motif have already been processed for msCentipede.
else
    sortBed -i $pwmdir/\$motif.C.final.bed | gzip > $pwmdir/\$motif.C.final.sort.bed.gz
    zcat $pwmdir/\$motif.C.final.sort.bed.gz | awk '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \$6 \"\t\" \$5 }' | gzip > $pwmdir/\$motif.C.final.cent.bed.gz

    awk '{print \$1 \"\t\" \$2 \"\t\" \$3 \"\t\" \$6 \"\t\" \$5 }' $pwmdir/\$motif.H.final.bed | gzip > $pwmdir/\$motif.H.final.cent.bed.gz

    # Some cleaning up:
    gzip $pwmdir/\$motif.C.final.bed
    gzip $pwmdir/\$motif.H.final.bed
    
    rm ${pwmdir}/\$motif*mapp*

    exitstatus=( \$? )
    if [ \${exitstatus}  != 0 ]; then
        echo ERROR: Failed to process PWM matches for msCentipede with error code \${exitstatus}
        exit \${exitstatus}
    else 
        echo Successfully processed PWM matches for msCentipede.
    fi
fi



# # Step 4: Pull out the top 10,000 motifs for learning, instead of letting msCentipede learn from the first 10,000 motifs.
# # This is a new step currently being tested - if I'm right, it should dramatically improve motif fitting etc etc. Discussed it with Heejung, and it is a better idea than simply learning from the first 10,000 motifs  - see 
# # IGR 08.02.2016

# if [ -s $pwmdir/\$motif.C.final.cent.learn.bed.gz ]; then
#     echo PWM files for \$motif have already been optimised for the msCentipede learning step.
# else
#     zcat $pwmdir/\$motif.H.final.cent.bed.gz | sort -nk5 | tail -n 10000 | sortBed -i stdin | gzip > $pwmdir/\$motif.H.final.cent.learn.bed.gz 
#     zcat $pwmdir/\$motif.C.final.cent.bed.gz | sort -nk5 | tail -n 10000 | sortBed -i stdin | gzip > $pwmdir/\$motif.C.final.cent.learn.bed.gz 

#     exitstatus=( \$? )
#     if [ \${exitstatus}  != 0 ]; then
#         echo ERROR: Failed to optimise the PWM matches for msCentipede learning step with error code \${exitstatus}
#         exit \${exitstatus}
#     else 
#         echo Successfully optimised the PWM matches for msCentipede learning step.
#     fi
# fi
" | qsub
