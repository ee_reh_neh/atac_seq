#! /bin/bash
######################################
# Filename: gem.mappability.sh       #
# Purpose: Generate gem mappability  #
# scores for hg19, panTro3 & rheMac3 #
# Date: 03/03/2014                   #
# Author: Shyam Gopalakrishnan       #
# University of Chicago              #
######################################

## NOTE: This script is currently used to run
## gem on cri since the pps cluster does not
## have right kernel to run this stuff.

## RUN COMMAND FOR CRI OR PPS: qsub -t 1-3 gem.mappability.sh

## The code reflects settings for the cri 
## cluster. Most items here can be changed 
## from '#PBS' to '#$' to make this a script
## for the pps cluster. Also the path to the
## genomes needs to be changed as well. 

## Name of program
#PBS -N mappability

## total wall time - 2 days
## comment out this for pps - not needed
#PBS -l walltime=48:00:00

## Shell to use
## comment out this for pps
#PBS -S /bin/bash

## Total number of nodes and processors per node
## Comment out this for pps
#PBS -l nodes=1

## Total memory available
## for pps, this should be -l h_vmem=8g
#PBS -l mem=20gb

## Merge the output and error streams - for 
## pps this needs to be changed to -j y
#PBS -j oe

## Variable for gem-indexer max memory
## Set to 10x10^9 (approx 10gb)
MAX_MEMORY=10000000000

## This script assumes that the file in the 
## next line contains the reference genomes
## one in each line. All results and indices
## are stored in the gem directory in the 
## directory of the reference genome. The name
## of the file is hard coded here since the
## CRI cluster does not take arguments in its
## qsub-bed shell scripts. On pps, you can
## pass it as an argument - the following code
## checks for that. If it is passed an arguemnt
## then that is used as the ref file. This is the 
## sole argument that this script can take.
if [ "x$1" == "x" ]; then #no argument passed
  REF_FILE="/home/sgopalakrishnan/projects/gem/reference_genomes_list"
else
  REF_FILE=$1
fi

if [ ! -s $REF_FILE ]; then 
  echo "ERROR: Reference genomes file '$REF_FILE' does not exist OR is empty."
  exit 1
fi

## Set the paths to the executables
GEM_INDEXER="/home/sgopalakrishnan/bin/gem-indexer"
GEM_MAPPABILITY="/home/sgopalakrishnan/bin/gem-mappability"

## This line is for the CRI cluster - comment it out on PPS
REF_GENOME=`head -$PBS_ARRAYID $REF_FILE | tail -1`
## This line is for the PPS cluster - comment it out on CRI
## REF_GENOME=`head -$SGE_TASKID $REF_FILE | tail -1`

## Find the reference genome basename and reference genome dirname
REF_GENOME_BASENAME=`basename $REF_GENOME`
REF_GENOME_DIRNAME=`dirname $REF_GENOME`

## Go to the reference genome directory
cd $REF_GENOME_DIRNAME

## Create the gem directory in the reference directory if it does not exist
## If it already does, do nothing print a message and exit. 
if [ ! -d gem ]; then
  mkdir gem
else
  echo "LOG: Directory gem exists, so storing them gem output in there."
fi

## Go into the gem directory
cd gem

## Create the output prefix
## The reference needs to end with .fa, .fasta, .fa.gz or .fasta.gz 
## Or else the script with throw and error
if [ ${REF_GENOME_BASENAME: -3} == ".fa" ]; then
  OUT_PREFIX=`basename $REF_GENOME_BASENAME .fa`_gem
elif [ ${REF_GENOME_BASENAME: -6} == ".fasta" ]; then
  OUT_PREFIX=`basename $REF_GENOME_BASENAME .fasta`_gem
elif [ ${REF_GENOME_BASENAME: -6} == ".fa.gz" ]; then
  OUT_PREFIX=`basename $REF_GENOME_BASENAME .fa.gz`_gem
elif [ ${REF_GENOME_BASENAME: -9} == ".fasta.gz" ]; then
  OUT_PREFIX=`basename $REF_GENOME_BASENAME .fasta.gz`_gem
else
  echo "ERROR: The reference filename does not end in .fa, .fasta, .fa.gz or .fasta.gz;"
  echo "ERROR: This script needs it to end in one of those - I wrote the script - Deal with it."
  exit 1
fi

echo "LOG: The output of all gem programs will be stored in the $REF_GENOME_DIRNAME/gem directory."
echo "LOG: The output prefix generated from the reference genome name is $OUT_PREFIX."
echo "LOG: Running the gem indexer for $REF_GENOME."

## Run the gem indexer
if [ -s $OUT_PREFIX.gem ]; then
  echo "LOG: The gem index $OUT_PREFIX.gem already exist. Using existing index."
else
  $GEM_INDEXER -i $REF_GENOME -o $OUT_PREFIX --complement yes -m $MAX_MEMORY --verbose
fi
## Check return value
RETVAL=$?
if [ $RETVAL -ne 0 ]; then
  ## Error
  echo "ERROR: gem-indexer command failed with error code $RETVAL."
  exit $RETVAL
else
  echo "LOG: Finished generating index for the reference genome."
fi

## Run the gem mappability estimator for read length 20 and 50.
## Note that the output name has the kmer length in it.
echo "LOG: Running the gem mappability estimator for k=20."
$GEM_MAPPABILITY -I $OUT_PREFIX.gem -o ${OUT_PREFIX}_kmer20 -l 20
## Check return value
RETVAL=$?
if [ $RETVAL -ne 0 ]; then
  ## Error
  echo "ERROR: gem-mappability command failed with error code $RETVAL."
  exit $RETVAL
else
  echo "LOG: Finished mappability for 20-mers."
fi

echo "LOG: Running the gem mappability estimator for k=20."
$GEM_MAPPABILITY -I $OUT_PREFIX.gem -o ${OUT_PREFIX}_kmer50 -l 50
## Check return value
RETVAL=$?
if [ $RETVAL -ne 0 ]; then
  ## Error
  echo "ERROR: gem-mappability command failed with error code $RETVAL."
  exit $RETVAL
else 
  echo "LOG: Finished mappabilty for 50-mers."
fi

echo "LOG: Success! This message was brought to you by Shyam G." 

