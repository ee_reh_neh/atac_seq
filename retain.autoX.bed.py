#! /usr/bin/env python

# File: retain.autoX.bed.py
# Author: Shyam Gopalakrishnan
# Date: 24th Mar 2015
# This script reads a bed file and only 
# retains the windows that are in the
# human autosomes and X. If you are using
# it for other species, change the list of 
# retained chromosomes.
# Args:
# sys.argv[1]: input bed
# sys.argv[2]: output bed

# Edit date: 26th March 2015
# Author: Shyam Gopalakrishnan
# Changed the name of the window to have both start and end position
# for uniqueness purposes.

import sys

if (len(sys.argv) != 3):
    print "Usage:", sys.argv[0], " inputBedFile outputBedFile"
    sys.exit(1)

retainedChrs = []
for i in range(1,23):
    retainedChrs.append("chr"+str(i))
retainedChrs.append("chrX")

cnt = 0
inbed = open(sys.argv[1])
outbed = open(sys.argv[2], "w")
for line in inbed:
    toks = line.strip().split()
    if (toks[0] in retainedChrs):
        toks[3] = toks[0]+"_"+toks[1]+"_"+toks[2]
        outbed.write("\t".join(toks)+"\n")
    cnt += 1
    if cnt%1000000 == 0:
        print cnt, "windows processed."
inbed.close()
outbed.close()
