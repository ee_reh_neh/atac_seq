#! /bin/bash

# File: reassign.read.groups.sh
# Author: Shyam Gopalakrishnan
# Date: 19th March 2015
# This script reassigns read group information to 
# each bam file. It is only needed because we did
# not assign unique ID to each flowcell lane combo
# bam. This script generates a new bam file with the 
# updated read group ID, then overwrites the orginal.
# This script is not needed once the script is 
# updated(DONE).
# __NOTE__
# Assumes that the current read group header is like:
# @RG ID:FC1 PL:FULL PU:L001 LB:LIB1 SM:C3647 CN:FGF
# with exactly this order of tags as well. 
# __NOTE__
# This script takes one argument.
# $1: Directory with bam files

# Date: 23rd March 2015
# Author: Shyam Gopalakrishnan
# Edit: Changed the output read group to FC.LIB, no lane
# information anymore - changed for ease of processing. 

if [ $# -ne 1 ]; then 
  echo "USAGE: reassign.read.groups.sh DirectoryOfBams"
  exit 1
fi

# Variables
tmp_dir="/mnt/gluster/data/internal_supp/atac_seq/picard_temp"
cur_dir=$1
java='/mnt/lustre/home/shyamg/bin/java'
changerg='/mnt/lustre/home/shyamg/tools/Picard/picard-tools-1.129/picard.jar AddOrReplaceReadGroups'
samhead='/usr/local/bin/samtools view -H'
samcount='/usr/local/bin/samtools view -c'

# make a list of files - store in temp directory
# change permission so no one else can delete it during the
# processing.
ls $cur_dir/*.sort.rg.bam > $tmp_dir/listOfFiles.txt
chmod 740 $tmp_dir/listOfFiles.txt
numtasks=`wc -l $tmp_dir/listOfFiles.txt | sed "s/^ \+//g" | cut -f1 -d" "`

echo "#!/bin/bash

#$ -j y
#$ -o /mnt/gluster/data/internal_supp/atac_seq/logs/change.rg.\$TASK_ID.log
#$ -l h_vmem=2g
#$ -N change.rg
#$ -t 1-$numtasks

infile=\`head -n \$SGE_TASK_ID $tmp_dir/listOfFiles.txt | tail -n 1\`

# If not exactly one read group in header - quit with info.
if [ \`$samhead \$infile | grep -c ^@RG\` -ne 1 ]; then 
  echo $infile does not have exactly 1 read group in header. Cannot process this.
  exit 1
fi

rgheader=\`$samhead \$infile | grep ^@RG\`
ID=\`echo \$rgheader | cut -f2 -d ' ' | cut -f2 -d: | cut -f1 -d.\`
PL=\`echo \$rgheader | cut -f3 -d ' ' | cut -f2 -d:\`
PU=\`echo \$rgheader | cut -f4 -d ' ' | cut -f2 -d:\`
LB=\`echo \$rgheader | cut -f5 -d ' ' | cut -f2 -d:\`
SM=\`echo \$rgheader | cut -f6 -d ' ' | cut -f2 -d:\`
CN=\`echo \$rgheader | cut -f7 -d ' ' | cut -f2 -d:\`

#modify id to add lane and library info.
ID=\"\$ID.\$LB\"

outfile=$tmp_dir/\`basename \$infile\`
$java -Xmx1g -jar $changerg INPUT=\$infile OUTPUT=\$outfile RGID=\$ID RGPL=\$PL RGPU=\$PU RGLB=\$LB RGSM=\$SM RGCN=\$CN CREATE_INDEX=TRUE VALIDATION_STRINGENCY=LENIENT
orig=\`$samcount \$infile\`
new=\`$samcount \$outfile\`
echo \`basename \$infile \`: \$orig \$new
" | qsub
